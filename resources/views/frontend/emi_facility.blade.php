@extends('frontend.layouts.app') @section('content')
<section class="pt-1 pb-1 breadcrumb_bg">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12">
                <ul
                    class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-start"
                >
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{
                            translate("Home")
                        }}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('emi') }}"
                            >"{{ translate("EMI Calculator") }}"</a
                        >
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="bg-white pt-4 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 col-md-12 col-xl-5 pb-3 pb-lg-0">
                <div
                    class="row d-flex emi_calculator_color py-3 justify-content-center"
                >
                    <div class="col-12 col-md-8 py-4">
                        <form>
                            <div class="form_heading">
                                <h2 class="fs-18 fw-600 pb-2">
                                    Please Fill up the Form Below <span style="color: red;">*</span> 
                                </h2>
                            </div>
                            <div class="form-group">
                                <label for="total_price" class="fs-14"
                                    >Total Price of the product :</label
                                >
                                <input
                                    type="number"
                                    class="form-control"
                                    id="total_price"
                                    placeholder="Total Amount"
                                />
                            </div>
                            <div class="form-group">
                                <label for="down_payment" class="fs-14"
                                    >Total Down Payment you want to pay :</label
                                >
                                <input
                                    type="number"
                                    class="form-control"
                                    id="down_payment"
                                    placeholder="Total Down Payment"
                                />
                            </div>
                            <div class="form-group">
                                <label for="select_product" class="fs-14"
                                    >Select Product</label
                                >
                                <select
                                    class="form-control"
                                    id="select_product"
                                >
                                    <option>JBL Speaker</option>
                                    <option>JBL Speaker</option>
                                    <option>JBL Speaker</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="select_bank" class="fs-14"
                                    >Select Bank</label
                                >
                                <select class="form-control" id="select_bank">
                                    <option>Sunrise Bank Limited</option>
                                    <option>Himalayan Bank Limited</option>
                                    <option>Mega Bank Limited</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="payment_time" class="fs-14"
                                    >Select Product</label
                                >
                                <select class="form-control" id="payment_time">
                                    <option>3 Months</option>
                                    <option>6 Months</option>
                                    <option>1 Year</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary px-1  w-lg-25 w-100">
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="row d-flex py-xl-0 py-lg-0 py-md-4 py-4 justify-content-center emi_content">
                    <div class="col-12 col-md-8 col-lg-10">
                        <h2 class="fs-18 fw-700">
                            Welcome to AnalogueMall EMI Calculator. Use this
                            tool to calculate the total amount you have to pay
                            to buy our products using EMI Service.
                        </h2>
                        <h3 class="fs-14 py-2"><strong>Please Follow these steps :</strong></h3>
                        <ol class="fs-14 px-3">
                            <li class="">
                                Applicants must be credit card holder from any
                                of our partner banks (Debit card holders is not
                                eligible to apply for EMI).
                            </li>
                            <li class="py-2">
                                Place a request for Quotation of the product you
                                want to buy, from AnalogueMall.
                            </li>
                            <li class="py-2">
                                Once quotation is drawn, you need to have an
                                approval from the Credit-Card Department of bank
                                you are applying for EMI.
                            </li>
                            <li class="py-2">
                                Acquire the DO(Delivery Order) and drop-by our
                                store.
                            </li>
                            <li class="py-2">
                                Based on the Bank's approval and the DO, we
                                shall be providing you the product. 
                            </li>
                        </ol>
                    
                        <div>
                            <div class="row fs-14">
                                <div class="col-6">
                                   <h2 class="fs-16 fw-700">Partner EMI Banks</h2>
                                   <h2 class="fs-14">Himalayan Bank</h2>
                                   <h2 class="fs-14">Mega Bank</h2>
                                   <h2 class="fs-14">NABIL Bank</h2>
                                   <h2 class="fs-14">Sunrise Bank</h2>
                                </div>
                                <div class="col-6">
                                    <h2 class="fs-16 fw-700">Interest Rate</h2>
                                    <h2 class="fs-14">10%</h2>
                                    <h2 class="fs-14">12%</h2>
                                    <h2 class="fs-14">8%</h2>
                                    <h2 class="fs-14">10%</h2>
                                </div>
                            </div>
                        </div>
                        <p class="py-2 fs-14">
                            For Apple MacBook, the maximum duration to pay the
                            total charges is 18 months only. For rest of the
                            products, 12 monthly only (Condition depend on the
                            choice of Bank and the opted EMI period)
                        </p>
                        <p class="py-2 text-color "> Minimum Amount for EMI is <strong> Rs. 20,000</strong>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
