@extends('frontend.layouts.app')
@section('title')
    Esewa Payment
@endsection
@section('content')
    <form id = "paymentEsewa" action="https://uat.esewa.com.np/epay/main" method="POST">
    <input value="100" name="tAmt" type="hidden">
    <input value="100" name="amt" type="hidden">
    <input value="0" name="txAmt" type="hidden">
    <input value="0" name="psc" type="hidden">
    <input value="0" name="pdc" type="hidden">
    <input value="EPAYTEST" name="scd" type="hidden">
    <input value="12ca1-696b-4cc5-a6be-2c40d929d453" name="pid" type="hidden">
    <input value="{{route('payment.done')}}" type="hidden" name="su">
    <input value="{{route('payment.cancel')}}" type="hidden" name="fu">
    <input value="Submit" type="submit">
    </form>
@endsection
@section('script')
<script>
    $(document).ready(function(){
   $("#paymentEsewa").submit();
});
</script>
@endsection