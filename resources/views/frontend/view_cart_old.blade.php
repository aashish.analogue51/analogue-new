@extends('frontend.layouts.app')

@section('content')

<!-- <section class="pt-5 mb-4">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 mx-auto">
                <div class="row aiz-steps arrow-divider">
                    <div class="col active">
                        <div class="text-center text-primary">
                            <i class="la-3x mb-2 las la-shopping-cart"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block text-capitalize">{{ translate('1. My Cart')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-map"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50 text-capitalize">{{ translate('2. Shipping info')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-truck"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50 text-capitalize">{{ translate('3. Delivery info')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-credit-card"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50 text-capitalize">{{ translate('4. Payment')}}</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="text-center">
                            <i class="la-3x mb-2 opacity-50 las la-check-circle"></i>
                            <h3 class="fs-14 fw-600 d-none d-lg-block opacity-50 text-capitalize">{{ translate('5. Confirmation')}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<style>
    .table thead th{
        border-bottom: 0px;
    }
</style>

<section class="slice-xs sct-color-2 border-bottom bg-white pt-4">

        <div class="container container-sm">
            <div class="text-center">
                <div class="justify-content-center cart_section_btn" style="display: inline-block;">
                    <ul class="nav  arrow-divider" role="tablist">
                        <li role="presentation" >
                            <a href="javascript:void(0)" class="active" aria-controls="my_cart" role="tab" data-toggle="tab">
                                <div class="icon-block icon-block--style-1-v5 text-center ">
                                    <div class="block-icon mb-0">
                                        <i class="la-3x mb-2 la la-shopping-cart"></i>
                                    </div>
                                    <div class="block-content d-md-block">
                                        <h3 class="fs-14 fw-600 d-md-block text-capitalize">{{ translate('1. My Cart')}}</h3>
                                    </div>
                                </div>                            
                            </a>
                        </li>
                        <li role="presentation" >
                            <a href="javascript:void(0)" class="disabled" aria-controls="shipping" role="tab" data-toggle="tab">                            
                                <div class="icon-block icon-block--style-1-v5 text-center">
                                    <div class="block-icon c-gray-light mb-0">
                                        <i class="la-3x mb-2 la la-map-o"></i>
                                    </div>
                                    <div class="block-content d-md-block">
                                        <h3 class="fs-14 fw-600 d-md-block text-capitalize">{{ translate('2. Shipping info')}}</h3>
                                    </div>
                                </div>                            
                           </a>
                       </li>
                      <li role="presentation" >
                            <a href="#shipping" class="">
                                <div class="icon-block icon-block--style-1-v5 text-center">
                                    <div class="block-icon c-gray-light mb-0">
                                        <i class="la-3x mb-2 las la-truck"></i>
                                    </div>
                                    <div class="block-content d-md-block">
                                        <h3 class="fs-14 fw-600 d-lg-block text-capitalize">{{ translate('3. Delivery info')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </li>
                
                        <li role="presentation" >
                            <a href="#payment" class="" aria-controls="payment" role="tab" data-toggle="tab">
                                <div class="icon-block icon-block--style-1-v5 text-center">
                                    <div class="block-icon c-gray-light mb-0">
                                        <i class="la-3x mb-2 la la-credit-card"></i>
                                    </div>
                                    <div class="block-content d-md-block">
                                        <h3 class="fs-14 fw-600 d-md-block text-capitalize">{{ translate('4. Payment')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </li>
                       <li role="presentation" >
                            <a href="#confirm" class="" aria-controls="confirm" role="tab" data-toggle="tab">
                                <div class="icon-block icon-block--style-1-v5 text-center">
                                    <div class="block-icon c-gray-light mb-0">
                                        <i class="la-3x mb-2 la la-check-circle"></i>
                                    </div>
                                    <div class="block-content d-md-block">
                                        <h3 class="fs-14 fw-600 d-md-block text-capitalize">{{ translate('5. Confirmation')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </li>                   
                    </ul>
                </div>
            </div>
        </div>
    </section>
<section class="p-top20 product-cart-detail bg-white">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="my_cart">
                <section class="mb-4" id="cart-summary">
                    <div class="container">
                        @if( Session::has('cart') && count(Session::get('cart')) > 0 )
                            <div class="row">
                                <div class="col-xxl-12 col-xxl-8 col-xl-10 col-xl-12 mx-auto">
                                    <div class="shadow-sm bg-white p-3 p-lg-4 rounded text-left">
                                        <div class="mb-4">
                                            <div class="row gutters-5   d-none d-lg-flex border-bottom mb-3 pb-3">
                                                <div class="col-md-5 fw-600 product-name">{{ translate('Product')}}</div>
                                                <div class="col fw-600 product-price">{{ translate('Price')}}</div>
                                                <div class="col fw-600 product-tax">{{ translate('Tax')}}</div>
                                                <div class="col fw-600 product-quanity">{{ translate('Quantity')}}</div>
                                                <div class="col fw-600 product-total">{{ translate('Total')}}</div>
                                                <div class="col-auto fw-600 product-remove">{{ translate('Remove')}}</div>
                                            </div>
                                            <ul class="list-group list-group-flush">
                                                @php
                                                $total = 0;
                                                @endphp
                                                @foreach (Session::get('cart') as $key => $cartItem)
                                                    @php
                                                    $product = \App\Product::find($cartItem['id']);
                                                    $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                    $product_name_with_choice = $product->getTranslation('name');
                                                    if ($cartItem['variant'] != null) {
                                                        $product_name_with_choice = $product->getTranslation('name').' - '.$cartItem['variant'];
                                                    }
                                                    @endphp
                                                    <li class="list-group-item px-0 px-lg-3">
                                                        <div class="row gutters-5">
                                                            <div class="col-lg-5 d-flex">
                                                                <span class="mr-2 ml-0 product-image">
                                                                    <img
                                                                        src="{{ uploaded_asset($product->thumbnail_img) }}"
                                                                        class="img-fit size-60px rounded"
                                                                        alt="{{  $product->getTranslation('name')  }}"
                                                                    >
                                                                </span>
                                                                <span class="fs-14 opacity-60 product-name">{{ $product_name_with_choice }}</span>
                                                            </div>

                                                            <div class="col-lg col-4 order-1 order-lg-0 my-3 my-lg-0">
                                                                <span class="opacity-60 fs-12 d-block d-lg-none product-price">{{ translate('Price')}}</span>
                                                                <span class="fw-600 fs-16 product-price">{{ single_price($cartItem['price']) }}</span>
                                                            </div>
                                                            <div class="col-lg col-4 order-2 order-lg-0 my-3 my-lg-0">
                                                                <span class="opacity-60 fs-12 d-block d-lg-none product-tax">{{ translate('Tax')}}</span>
                                                                <span class="fw-600 fs-16 product-tax">{{ single_price($cartItem['tax']) }}</span>
                                                            </div>

                                                            <div class="col-lg col-6 order-4 order-lg-0">
                                                                @if($cartItem['digital'] != 1)
                                                                    <div class="row no-gutters align-items-center aiz-plus-minus mr-2 ml-0 product-quanity">
                                                                        <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" type="button" data-type="minus" data-field="quantity[{{ $key }}]">
                                                                            <i class="las la-minus"></i>
                                                                        </button>
                                                                        <input type="text" name="quantity[{{ $key }}]" class="col border-0 text-center flex-grow-1 fs-16 input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" readonly onchange="updateQuantity({{ $key }}, this)">
                                                                        <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" type="button" data-type="plus" data-field="quantity[{{ $key }}]">
                                                                            <i class="las la-plus"></i>
                                                                        </button>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="col-lg col-4 order-3 order-lg-0 my-3 my-lg-0">
                                                                <span class="opacity-60 fs-12 d-block d-lg-none product-total">{{ translate('Total')}}</span>
                                                                <span class="fw-600 fs-16 text-primary product-total">{{ single_price(($cartItem['price']+$cartItem['tax'])*$cartItem['quantity']) }}</span>
                                                            </div>
                                                            <div class="col-lg-auto col-6 order-5 order-lg-0 text-right">
                                                                <a href="javascript:void(0)" onclick="removeFromCartView(event, {{ $key }})" class="btn btn-icon btn-sm btn-circle product-remove">
                                                                    <i class="las la-trash"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>

                                        <div class="px-3 py-2 mb-4 border-top d-flex justify-content-between">
                                            <span class="opacity-60 fs-15">{{translate('Subtotal')}}</span>
                                            <span class="fw-600 fs-17">{{ single_price($total) }}</span>
                                        </div>
                                        <div class="row align-items-center return-btn-link">
                                            <div class="col-md-6 text-center text-md-left order-1 order-md-0">
                                                <a href="{{ route('home') }}" class="btn btn-link">
                                                    <i class="las la-arrow-left"></i>
                                                    {{ translate('Return to shop')}}
                                                </a>
                                            </div>
                                            <div class="col-md-6 text-center text-md-right">
                                                @if(Auth::check())
                                                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary fw-600">{{ translate('Continue to Shipping')}}</a>
                                                @else
                                                    <button class="btn btn-primary fw-600" onclick="showCheckoutModal()">{{ translate('Continue to Shipping')}}</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-xl-12 mx-auto">
                                    <div class="shadow-sm bg-white p-4 rounded">
                                        <div class="text-center p-3">
                                            <i class="las la-frown la-3x opacity-60 mb-3"></i>
                                            <h3 class="h4 fw-700">{{translate('Your Cart is empty')}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </section>
               <!-- <div class="container">
                    @if(Session::has('cart'))
                        <div class="row cols-xs-space cols-sm-space cols-md-space">
                            <div class="col-xl-12">
                                <form class="form-default bg-white p-4" data-toggle="validator" role="form">
                                <div class="form-default bg-white p-4">
                                    <div class="">
                                        <div class="">
                                            <table class="table-cart border-bottom">
                                                <thead>
                                                    <tr>
                                                        <th class="product-image"></th>
                                                        <th class="product-name">{{ translate('Product')}}</th>
                                                        <th class="product-price d-none d-lg-table-cell">{{ translate('Price')}}</th>
                                                        <th class="product-quanity d-none d-md-table-cell">{{ translate('Quantity')}}</th>
                                                        <th class="product-total">{{ translate('Total')}}</th>
                                                        <th class="product-remove"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $total = 0;
                                                    @endphp
                                                    @foreach (Session::get('cart') as $key => $cartItem)
                                                        @php
                                                        $product = \App\Product::find($cartItem['id']);
                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                        $product_name_with_choice = $product->name;
                                                        if ($cartItem['variant'] != null) {
                                                            $product_name_with_choice = $product->name.' - '.$cartItem['variant'];
                                                        }
                                                        // if(isset($cartItem['color'])){
                                                        //     $product_name_with_choice .= ' - '.\App\Color::where('code', $cartItem['color'])->first()->name;
                                                        // }
                                                        // foreach (json_decode($product->choice_options) as $choice){
                                                        //     $str = $choice->name; // example $str =  choice_0
                                                        //     $product_name_with_choice .= ' - '.$cartItem[$str];
                                                        // }
                                                        @endphp
                                                        <tr class="cart-item">
                                                            <td class="product-image">
                                                                <a href="#" class="mr-3">
                                                                    <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        class="img-fit lazyload size-60px rounded"
                                        alt="{{  $product->getTranslation('name')  }}">
                                                                </a>
                                                            </td>

                                                            <td class="product-name">
                                                                <span class="pr-4 d-block">{{ $product_name_with_choice }}</span>
                                                            </td>

                                                            <td class="product-price d-none d-lg-table-cell">
                                                                <span class="pr-3 d-block">{{ single_price($cartItem['price']) }}</span>
                                                            </td>

                                                            <td class="product-quantity d-none d-md-table-cell">
                                                                @if($cartItem['digital'] != 1)
                                                                    <div class="input-group input-group--style-2 pr-4" style="width: 130px;">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]">
                                                                                <i class="la la-minus"></i>
                                                                            </button>
                                                                        </span>
                                                                        <input type="text" name="quantity[{{ $key }}]" class="form-control h-auto input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" onchange="updateQuantity({{ $key }}, this)">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]">
                                                                                <i class="la la-plus"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="product-total">
                                                                <span>{{ single_price(($cartItem['price']+$cartItem['tax'])*$cartItem['quantity']) }}</span>
                                                            </td>
                                                            <td class="product-remove">
                                                                <a href="#" onclick="removeFromCartView(event, {{ $key }})" class="text-right pl-4">
                                                                    <i class="la la-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row align-items-center pt-4 return-btn-link">
                                        <div class="col-5 text-left">
                                            <a href="{{ route('home') }}" class="link link--style-3">
                                                <i class="la la-mail-reply"></i>
                                                {{ translate('Return to shop')}}
                                            </a>
                                        </div>
                                        <div class="col-7 text-right">
                                            @if(Auth::check())
                                                <a href="{{ route('checkout.shipping_info') }}" class="btn btn-styled btn-base-1">{{ translate('Continue to Shipping')}}</a>
                                            @else
                                                <button class="btn btn-styled btn-base-1" onclick="showCheckoutModal()">{{ translate('Continue to Shipping')}}</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>

                            
                        </div>
                    @else
                        <div class="dc-header">
                            <h3 class="heading heading-6 strong-700">{{ translate('Your Cart is empty')}}</h3>
                        </div>
                    @endif
                </div>  -->
            </div>
            <div role="tabpanel" class="tab-pane shipping-info-analogue" id="shipping">
                <div class="container">
                    <div class="p-4">
                        <div class="row cols-xs-space cols-sm-space cols-md-space">
                            <div class="col-lg-8">
                                <form class="form-default" data-toggle="validator" action="{{ route('checkout.store_shipping_infostore') }}" role="form" method="POST">
                                    @csrf
                                        @if(Auth::user())
                                                    <div class="row gutters-5">
                                                @foreach (Auth::user()->addresses as $key => $address)
                                                        <div class="col-md-6 ">
                                                            <label class="aiz-megabox d-block bg-white">
                                                                <input type="radio" name="address_id" value="{{ $address->id }}" @if ($address->set_default)
                                                                    checked
                                                                @endif required>
                                                                <span class="d-flex p-3 aiz-megabox-elem address-box">
                                                                    <span class="aiz-rounded-check flex-shrink-0 mt-1"></span>
                                                                    <span class="flex-grow-1 pl-3">
                                                                        <div>
                                                                            <span class="alpha-6">{{ translate('Address') }}</span>
                                                                            <span class="strong-600 ml-2">: &nbsp; &nbsp;{{ $address->address }}</span>
                                                                        </div>
                                                                        <div>
                                                                            <span class="alpha-6">{{ translate('Postal Code') }}</span>
                                                                            <span class="strong-600 ml-2">: &nbsp; &nbsp;{{ $address->postal_code }}</span>
                                                                        </div>
                                                                        <div>
                                                                            <span class="alpha-6">{{ translate('City') }}</span>
                                                                            <span class="strong-600 ml-2">: &nbsp; &nbsp;{{ $address->city }}</span>
                                                                        </div>
                                                                        <div>
                                                                            <span class="alpha-6">{{ translate('Country') }}</span>
                                                                            <span class="strong-600 ml-2">: &nbsp; &nbsp;{{ $address->country }}</span>
                                                                        </div>
                                                                        <div>
                                                                            <span class="alpha-6">{{ translate('Phone') }}</span>
                                                                            <span class="strong-600 ml-2">: &nbsp; &nbsp;{{ $address->phone }}</span>
                                                                        </div>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                @endforeach
                                                    </div>
                                            <div class="row gutters-5">
                                                <input type="hidden" name="checkout_type" value="logged">
                                                    <div class="col-md-6 mx-auto" onclick="add_new_address()">
                                                        <div class="border p-3 rounded mb-3 c-pointer text-center bg-white">
                                                            <i class="la la-home la-2x"></i>
                                                            <div class="alpha-7">{{ translate('Add Home Address') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mx-auto" onclick="add_new_address()">
                                                        <div class="border p-3 rounded mb-3 c-pointer text-center bg-white">
                                                            <i class="la la-building la-2x"></i>
                                                            <div class="alpha-7">{{ translate('Add Office Address') }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 max-auto">
                                                    <hr>
                                                        <div class="form-group local-pickup">
                                                            <label class="control-label">{{ translate('Select Local Pickup')}}</label>
                                                            <select class="form-control custome-control" data-live-search="true" name="country">
                                                                <option>Select Local Pickup</option>
                                                                <option> Address A</option>
                                                                <option> Address A</option>
                                                                <option> Address A</option>
                                                                <!-- @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                                                    <option value="{{ $country->name }}">{{ $country->name }}</option>
                                                                @endforeach -->
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                        @else
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ translate('Name')}}</label>
                                                                <input type="text" class="form-control" name="name" placeholder="{{ translate('Name')}}" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ translate('Email')}}</label>
                                                                <input type="text" class="form-control" name="email" placeholder="{{ translate('Email')}}" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ translate('Address')}}</label>
                                                                <input type="text" class="form-control" name="address" placeholder="{{ translate('Address')}}" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ translate('Select your country')}}</label>
                                                                <select class="form-control custome-control" data-live-search="true" name="country">
                                                                    @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                                                        <option value="{{ $country->name }}">{{ $country->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <label class="control-label">{{ translate('City')}}</label>
                                                                <input type="text" class="form-control" placeholder="{{ translate('City')}}" name="city" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <label class="control-label">{{ translate('Postal code')}}</label>
                                                                <input type="text" class="form-control" placeholder="{{ translate('Postal code')}}" name="postal_code" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <label class="control-label">{{ translate('Phone')}}</label>
                                                                <input type="number" min="0" class="form-control" placeholder="{{ translate('Phone')}}" name="phone" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="checkout_type" value="guest">
                                                </div>
                                            </div>
                                        @endif                                  
                                </form>
                            </div>

                            <div class="col-lg-4 ml-lg-auto">
                                @include('frontend.partials.cart_summary')
                            </div>
                        </div>
                        <div class="row align-items-center pt-4 return-btn-link">
                                        <div class="col-5 text-left">
                                            <a href="{{ route('home') }}" class="link link--style-3">
                                                    <i class="la la-mail-reply"></i>
                                                    {{ translate('Return to shop')}}
                                                </a>
                                        </div>
                                        <div class="col-7 text-right">
                                            <a href="#payment" class="btn btn-styled btn-base-1">{{ translate('Continue to Payment')}}</a>
                                        </div>
                        </div>
                    </div>

                </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="payment">
                <div class="container">
                    <div class="p-4">
                        <div class="row cols-xs-space cols-sm-space cols-md-space">
                            <div class="col-lg-8">
                                 <div class="aiz-megabox d-block bg-white p-top10 p-bottom10">
                                        <div class="payment_method  aiz-megabox-elem">
                                            <div class="text-center text-title-payment">
                                                <h4 class="strong-800" style="font-weight: 600;">Payment</h4>
                                            </div>
                                            <ul>
                                                <li class="active">
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img width="" src="{{ static_asset('frontend/images/icons/cards/esewa.png') }}">
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- <div class="row gutters-5">
                                                    <div class="col-md-6">
                                                        <label class="aiz-megabox d-block bg-white">
                                                           
                                                            <span class="d-flex p-3 aiz-megabox-elem address-box active">
                                                                <span class="flex-grow-1 pl-3">
                                                                    <div>
                                                                        <span class="alpha-6">{{ translate('Address') }}</span>
                                                                        <span class="strong-600 ml-2">: &nbsp; &nbsp;Pinglasthan</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="alpha-6">{{ translate('Postal Code') }}</span>
                                                                        <span class="strong-600 ml-2">: &nbsp; &nbsp;5468556</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="alpha-6">{{ translate('City') }}</span>
                                                                        <span class="strong-600 ml-2">: &nbsp; &nbsp;Kathmandu</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="alpha-6">{{ translate('Country') }}</span>
                                                                        <span class="strong-600 ml-2">: &nbsp; &nbsp;Nepal</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="alpha-6">{{ translate('Phone') }}</span>
                                                                        <span class="strong-600 ml-2">: &nbsp; &nbsp;9874563210</span>
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </label>
                                                    </div>
                                    </div> -->
                            </div>                    
                            <div class="col-lg-4 ml-lg-auto">
                                @include('frontend.partials.cart_summary')
                            </div>
                        
                        </div>
                        <div class="row align-items-center pt-4 return-btn-link">
                            <div class="col-12">
                                
                            </div>
                            <div class="col-12 col-lg-8 text-md-right text-left">
                                <a href="{{ route('home') }}" class="link link--style-3">
                                    <i class="la la-mail-reply"></i>
                                    {{ translate('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-lg-4 col-12 text-left">
                                <div class="pt-3">
                                <label class="aiz-checkbox">
                                    <input type="checkbox" required id="agree_checkbox">
                                    <span class="aiz-square-check"></span>
                                    <span>{{ translate('I agree to the')}}</span>
                                <a href="{{ route('terms') }}">{{ translate('terms and conditions')}}</a>,
                                <a href="{{ route('returnpolicy') }}">{{ translate('return policy')}}</a> &
                                <a href="{{ route('privacypolicy') }}">{{ translate('privacy policy')}}</a>
                                </label>
                            </div>
                                <a href="#payment" class="btn btn-styled mt-1 btn-base-1">{{translate('Confirmation')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="confirm">
                <!-- <div class="container">
                    <div class="confirm-box p-top20 text-center">
                        <div class="icon">
                            <i class="la la-check"></i>
                        </div>
                        <h3>{{ translate('Thank You !!')}}</h3>
                        <p>{{ translate('Your Order Completed')}}</p>
                        
                    </div>
                    <div class="p-4">
                        <div class="row align-items-center pt-4 return-btn-link">
                            <div class="col-5 text-left">
                                <a href="{{ route('home') }}" class="link link--style-3">
                                    <i class="la la-mail-reply"></i>
                                    {{ translate('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-7 text-right">
                                <a href="#payment" class="btn btn-styled btn-base-1">
                                    {{ translate('Confirm Order')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->

                   {{-- @php
                        $status = $order->orderDetails->first()->delivery_status;
                    @endphp
                    --}}
                  
                    <section class="py-4">
                        <div class="container text-left">
                            <div class="row">
                                <div class="col-xl-8 mx-auto">
                                    <div class="card shadow-sm border-0 rounded">
                                        <div class="card-body">
                                            <div class="text-center py-4 mb-4">
                                                <i class="la la-check-circle la-3x text-success mb-3"></i>
                                                <h1 class="h3 mb-3 fw-600">{{ translate('Thank You for Your Order!')}}</h1>
                                                <h2 class="h5">{{ translate('Order Code:')}} <span class="fw-700 text-primary">20210216-14594458</span></h2>
                                                <p class="opacity-70 font-italic">{{  translate('A copy or your order summary has been sent to') }} {{ 'admin@admin.com' }}</p>
                                            </div>
                                            <div class="mb-4">
                                                <h5 class="fw-600 mb-3 fs-17 pb-2">{{ translate('Order Summary')}}</h5>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table">
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Order Code')}}:</td>
                                                                <td>{{ '20210216-14594458' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Name')}}:</td>
                                                                <td>{{ 'Admin admin' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Email')}}:</td>
                                                                <td>{{ 'admin@admin.com' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Shipping address')}}:</td>
                                                                <td>{{ 'Address, Kathmandu, Albania' }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <table class="table">
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Order date')}}:</td>
                                                                <td>{{ '16-02-2021 14:59 PM'}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Order status')}}:</td>
                                                                <td>{{ 'Pending' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Total order amount')}}:</td>
                                                                <td>{{ '$-2,938.000' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Shipping')}}:</td>
                                                                <td>{{ 'Flat shipping rate'}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w-50 fw-600">{{ translate('Payment method')}}:</td>
                                                                <td>{{ 'Cash on delivery' }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <h5 class="fw-600 mb-3 fs-17 pb-2">{{ translate('Order Details')}}</h5>
                                                <div>
                                                    <table class="table table-responsive-md">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th width="30%">{{ translate('Product')}}</th>
                                                                <th>{{ translate('Variation')}}</th>
                                                                <th>{{ translate('Quantity')}}</th>
                                                                <th>{{ translate('Delivery Type')}}</th>
                                                                <th class="text-right">{{ translate('Price')}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                                <tr>
                                                                    <td>{{ '1' }}</td>
                                                                    <td>
                                                                       <a href="#"> {{'FREE !!! Apple Watch Series 3 (GPS, 38mm)'}}</a>
                                                                    </td>
                                                                    <td>
                                                                        {{ '$orderDetail->variation' }}
                                                                    </td>
                                                                    <td>
                                                                        {{ 1 }}
                                                                    </td>
                                                                    <td>
                                                                       {{'Home Delivery'}}
                                                                    </td>
                                                                    <td class="text-right">{{ '$-2,600.000'}}</td>
                                                                </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-5 col-md-6 ml-auto mr-0">
                                                        <table class="table ">
                                                            <tbody>
                                                                <tr>
                                                                    <th>{{ translate('Subtotal')}}</th>
                                                                    <td class="text-right">
                                                                        <span class="fw-600">{{-- single_price($order->orderDetails->sum('price')) --}} {{'Rs. 1,25,000/-'}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>{{ translate('Shipping')}}</th>
                                                                    <td class="text-right">
                                                                        <span class="font-italic">{{-- single_price($order->orderDetails->sum('shipping_cost')) --}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>{{ translate('Tax')}}</th>
                                                                    <td class="text-right">
                                                                        <span class="font-italic">{{-- single_price($order->orderDetails->sum('tax')) --}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>{{ translate('Coupon Discount')}}</th>
                                                                    <td class="text-right">
                                                                        <span class="font-italic">{{-- single_price($order->coupon_discount) --}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="fw-600">{{-- translate('Total') --}}</span></th>
                                                                    <td class="text-right">
                                                                        <strong><span>{{-- single_price($order->grand_total) --}}</span></strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
              
            </div>
        </div>
      </section>
@endsection

@section('modal')
    <div class="modal fade" id="GuestCheckout">
        <div class="modal-dialog modal-dialog-zoom">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title fw-600">{{ translate('Login')}}</h6>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3">
                        <form class="form-default" role="form" action="{{ route('cart.login.submit') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <input type="text" class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ translate('Email Or Phone')}}" name="email" id="email">
                                @else
                                    <input type="email" class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                                @endif
                                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                                    <span class="opacity-60">{{  translate('Use country code before number') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control h-auto form-control-lg" placeholder="{{ translate('Password')}}">
                            </div>

                            <div class="row mb-2">
                                <div class="col-6">
                                    <label class="aiz-checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class=opacity-60>{{  translate('Remember Me') }}</span>
                                        <span class="aiz-square-check"></span>
                                    </label>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('password.request') }}" class="text-reset opacity-60 fs-14">{{ translate('Forgot password?')}}</a>
                                </div>
                            </div>

                            <div class="mb-5">
                                <button type="submit" class="btn btn-primary btn-block fw-600">{{  translate('Login') }}</button>
                            </div>
                        </form>

                    </div>
                    <div class="text-center mb-3">
                        <p class="text-muted mb-0">{{ translate('Dont have an account?')}}</p>
                        <a href="{{ route('user.registration') }}">{{ translate('Register Now')}}</a>
                    </div>
                    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                        <div class="separator mb-3">
                            <span class="bg-white px-3 opacity-60">{{ translate('Or Login With')}}</span>
                        </div>
                        <ul class="list-inline social colored text-center mb-5">
                            @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="facebook">
                                        <i class="lab la-facebook-f"></i>
                                    </a>
                                </li>
                            @endif
                            @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'google']) }}" class="google">
                                        <i class="lab la-google"></i>
                                    </a>
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                                <li class="list-inline-item">
                                    <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="twitter">
                                        <i class="lab la-twitter"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                    @if (\App\BusinessSetting::where('type', 'guest_checkout_active')->first()->value == 1)
                        <div class="separator mb-3">
                            <span class="bg-white px-3 opacity-60">{{ translate('Or')}}</span>
                        </div>
                        <div class="text-center">
                            <a href="{{ route('checkout.shipping_info') }}" class="btn">{{ translate('Guest Checkout')}}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="new-address-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-zoom modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title text_color fw-600" id="exampleModalLabel">{{ translate('New Address')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-default" role="form" action="{{ route('addresses.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{ translate('Address')}}</label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="form-control textarea-autogrow mb-3" placeholder="{{ translate('Your Address')}}" rows="1" name="address" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{ translate('Country')}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3">
                                    <select class="form-control mb-3 selectpicker" data-placeholder="{{ translate('Select your country')}}" name="country" required>
                                        @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{ translate('City')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Your City')}}" name="city" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{ translate('Postal code')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Your Postal Code')}}" name="postal_code" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{ translate('Phone')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('+880')}}" name="phone" value="" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-base-1 text-white">{{  translate('Add Address') }}</button>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
    function removeFromCartView(e, key){
        e.preventDefault();
        removeFromCart(key);
    }

    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        $('#GuestCheckout').modal();
    }
    </script>
    <script type="text/javascript">
    function add_new_address(){
        $('#new-address-modal').modal('show');
        
    }
</script>
@endsection


