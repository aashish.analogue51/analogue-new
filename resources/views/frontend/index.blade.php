@inject('helper', \App\Helpers\Helper)
@extends('frontend.layouts.app')

@section('content')
{{-- Categories , Sliders . Today's deal --}}
<div class="home-banner-area white-background mb-0 mb-lg-0 pt-3">
    <div class="container">
        <div class="row gutters-10 position-relative">

            <div class="@if($num_todays_deal > 0) col-lg-9 @else col-lg-9 @endif">
                @if (get_setting('home_slider_images') != null)
                <div class="aiz-carousel dots-inside-bottom mobile-img-auto-height" data-arrows="true" data-dots="true"
                data-autoplay="true" data-infinite="true">
                @foreach ($slider_images as $key => $value)
                <div class="carousel-box">
                    <a href="{{ json_decode(get_setting('home_slider_links'), true)[$key] }}">
                        <img class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                        src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                        data-src="{{ uploaded_asset($slider_images[$key]) }}" alt="{{ env('APP_NAME')}} promo"
                        @if(count($featured_categories)==0) height="457" @else height="315" @endif
                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';">
                    </a>
                </div>
                @endforeach
            </div>
            @endif

        </div>
        @if($num_todays_deal > 0)
        <div class="col-lg-3 d-none d-lg-block todays_deal-box">
            <div class="flash-deal-box flash-deal-box-analogue bg-white h-100 text-center">
                <h4>Today's Deal</h4>
                <div class="flash-content c-scrollbar c-height">
                    @foreach ($flashDeal as $key => $product)
                    @if ($product != null)
                    <a href="{{ route('product', $product->slug) }}" class="d-block flash-deal-item">
                        <div class="row no-gutters align-items-center">
                            <div class="col">
                                <div class="img">
                                    <img class="lazyload img-fit h-140px h-lg-80px"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($product->todays_deal_image ? $product->todays_deal_image : $product->thumbnail_img) }}"
                                    alt="{{ $product->getTranslation('name') }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                                </div>
                            </div>
                        </div>
                    </a>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>
    @if($num_todays_deal > 0)
    <div class=" py-md-0 d-block d-lg-none bg-white todays_deal_image-box px-2 shadow-sm rounded">
        <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"
        data-md-items="3" data-sm-items="2" data-rows="1" data-xs-items="1" data-arrows='true'
        data-infinite='true'>
        @foreach ($flashDeal as $key => $product)
        @if ($product != null)
        <div class="carousel-box">
            <div class="aiz-card-box border border-light rounded hov-shadow-md has-transition">
                <div class="position-relative">
                    <a href="{{ route('product', $product->slug) }}" class="d-block flash-deal-item">
                        <div class="row no-gutters align-items-center">
                            <div class="col">
                                <div class="img">
                                    <img class="lazyload img-fit h-140px h-lg-80px"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($product->todays_deal_image ? $product->todays_deal_image : $product->thumbnail_img) }}"
                                    alt="{{ $product->getTranslation('name') }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                                </div>
                            </div>
                        </div>
                    </a>

                </div>


            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
@endif
</div>
</div>

{{-- Video Banner --}}
@if ($video_banners)
<!-- video banner -->
<section class="video-banner gry-bg py-5">
    <div class="container">
        <div class="row">
            @foreach ($video_banners as $key => $value)
            <div class="col-lg-6 col-12 pb-3 pb-lg-0 mb-4">
                <div style="padding:56.25% 0 0 0;position:relative;"><iframe
                    src="{{ json_decode(get_setting('home_video_banner'), true)[$key] }}"
                    style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
                    allow="autoplay; fullscreen; picture-in-picture;" allowfullscreen></iframe>
                </div>
                <script src="https://player.vimeo.com/api/player.js"></script>
            </div>
            @endforeach


            </div>
        </div>

    </section>
    <!-- end of video banner -->
    @endif

    {{-- Flash Deal --}}

    @if($flash_deal != null && strtotime(date('Y-m-d H:i:s')) >= $flash_deal->start_date && strtotime(date('Y-m-d H:i:s'))
    <= $flash_deal->end_date)
    <section class="mb-4 white-background pt-0" style="padding-bottom: 40px;">
        <div class="light-blue py-2">
            <div class="container">
                <div class="d-flex flex-wrap align-items-baseline">
                    <h3 class="h5 fw-600 mb-0">
                        <span class="flash-border border-primary border-width-2  d-inline-block">{{
                        translate('Flash Sale') }}</span>
                    </h3>
                    <div class="aiz-count-down ml-auto ml-lg-3 align-items-center"
                    data-date="{{ date('Y/m/d H:i:s', $flash_deal->end_date) }}"></div>
                    <a href="{{ route('flash-deal-details', $flash_deal->slug) }}"
                     class="ml-auto mr-0 btn btn-primary btn-sm shadow-md w-100 w-md-auto">{{ translate('View More')
                    }}</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="py-4  py-md-3 bg-white rounded">
                

             <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4"
             data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true'
             data-infinite='true' data-dots="true">
             @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
             @if ($flash_deal_product->product != null && $flash_deal_product->product->published != 0)
             <div class="carousel-box">
                <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                    <div class="position-relative">
                        <a href="{{ route('product', $flash_deal_product->product->slug) }}" class="d-block">
                            <img class="img-fit lazyload mx-auto h-auto"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($flash_deal_product->product->thumbnail_img) }}"
                            alt="{{  $flash_deal_product->product->getTranslation('name')  }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                        </a>
                        <div class="absolute-top-right aiz-p-hov-icon">
                            <a href="javascript:void(0)"
                            onclick="addToWishList({{ $flash_deal_product->product->id }})"
                            data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}"
                            data-placement="left">
                            <i class="la la-heart-o"></i>
                        </a>
                        <a href="javascript:void(0)"
                        onclick="addToCompare({{ $flash_deal_product->product->id }})"
                        data-toggle="tooltip" data-title="{{ translate('Add to compare') }}"
                        data-placement="left">
                        <i class="las la-sync"></i>
                    </a>
                    <a href="javascript:void(0)"
                    onclick="showAddToCartModal({{ $flash_deal_product->product->id }})"
                    data-toggle="tooltip" data-title="{{ translate('Add to cart') }}"
                    data-placement="left">
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </div>
        </div>

        <div class="p-md-3 p-2">
            <div class="star-rating star-rating-sm mt-1">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0;">{{
                        renderStarRating($flash_deal_product->product->rating) }}</div>
                        <div class="col" align="right"><span class="rating-number">(0 Reviews)</span>
                        </div>
                    </div>

                </div>
                <div class="sub-cat">{{ __($flash_deal_product->product->category->name) }}</div>
                <h2 class="product-title p-0">
                    <a href="{{ route('product', $flash_deal_product->product->slug) }}"
                     class=" text-truncate">{{ __($flash_deal_product->product->name) }}</a>
                 </h2>
                 <div class="price-box">
                    @if(home_base_price($flash_deal_product->product->id) !=
                    home_discounted_base_price($flash_deal_product->product->id))
                    <del class="old-product-price strong-400">{{
                        home_base_price($flash_deal_product->product->id) }}</del>
                        @endif
                        <span class="product-price strong-600">{{
                            home_discounted_base_price($flash_deal_product->product->id) }}</span>
                        </div>
                        <div class="Product-cart-2-footer-btn text-left">
                            <div class="d-inline-block">
                                <button class="add-to-cart p-all btn" title="Add to Cart" onclick="showAddToCartModal({{ $product->id }})" tabindex="0">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="cart-name">Add To Cart</span>
                                </button>
                                <a href="javascript:void(0)" class="product-footer-a cart-compare" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="33.147" height="23.736" viewBox="0 0 33.147 23.736">
                                      <path id="Path_281" data-name="Path 281" d="M56.035,126.3H44.417v3.391H56.035v5.087L62.648,128l-6.613-6.782Zm9.911-1.7v-5.087H77.564v-3.391H65.946v-5.087l-6.613,6.782Z" transform="translate(-44.417 -111.042)" fill="#fff"></path>
                                    </svg>
                                </a>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
</section>
@endif
<!--Banner 1-->
<div class="white-bg-analogue padding_top_btn gry-bg ads-banner-section">
    <div class="container">
        <div class="row gutters-10">
            @foreach ($banner_1_imags as $key => $value)
            <div class="col-xl col-lg col-md col-sm-6">
                <div class="ad-box mb-lg-0">
                    <a href="{{ json_decode(get_setting('home_banner1_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_1_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>

{{-- Top Catagory --}}
@if (get_setting('top10_categories') != null)
<div class="topcategories-analogue padding_top_btn bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="title-box">
                    <strong>Top Categories in </strong>
                    <h3>Sales & Trending</h3>
                    <p>Last Month upto 1500+ Products Sales From this category. <br>You can choose a product from
                    here and save money.</p>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <div class="tab">
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach ($top10_categories as $key => $value)
                        @php $category = $helper->getCategory($value); @endphp
                        @if ($category)
                        <li role="presentation">
                            <a href="#cat{{$category? $category->id : ''}}"
                             aria-controls="cat{{$category ? $category->id : ''}}" role="tab"
                             data-toggle="tab" class="slick-arrow {{ $loop->first ? 'active' : ''}}">
                             <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                             data-src="{{ uploaded_asset($category->banner) }}"
                             alt="{{ $category->getTranslation('name') }}" class="img-fluid img lazyload"
                             onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';">
                             <span>{{$category->name}}</span>
                         </a>
                     </li>
                     @endif
                     @endforeach

                 </ul>
             </div>
         </div>
         <div class="col-lg-12">
            <div class="tab-content product-slider-ana">
                @foreach ($top10_categories as $key => $value)
                @php $category = $helper->getCategory($value); @endphp
                <div role="tabpanel" class="tab-pane active {{ $loop->first ? 'tab-pane-first' : ''}}"
                   id="cat{{$category ? $category->id : ''}}">
                   <div class="px-0 py-4 px-md-0 bg-white shadow-sm rounded">
                    <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4"
                    data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="2"
                    data-arrows='true' data-infinite='true'>
                    @if($category && $category->products)
                    @foreach ($category->products as $key => $product)
                    @include('frontend.partials.single_product')
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
</div>
</div>
@endif

{{-- Banner Section 2 --}}
@if (get_setting('home_banner2_images') != null)
<div class="ads-section ads-banner-section">
    <div class="">
        <div class="row gutters-10">
            @foreach ($banner_2_imags as $key => $value)
            <div class="col-xl col-lg col-md">
                <div class="mb-lg-0 ad-box">
                    <a href="{{ json_decode(get_setting('home_banner2_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_2_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
@endif

<div class="categories-analogue bg-white m-top0 p-top35 p-bottom35 m-p-top20 m-p-bottom20">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 text-right">
                <div class="tab taps-three_grp">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a href="#trending" aria-controls="trending" class="taps-three active" role="tab"
                            data-toggle="tab">Trending Now</a>
                        </li>
                        <li role="presentation">
                            <a href="#featured" class="taps-three" aria-controls="featured" role="tab"
                            data-toggle="tab">Featured Products</a>
                        </li>
                        <li role="presentation">
                            <a href="#narrival" class="taps-three" aria-controls="narrival" role="tab"
                            data-toggle="tab">New Arrival</a>
                        </li>
                        <!-- <div class="line-hover"></div> -->
                            <!--  <li role="presentation">
                            <a href="#latest" aria-controls="latest" role="tab" data-toggle="tab">Latest</a>
                        </li>      -->

                    </ul>
                </div>
            </div>

        </div>
    </div>
    @include('frontend.partials.product_two_row')


</div>

<!--Banner 1-->
<div class="white-bg-analogue  padding_top_btn gry-bg ads-banner-section" style="margin-bottom: 0;">
    <div class="container">
        <div class="row gutters-10">
            @foreach ($banner_1_imags as $key => $value)
            <div class="col-xl col-lg col-md col-sm-6">
                <div class="mb-3 mb-lg-0 ad-box">
                    <a href="{{ json_decode(get_setting('home_banner1_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_1_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>

@if(isset($topthree_bar->value))
<div class="best_selling_box bg-white white-bg-analogue padding_top_btn ">
    @include('frontend.partials.best_selling')
</div>
@endif

{{-- Banner Section 2 --}}
@if (get_setting('home_banner2_images') != null)
<div class="ads-section ads-banner-section">
    <div class="">
        <div class="row gutters-10">
            @foreach ($banner_2_imags as $key => $value)
            <div class="col-xl col-lg col-md">
                <div class="mb-3 mb-lg-0 ad-box">
                    <a href="{{ json_decode(get_setting('home_banner2_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_2_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
@endif

{{-- First  Brand --}}
@include('frontend.partials.featured_products_section')

{{-- Last brand --}}
<div class="border-t">
    @include('frontend.partials.best_selling_section')
</div>

{{-- Banner Section 2 --}}
@if (get_setting('home_banner3_images') != null)
<div class="mb-4 ads-banner-section">
    <div>
        <div class="row gutters-10">
            @php $banner_3_imags = json_decode(get_setting('home_banner3_images')); @endphp
            @foreach ($banner_3_imags as $key => $value)
            <div class="col-xl col-md-6">
                <div class="mb-3 mb-lg-0">
                    <a href="{{ json_decode(get_setting('home_banner3_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_3_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
@endif

{{-- Classified Product --}}
@if(\App\BusinessSetting::where('type', 'classified_product')->first()->value == 1)
@php
$customer_products = \App\CustomerProduct::whereIn('assured',[0,3])->where('status', '1')->where('published',
'1')->take(10)->get();
@endphp
@if (count($customer_products) > 0)
<section class="pre-owned-section bg-white p-top35 p-bottom35 m-p-top20 m-p-bottom20">
    <div class="container">
        <div class="px-2 py-4 px-md-4 py-md-3 bg-white shadow-sm rounded">
            <div class="d-flex mb-3 align-items-baseline border-bottom">
                <h3 class="h5 fw-600 mb-0">
                    <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">
                        <img src="{{static_asset('frontend/images/4.png')}}" title="Pre-owned" style="width: 30px;" alt="">
                        {{translate('Pre-Owned') }}
                    </span>
                </h3>
                <a href="{{ route('customer.products') }}" class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{
                translate('Go To Pre-Owned') }}</a>
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4"
            data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true'
            data-infinite='true'>
            @foreach ($customer_products as $key => $customer_product)
            <div class="carousel-box">
                <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                    <div class="position-relative">
                        <a href="{{ route('customer.product', $customer_product->slug) }}" class="d-block">
                            <img class="img-fit lazyload mx-auto h-200px h-md-170px h-sm-200px"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($customer_product->thumbnail_img) }}"
                            alt="{{ $customer_product->getTranslation('name') }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                        </a>
                        <div class="absolute-top-right pt-2 pl-2">
                            @if($customer_product->assured == '3')
                            <span
                            class="badge badge-inline badge-success bg-primary px-2">{{translate('Assured')}}</span>
                            @elseif($customer_product->conditon == 'new')
                            <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                            @elseif($customer_product->conditon == 'used')
                            <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="p-md-3 p-2 text-left">
                        <div class="fs-15 mb-1">
                            <span class="fw-700 text-primary">{{ single_price($customer_product->unit_price)
                            }}</span>
                        </div>
                        <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
                            <a href="{{ route('customer.product', $customer_product->slug) }}"
                             class="d-block text-reset">{{ $customer_product->getTranslation('name') }}</a>
                         </h3>
                     </div>
                 </div>
             </div>
             @endforeach
         </div>
     </div>
 </div>
</section>
@endif
@endif

{{-- Banner Section 2 --}}
@if (get_setting('home_banner3_images') != null)
<div class="mb-4 ads-banner-section">
    <div class="">
        <div class="row gutters-10">
            @php $banner_3_imags = json_decode(get_setting('home_banner3_images')); @endphp
            @foreach ($banner_3_imags as $key => $value)
            <div class="col-xl col-md-6">
                <div class="mb-3 mb-lg-0">
                    <a href="{{ json_decode(get_setting('home_banner3_links'), true)[$key] }}"
                    class="d-block text-reset">
                    <img src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                    data-src="{{ uploaded_asset($banner_3_imags[$key]) }}" alt="{{ env('APP_NAME') }} promo"
                    class="img-fluid lazyload">
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
@endif

<!--Bland Logo-->

<div class="brand_logo_listing bg-white p-top35 p-bottom35 m-p-top20 m-p-bottom20">
    <div class="container">
        <div class="">
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="12" data-xl-items="10"
            data-lg-items="8" data-md-items="6" data-sm-items="4" data-rows="1" data-xs-items="4"
            data-arrows='true' data-infinite='true'>
            @php $top10_brands = json_decode(get_setting('top10_brands')); @endphp
            @foreach ($top10_brands as $key => $value)
            @php $brand = $helper->getBrand($value); @endphp
            @if ($brand != null)
            <div class="carousel-box">
                <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                    <div class="position-relative">
                        <a href="{{ route('products.brand', $brand->slug) }}" class="d-block">
                            <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($brand->logo) }}"
                            alt="{{ $brand->getTranslation('name') }}" class="img-fluid img lazyload h-60px"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';">
                        </a>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>

    </div>

</div>
</div>


{{-- Top 10 categories and Brands --}}
<section class="">
    <div class="container">
        <!-- <div class="row gutters-10">
            @if (get_setting('top10_categories') != null)
            <div class="col-lg-6">
                <div class="d-flex mb-3 align-items-baseline border-bottom">
                    <h3 class="h5 fw-700 mb-0">
                        <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">{{ translate('Top 10 Categories') }}</span>
                    </h3>
                    <a href="{{ route('categories.all') }}" class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{ translate('View All Categories') }}</a>
                </div>
                <div class="row gutters-5">
                    @php $top10_categories = json_decode(get_setting('top10_categories')); @endphp
            @foreach ($top10_categories as $key => $value)
                @php $category = \App\Category::find($value); @endphp
                @if ($category != null)
                    <div class="col-sm-6">
                        <a href="{{ route('products.category', $category->slug) }}" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                            <div class="row align-items-center no-gutters">
                                <div class="col-3 text-center">
                                    <img
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($category->banner) }}"
                                    alt="{{ $category->getTranslation('name') }}"
                                    class="img-fluid img lazyload h-60px"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';"
                                    >
                                </div>
                                <div class="col-7">
                                    <div class="text-truncat-2 pl-3 fs-14 fw-600 text-left">{{ $category->getTranslation('name') }}</div>
                                </div>
                                <div class="col-2 text-center">
                                    <i class="la la-angle-right text-primary"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
            @endforeach
                </div>
            </div>
            @endif
        @if (get_setting('top10_categories') != null)
            <div class="col-lg-6">
                <div class="d-flex mb-3 align-items-baseline border-bottom">
                    <h3 class="h5 fw-700 mb-0">
                        <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">{{ translate('Top 10 Brands') }}</span>
                    </h3>
                    <a href="{{ route('brands.all') }}" class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{ translate('View All Brands') }}</a>
                </div>
                <div class="row gutters-5">
                    @php $top10_brands = json_decode(get_setting('top10_brands')); @endphp
            @foreach ($top10_brands as $key => $value)
                @php $brand = \App\Brand::find($value); @endphp
                @if ($brand != null)
                    <div class="col-sm-6">
                        <a href="{{ route('products.brand', $brand->slug) }}" class="bg-white border d-block text-reset rounded p-2 hov-shadow-md mb-2">
                            <div class="row align-items-center no-gutters">
                                <div class="col-4 text-center">
                                    <img
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($brand->logo) }}"
                                    alt="{{ $brand->getTranslation('name') }}"
                                    class="img-fluid img lazyload h-60px"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';"
                                    >
                                </div>
                                <div class="col-6">
                                    <div class="text-truncate-2 pl-3 fs-14 fw-600 text-left">{{ $brand->getTranslation('name') }}</div>
                                </div>
                                <div class="col-2 text-center">
                                    <i class="la la-angle-right text-primary"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endif
            @endforeach
                </div>
            </div>
            @endif
        </div> -->
    </div>
</section>

@endsection

@section('script')
<script>
    $(document).ready(function () {

    });
</script>
@endsection