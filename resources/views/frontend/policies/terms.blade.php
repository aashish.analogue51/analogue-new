@extends('frontend.layouts.app')

@section('content')
@php
    $terms =  \App\Page::where('type', 'terms_conditions_page')->first();
@endphp
<section class="pt-4 background_primary ">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 background_primary d-none h4">{{ $terms->getTranslation('title') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-80">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('terms') }}">{{ translate('Terms & conditions') }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class=" background_primary">
    <div class="container w-lg-75">
        <div class="p-5 fs-18 rounded fw-600 overflow-hidden mw-100 text-left background_primary">
            @php
                echo $terms->getTranslation('content');
            @endphp
        </div>
    </div>
</section>
@endsection
