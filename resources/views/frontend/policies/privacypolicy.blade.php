@extends('frontend.layouts.app')

@section('content')
@php
    $privacy_policy =  \App\Page::where('type', 'privacy_policy_page')->first();
@endphp
<section class="pt-4 background_primary">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <h1 class="fw-600 background_primary d-none h4">{{ $privacy_policy->getTranslation('title') }}</h1>
            </div>
            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-end">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('privacypolicy') }}">"{{ translate('Privacy Policy') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="background_primary">
    <div class="container w-lg-75">
        <div class="p-5 fs-18 rounded fw-600 overflow-hidden mw-100 text-left background_primary" style="font-size: 28px;">
          
            @php
                echo $privacy_policy->getTranslation('content');
            @endphp
        </div>
    </div>
</section>
@endsection
