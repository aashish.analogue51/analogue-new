@extends('frontend.layouts.app')

@if (isset($category_id))
@php
$meta_title = \App\Category::find($category_id)->meta_title;
$meta_description = \App\Category::find($category_id)->meta_description;
@endphp
@elseif (isset($brand_id))
@php
$meta_title = \App\Brand::find($brand_id)->meta_title;
$meta_description = \App\Brand::find($brand_id)->meta_description;
@endphp
@else
@php
$meta_title         = get_setting('meta_title');
$meta_description   = get_setting('meta_description');
@endphp
@endif

@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop

@section('meta')
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $meta_title }}">
<meta itemprop="description" content="{{ $meta_description }}">

<!-- Twitter Card data -->
<meta name="twitter:title" content="{{ $meta_title }}">
<meta name="twitter:description" content="{{ $meta_description }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $meta_title }}" />
<meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')
<!-- preowned banner -->
<div class="home-banner-area white-background mb-4 pt-3">
    <div class="container">
        <div class="row gutters-10 position-relative">

            <div class="@if($num_todays_deal > 0) col-lg-9 @else col-lg-9 @endif">
                @if (get_setting('home_slider_images') != null)
                <div class="aiz-carousel dots-inside-bottom mobile-img-auto-height" data-arrows="true" data-dots="true" data-autoplay="true" data-infinite="true">
                    @foreach ($slider_images as $key => $value)
                    <div class="carousel-box">
                        <a href="{{ json_decode(get_setting('home_slider_links'), true)[$key] }}">
                            <img
                            class="d-block mw-100 lazyload img-fit rounded shadow-sm"
                            src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                            data-src="{{ uploaded_asset($slider_images[$key]) }}"
                            alt="{{ env('APP_NAME')}} promo"
                            @if(count($featured_categories) == 0)
                            height="457"
                            @else
                            height="315"
                            @endif
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';"
                            >
                        </a>
                    </div>
                    @endforeach
                </div>
                @endif

            </div>
            <!-- preown login -->
            <div class="col-lg-3 d-lg-block">
                <div class="bg-soft-secondary h-100 p-2">
                    <div class="">
                        <div class="heading_form pt-3">
                            <h2 class="fs-26 text-center pt-2">Welcome to Analogue Mall !</h2>
                            <p class="px-2 pt-3 text-center">
                                Analogue Mall is perfect solution which helps to list your products for free and sell it
                                by connecting buyers and sellers.
                            </p>
                        </div>

                        <div class="text-center pt-3">
                            <i class="la la-chevron-circle-right text_color
                            "></i>
                            <a href="#">Post your Ad</a>
                            <p>It's Fast, Easy & Free!</p>
                        </div>
                        <div class="text-center pt-5  mb-4">
                            <a href="#" class="py-1 border text-white bg_form opacity-80 px-3 py-2 ">Register Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login end -->
        </div>
    </div>
</div>

<!-- Ads section one  -->
<section class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(1110x150).png') }}" alt="adsbanner1100x150">
            </div>
        </div>
    </div>
</section>
<!-- End of ads section one -->
<!-- end of preowned banner -->
<!-- feature ads -->
@if(filter_products(\App\Product::where('published', 1)->where('featured', '1'))->limit(12)->get())
<section class="bg-white p-top35 p-bottom35">
    <div class="container">
        <div class="bg-white shadow-sm rounded">
            <div class="d-flex mb-3 align-items-baseline border-bottom">
                <h3 class="h5 fw-700 mb-0">
                    <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">{{ translate('Featured Ads') }}</span>
                </h3>
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach (filter_products(\App\Product::where('published', 1)->where('featured', '1'))->limit(12)->get() as $key => $product)
                <div class="carousel-box">
                    <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                        <div class="position-relative">
                            <a href="{{ route('product', $product->slug) }}" class="d-block">
                                <img
                                class="img-fit lazyload mx-auto h-auto"
                                src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                alt="{{  $product->getTranslation('name')  }}"
                                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                >
                            </a>
                                <!-- <div class="absolute-top-right aiz-p-hov-icon">
                                    <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                                        <i class="la la-heart-o"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                                        <i class="las la-sync"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                                        <i class="las la-shopping-cart"></i>
                                    </a>
                                </div> -->
                            </div>

                            <div class="p-md-3 p-2">
                                <!-- <div class="star-rating star-rating-sm mt-1">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0;">{{ renderStarRating($product->rating) }}</div>
                                        <div class="col" align="right"><span class="rating-number">(0 Reviews)</span></div>                                                    
                                    </div>

                                </div> -->
                                <div class="sub-cat">{{ __($product->category->name) }}</div>
                                <h2 class="product-title p-0">
                                    <a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
                                </h2>
                                <!-- <div class="price-box">
                                    @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                                    @endif
                                    <span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
                                </div> -->
                                <!-- <div class="Product-cart-2-footer-btn">
                                    <button class="add-to-cart p-all" title="Add to Cart" onclick="addToCart()" >
                                        <i class="la la-shopping-cart"></i> Add To Cart
                                    </button>
                                    <button class="add-to-compare btn" title="Quick View" onclick="showAddToCartModal({{ $product->id }})" tabindex="0">
                                        <i class="la la-eye opacity-80"></i>
                                    </button>

                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- second ads banner -->
    <section class="bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(1110x150).png')}}" alt="adsbanner1100x150">
                </div>
            </div>
        </div>
    </section>
    <!-- small ads banner 3 -->

    <!-- end of small ads banner 3 -->
    <!-- end of second ads banner -->
    <!-- end of feature ads -->
    <!-- Tab preowned product -->
    <section class="bg-white">
        <div class="pl-4 container mt-3 mb-1">
            <ul class="breadcrumb bg-soft-secondary pl-4">
                <li class="breadcrumb-item opacity-80">
                    <a class="text-reset fs-18" href="{{ route('home') }}">{{ translate('Home')}}</a>
                </li>
                @if(!isset($category_id))
                <li class="breadcrumb-item fw-600  text-dark">
                    <a class="text-reset fs-18" href="{{ route('customer.products') }}">{{ translate('All Categories')}}</a>
                </li>
                @else
                <li class="breadcrumb-item opacity-80">
                    <a class="text-reset" href="{{ route('customer.products') }}">{{ translate('All Categories')}}</a>
                </li>
                @endif
                @if(isset($category_id))
                <li class="text-dark fw-600 breadcrumb-item">
                    <a class="text-reset text-primary" href="{{ route('customer_products.category', \App\Category::find($category_id)->slug) }}">"{{ \App\Category::find($category_id)->getTranslation('name') }}"</a>
                </li>
                @endif
            </ul>
        </div>

    </section>
    <section>
        <section class="bg-white">
            <div class="container">

              <div class="row d-flex">

          <!-- <div class="col-xl-3 d-flex justify-content-center pt-5 mt-1">
               <h3 class="fw-600">New Listing<span class="text_color">
                   Ads
               </span></h3>
           </div> -->
           <div class="col-xl-3 col-12 order-2 order-lg-1 row text-left pb-3 pt-3">

            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(250x180).png')}}" alt="">
            </div>
            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(250x180).png')}}" alt="">
            </div>
            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(250x180).png')}}" alt="">
            </div>
            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(250x180).png')}}" alt="">
            </div>
            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(250x180).png')}}" alt="">
            </div>
            <div class="col-xl-12 col-3 pb-2">
                <img class="img-fluid" src="frontend/images/adsbanner(250x180).png" alt="">
            </div>


        </div>
        <div class="col-xl-9 order-1 order-lg-2">
          <hr>
          <ul class="nav nav-pills  mb-3 justify-content-between" id="pills-tab" role="tablist">
            <li class="nav-item  ">
              <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-category-1" role="tab" aria-controls="pills-home" aria-selected="true">Demo Category 1</a>
          </li>
          <li class="nav-item">
              <a class="nav-link " id="pills-profile-tab" data-toggle="pill" href="#pills-category-2" role="tab" aria-controls="pills-profile" aria-selected="false">Demo Category 2</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-category-3" role="tab" aria-controls="pills-contact" aria-selected="false">Demo Category 3</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-category-4" role="tab" aria-controls="pills-contact" aria-selected="false">Women Clothing & Fashion</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-category-5" role="tab" aria-controls="pills-contact" aria-selected="false">Mans Fashion</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-category-6" role="tab" aria-controls="pills-contact" aria-selected="false">Clothing</a>
        </li>
    </ul>
    <hr>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-category-1" role="tabpanel" aria-labelledby="pills-home-tab">           
            <div class="container sm-px-0">
                <form class="" id="search-form" action="" method="GET">
                    <div class="row d-flex">
                        <div class="col-xl-12 oeder-xl-2 order-1">

                            @isset($category_id)
                            <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                            @endisset
                            <div class="text-left">
                               <div class="d-flex">
                                <div class="form-group w-200px pl-3 pl-md-0">
                                    <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                        <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                        <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                        <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                        <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                    </select>
                                </div>
                                 <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Assured')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="assured" onchange="filter()">
                                        <option value="">{{ translate('All Type')}}</option>
                                        <option value="3" @isset($assured) @if ($assured == 3) selected @endif @endisset>{{ translate('assured')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                        <option value="">{{ translate('All Type')}}</option>
                                        <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                        <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                        <option value="">{{ translate('All Brands')}}</option>
                                        @foreach (\App\Brand::all() as $brand)
                                        <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                    <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                        <i class="la la-filter la-2x"></i>
                                    </button>
                                </div>
                            </div> 
                        </div>
                        <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                            @foreach ($customer_products as $key => $product)
                            <div class="col mb-2">
                                <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                    <div class="position-relative">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                            <img
                                            class="img-fit lazyload mx-auto h-140px h-md-210px"
                                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                            data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                            alt="{{  $product->getTranslation('name')  }}"
                                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                            >
                                        </a>
                                        <div class="absolute-top-right pt-2 pr-2">
                                            @if($product->assured == '3')
                                            <span class="badge badge-inline badge-success bg-primary px-2">{{translate('Assured')}}</span>
                                            @elseif($product->conditon == 'new')
                                            <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                            @elseif($product->conditon == 'used')
                                            <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="p-md-3 p-2 text-left">
                                        <div class="fs-15">
                                            <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                        </div>
                                        <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                            <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                            {{ $customer_products->links() }}
                        </div>

                    </div>
                </div>

            </form>
        </div>
        
    </div>
    <div class="tab-pane fade" id="pills-category-2" role="tabpanel" aria-labelledby="pills-profile-tab">
        <section class="mb-4">      
            <div class="container sm-px-0">
                <form class="" id="search-form" action="" method="GET">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                                <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                                <div class="collapse-sidebar c-scrollbar-light text-left">
                                    <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                        <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                        <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                            <i class="las la-times la-2x"></i>
                                        </button>
                                    </div>
                                    {{--    <div class="bg-white shadow-sm rounded mb-3 text-left">
                                        <div class="fs-15 fw-600 p-3 border-bottom">
                                            {{ translate('Categories')}}
                                        </div>
                                        <div class="p-3">
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>

                            

                            @isset($category_id)
                            <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                            @endisset
                            <div class="text-left">
                               <div class="d-flex">
                                <div class="form-group w-200px pl-3 pl-md-0">
                                    <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                        <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                        <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                        <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                        <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                        <option value="">{{ translate('All Type')}}</option>
                                        <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                        <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                        <option value="">{{ translate('All Brands')}}</option>
                                        @foreach (\App\Brand::all() as $brand)
                                        <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                    <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                        <i class="la la-filter la-2x"></i>
                                    </button>
                                </div>
                            </div> 
                        </div>
                        <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                            @foreach ($customer_products as $key => $product)
                            <div class="col mb-2">
                                <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                    <div class="position-relative">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                            <img
                                            class="img-fit lazyload mx-auto h-140px h-md-210px"
                                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                            data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                            alt="{{  $product->getTranslation('name')  }}"
                                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                            >
                                        </a>
                                        <div class="absolute-top-right pt-2 pr-2">
                                            @if($product->conditon == 'new')
                                            <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                            @elseif($product->conditon == 'used')
                                            <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="p-md-3 p-2 text-left">
                                        <div class="fs-15">
                                            <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                        </div>
                                        <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                            <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                            {{ $customer_products->links() }}
                        </div>

                    </div>
                </div>

            </form>
        </div>
    </section>
</div>
<div class="tab-pane fade" id="pills-category-3" role="tabpanel" aria-labelledby="pills-profile-tab">
    <section class="mb-4">      
        <div class="container sm-px-0">
            <form class="" id="search-form" action="" method="GET">
                <div class="row">











                    <div class="col-xl-12">


                        <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                            <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                            <div class="collapse-sidebar c-scrollbar-light text-left">
                                <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                    <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                    <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                        <i class="las la-times la-2x"></i>
                                    </button>
                                </div>
                                {{--    <div class="bg-white shadow-sm rounded mb-3 text-left">
                                    <div class="fs-15 fw-600 p-3 border-bottom">
                                        {{ translate('Categories')}}
                                    </div>
                                    <div class="p-3">
                                        <div class="row">

                                        </div>

                                    </div>

                                </div> --}}
                            </div>
                        </div>


                        @isset($category_id)
                        <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        <div class="text-left">
                           <div class="d-flex">
                            <div class="form-group w-200px pl-3 pl-md-0">
                                <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                    <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                    <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                    <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                    <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                    <option value="">{{ translate('All Type')}}</option>
                                    <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                    <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                    <option value="">{{ translate('All Brands')}}</option>
                                    @foreach (\App\Brand::all() as $brand)
                                    <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                    <i class="la la-filter la-2x"></i>
                                </button>
                            </div>
                        </div> 
                    </div>
                    <div class="row gutters-5 row-cols-xxl-3 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                        @foreach ($customer_products as $key => $product)
                        <div class="col mb-2">
                            <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                <div class="position-relative">
                                    <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-auto"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                    <div class="absolute-top-right pt-2 pr-2">
                                        @if($product->conditon == 'new')
                                        <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                        @elseif($product->conditon == 'used')
                                        <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="p-md-3 p-2 text-left">
                                    <div class="fs-15">
                                        <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                    </div>
                                    <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                        {{ $customer_products->links() }}
                    </div>

                </div>
            </div>

        </form>
    </div>
</section>
</div>
<div class="tab-pane fade" id="pills-category-4" role="tabpanel" aria-labelledby="pills-profile-tab">
    <section class="mb-4">      
        <div class="container sm-px-0">
            <form class="" id="search-form" action="" method="GET">
                <div class="row">







                    <div class="col-xl-12">

                        <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                            <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                            <div class="collapse-sidebar c-scrollbar-light text-left">
                                <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                    <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                    <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                        <i class="las la-times la-2x"></i>
                                    </button>
                                </div>
                                {{--    <div class="bg-white shadow-sm rounded mb-3 text-left">
                                    <div class="fs-15 fw-600 p-3 border-bottom">
                                        {{ translate('Categories')}}
                                    </div>
                                    <div class="p-3">
                                        <div class="row">

                                        </div>

                                    </div>

                                </div> --}}
                            </div>
                        </div>


                        @isset($category_id)
                        <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        <div class="text-left">
                           <div class="d-flex">
                            <div class="form-group w-200px pl-3 pl-md-0">
                                <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                    <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                    <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                    <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                    <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                    <option value="">{{ translate('All Type')}}</option>
                                    <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                    <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                    <option value="">{{ translate('All Brands')}}</option>
                                    @foreach (\App\Brand::all() as $brand)
                                    <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                    <i class="la la-filter la-2x"></i>
                                </button>
                            </div>
                        </div> 
                    </div>
                    <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                        @foreach ($customer_products as $key => $product)
                        <div class="col mb-2">
                            <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                <div class="position-relative">
                                    <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-140px h-md-210px"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                    <div class="absolute-top-right pt-2 pr-2">
                                        @if($product->conditon == 'new')
                                        <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                        @elseif($product->conditon == 'used')
                                        <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="p-md-3 p-2 text-left">
                                    <div class="fs-15">
                                        <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                    </div>
                                    <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                        {{ $customer_products->links() }}
                    </div>

                </div>
            </div>

        </form>
    </div>
</section>
</div>
<div class="tab-pane fade" id="pills-category-5" role="tabpanel" aria-labelledby="pills-profile-tab">
    <section class="mb-4">      
        <div class="container sm-px-0">
            <form class="" id="search-form" action="" method="GET">
                <div class="row">






                    <div class="col-xl-12">

                        <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                            <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                            <div class="collapse-sidebar c-scrollbar-light text-left">
                                <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                    <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                    <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                        <i class="las la-times la-2x"></i>
                                    </button>
                                </div>
                                {{--    <div class="bg-white shadow-sm rounded mb-3 text-left">
                                    <div class="fs-15 fw-600 p-3 border-bottom">
                                        {{ translate('Categories')}}
                                    </div>
                                    <div class="p-3">
                                        <div class="row">

                                        </div>

                                    </div>

                                </div> --}}
                            </div>
                        </div>



                        @isset($category_id)
                        <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        <div class="text-left">
                           <div class="d-flex">
                            <div class="form-group w-200px pl-3 pl-md-0">
                                <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                    <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                    <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                    <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                    <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                    <option value="">{{ translate('All Type')}}</option>
                                    <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                    <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                    <option value="">{{ translate('All Brands')}}</option>
                                    @foreach (\App\Brand::all() as $brand)
                                    <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                    <i class="la la-filter la-2x"></i>
                                </button>
                            </div>
                        </div> 
                    </div>
                    <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                        @foreach ($customer_products as $key => $product)
                        <div class="col mb-2">
                            <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                <div class="position-relative">
                                    <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-140px h-md-210px"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                    <div class="absolute-top-right pt-2 pr-2">
                                        @if($product->conditon == 'new')
                                        <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                        @elseif($product->conditon == 'used')
                                        <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="p-md-3 p-2 text-left">
                                    <div class="fs-15">
                                        <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                    </div>
                                    <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                        {{ $customer_products->links() }}
                    </div>

                </div>
            </div>

        </form>
    </div>
</section>
</div>
<div class="tab-pane fade" id="pills-category-6" role="tabpanel" aria-labelledby="pills-profile-tab">
    <section class="mb-4">      
        <div class="container sm-px-0">
            <form class="" id="search-form" action="" method="GET">
                <div class="row">






                    <div class="col-xl-12">
                        <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                            <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                            <div class="collapse-sidebar c-scrollbar-light text-left">
                                <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                    <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                    <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                        <i class="las la-times la-2x"></i>
                                    </button>
                                </div>
                                {{--    <div class="bg-white shadow-sm rounded mb-3 text-left">
                                    <div class="fs-15 fw-600 p-3 border-bottom">
                                        {{ translate('Categories')}}
                                    </div>
                                    <div class="p-3">
                                        <div class="row">

                                        </div>

                                    </div>

                                </div> --}}
                            </div>
                        </div>



                        @isset($category_id)
                        <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        <div class="text-left">
                           <div class="d-flex">
                            <div class="form-group w-200px pl-3 pl-md-0">
                                <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                    <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                    <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                    <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                    <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                    <option value="">{{ translate('All Type')}}</option>
                                    <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                    <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                </select>
                            </div>
                            <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                    <option value="">{{ translate('All Brands')}}</option>
                                    @foreach (\App\Brand::all() as $brand)
                                    <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                    <i class="la la-filter la-2x"></i>
                                </button>
                            </div>
                        </div> 
                    </div>
                    <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                        @foreach ($customer_products as $key => $product)
                        <div class="col mb-2">
                            <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                <div class="position-relative">
                                    <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-140px h-md-210px"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                    <div class="absolute-top-right pt-2 pr-2">
                                        @if($product->conditon == 'new')
                                        <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                        @elseif($product->conditon == 'used')
                                        <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="p-md-3 p-2 text-left">
                                    <div class="fs-15">
                                        <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                    </div>
                                    <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="aiz-pagination aiz-pagination-center mt-4 mb-2">
                        {{ $customer_products->links() }}
                    </div>

                </div>
            </div>

        </form>
    </div>
</section>
</div>
</div>
</div>

</div>



</div>
</section>
<!-- second ads banner -->
<section class="bg-white pt-0 pt-xl-3">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(1110x150).png')}}" alt="adsbanner1100x150">
            </div>
        </div>
    </div>
</section>
<!-- small ads banner 3 -->
<section class="bg-white">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-3">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(285x260).png')}}" alt="adsbanner1100x150">

            </div>
            <div class="col-3">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(285x260).png')}}" alt="adsbanner1100x150">

            </div>
            <div class="col-3">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(285x260).png')}}" alt="adsbanner1100x150">

            </div>
            <div class="col-3">
                <img class="img-fluid" src="{{ static_asset('frontend/images/adsbanner(285x260).png')}}" alt="adsbanner1100x150">

            </div>
        </div>
    </div>
</section>


<!-- end of tab preowned product -->
<!-- previous code start-->
{{--<div class="container">
    <ul class="breadcrumb bg-transparent">
        <li class="breadcrumb-item opacity-80">
            <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
        </li>
        @if(!isset($category_id))
        <li class="breadcrumb-item fw-600  text-dark">
            <a class="text-reset" href="{{ route('customer.products') }}">{{ translate('All Categories')}}</a>
        </li>
        @else
        <li class="breadcrumb-item opacity-80">
            <a class="text-reset" href="{{ route('customer.products') }}">{{ translate('All Categories')}}</a>
        </li>
        @endif
        @if(isset($category_id))
        <li class="text-dark fw-600 breadcrumb-item">
            <a class="text-reset text-primary" href="{{ route('customer_products.category', \App\Category::find($category_id)->slug) }}">"{{ \App\Category::find($category_id)->getTranslation('name') }}"</a>
        </li>
        @endif
    </ul>
</div> --}}


<!-- previous section -->
{{--  <section class="mb-4 bg-white">      
    <div class="container sm-px-0">
        <form class="" id="search-form" action="" method="GET">
            <div class="row">
                <div class="col-xl-3 side-filter d-xl-block">
                    <div class="aiz-filter-sidebar collapse-sidebar-wrap sidebar-xl sidebar-right z-1035">
                        <div class="overlay overlay-fixed dark c-pointer" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" data-same=".filter-sidebar-thumb"></div>
                        <div class="collapse-sidebar c-scrollbar-light text-left">
                            <div class="d-flex d-xl-none justify-content-between align-items-center pl-3 border-bottom">
                                <h3 class="h6 mb-0 fw-600">{{ translate('Filters') }}</h3>
                                <button type="button" class="btn btn-sm p-2 filter-sidebar-thumb" data-toggle="class-toggle" data-target=".aiz-filter-sidebar" type="button">
                                    <i class="las la-times la-2x"></i>
                                </button>
                            </div>
                            <div class="bg-white shadow-sm rounded mb-3 text-left">
                                <div class="fs-15 fw-600 p-3 border-bottom">
                                    {{ translate('Categories')}}
                                </div>
                                <div class="p-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="list-unstyled">
                                                @if (!isset($category_id))
                                                @foreach (\App\Category::where('level', 0)->get() as $category)
                                                <li class="mb-2 ml-2">
                                                    <a class="text-reset fs-14" href="{{ route('customer_products.category', $category->slug) }}">{{ $category->getTranslation('name') }}</a>
                                                </li>
                                                @endforeach
                                                @else
                                                <li class="mb-2">
                                                    <a class="text-reset fs-14 fw-600" href="{{ route('customer.products') }}">
                                                        <i class="las la-angle-left"></i>
                                                        {{ translate('All Categories')}}
                                                    </a>
                                                </li>
                                                @if (\App\Category::find($category_id)->parent_id != 0)
                                                <li class="mb-2">
                                                    <a class="text-reset fs-14 fw-600" href="{{ route('customer_products.category', \App\Category::find(\App\Category::find($category_id)->parent_id)->slug) }}">
                                                        <i class="las la-angle-left"></i>
                                                        {{ \App\Category::find(\App\Category::find($category_id)->parent_id)->getTranslation('name') }}
                                                    </a>
                                                </li>
                                                @endif
                                                <li class="mb-2">
                                                    <a class="text-reset fs-14 fw-600" href="{{ route('customer_products.category', \App\Category::find($category_id)->slug) }}">
                                                        <i class="las la-angle-left"></i>
                                                        {{ \App\Category::find($category_id)->getTranslation('name') }}
                                                    </a>
                                                </li>
                                                @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($category_id) as $key => $id)
                                                <li class="ml-4 mb-2">
                                                    <a class="text-reset fs-14" href="{{ route('customer_products.category', \App\Category::find($id)->slug) }}">{{ \App\Category::find($id)->getTranslation('name') }}</a>
                                                </li>
                                                @endforeach
                                                @endif
                                            </ul>

                                        </div>
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9">

                        <!-- <ul class="breadcrumb bg-transparent p-0">
                            <li class="breadcrumb-item opacity-90">
                                <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                            </li>
                            @if(!isset($category_id))
                                <li class="breadcrumb-item fw-600  text-dark">
                                    <a class="text-reset" href="{{ route('customer.products') }}">"{{ translate('All Categories')}}"</a>
                                </li>
                            @else
                                <li class="breadcrumb-item opacity-90">
                                    <a class="text-reset" href="{{ route('customer.products') }}">{{ translate('All Categories')}}</a>
                                </li>
                            @endif
                            @if(isset($category_id))
                                <li class="text-dark fw-600 breadcrumb-item">
                                    <a class="text-reset" href="{{ route('customer_products.category', \App\Category::find($category_id)->slug) }}">"{{ \App\Category::find($category_id)->getTranslation('name') }}"</a>
                                </li>
                            @endif
                        </ul> -->

                        @isset($category_id)
                        <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        <div class="text-left">
                            <div class="d-flex">
                                <div class="form-group w-200px pl-3 pl-md-0">
                                    <label class="mb-0 opacity-90">{{ translate('Sort by')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="sort_by" onchange="filter()">
                                        <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                        <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                        <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                        <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-auto mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Condition')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" name="condition" onchange="filter()">
                                        <option value="">{{ translate('All Type')}}</option>
                                        <option value="new" @isset($condition) @if ($condition == 'new') selected @endif @endisset>{{ translate('New')}}</option>
                                        <option value="used" @isset($condition) @if ($condition == 'used') selected @endif @endisset>{{ translate('Used')}}</option>
                                    </select>
                                </div>
                                <div class="form-group ml-2 mr-0 w-200px d-none d-md-block">
                                    <label class="mb-0 opacity-90">{{ translate('Brands')}}</label>
                                    <select class="form-control form-control-sm aiz-selectpicker" data-live-search="true" name="brand" onchange="filter()">
                                        <option value="">{{ translate('All Brands')}}</option>
                                        @foreach (\App\Brand::all() as $brand)
                                        <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->getTranslation('name') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="d-xl-none ml-auto ml-md-3 mr-0 form-group align-self-end">
                                    <button type="button" class="btn btn-icon p-0" data-toggle="class-toggle" data-target=".aiz-filter-sidebar">
                                        <i class="la la-filter la-2x"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row gutters-5 row-cols-xxl-4 row-cols-xl-3 row-cols-lg-4 row-cols-md-3 row-cols-2">
                            @foreach ($customer_products as $key => $product)
                            <div class="col mb-2">
                                <div class="aiz-card-box border border-light rounded shadow-sm hov-shadow-md h-100 has-transition bg-white">
                                    <div class="position-relative">
                                        <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                            <img
                                            class="img-fit lazyload mx-auto h-140px h-md-210px"
                                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                            data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                            alt="{{  $product->getTranslation('name')  }}"
                                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                            >
                                        </a>
                                        <div class="absolute-top-right pt-2 pr-2">
                                            @if($product->conditon == 'new')
                                            <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                            @elseif($product->conditon == 'used')
                                            <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="p-md-3 p-2 text-left">
                                        <div class="fs-15">
                                            <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                                        </div>
                                        <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0">
                                            <a href="{{ route('customer.product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="aiz-pagination aiz-pagination-center mt-4">
                            {{ $customer_products->links() }}
                        </div>

                    </div>
                </div>
                <!-- <div class="pt-3 container">
                    <div class="row">
                        <div class="col-xl-3 row-grid">

                        
                        <div class="col-12 pb-2">
                            <img class="img-fluid" src="frontend/images/ban1.jpg" alt="">
                        </div>
                        <div class="col-12 pb-2">
                            <img class="img-fluid" src="frontend/images/ban2.jpg" alt="">
                        </div>
                        <div class="col-12 pb-2">
                            <img class="img-fluid" src="frontend/images/ban1.jpg" alt="">
                        </div>
                        <div class="col-12 pb-2">
                            <img class="img-fluid" src="frontend/images/ban2.jpg" alt="">
                        </div>
                        <div class="col-12 pb-2">
                            <img class="img-fluid" src="frontend/images/ban1.jpg" alt="">
                        </div>
                    </div>
                    </div>
                </div> -->
            </form>
        </div>
    </section> --}}
    <!-- end of previous -->
    <!-- end of previous code -->

    @endsection

    @section('script')
    <script type="text/javascript">
        function filter(){
            $('#search-form').submit();
        }
    </script>
    @endsection
