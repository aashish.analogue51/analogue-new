@extends('frontend.layouts.app')

@section('content')

<section class="py-5 bg-white">
    <div class="container">
        <div class="d-flex align-items-start">
            @include('frontend.inc.user_side_nav')

            <div class="aiz-user-panel">
                <div class="aiz-titlebar mt-2 mb-4">
                    <div class="row align-items-start">
                        <div class="col-md-6">
                            <h1 class="h3 fw-600 text-uppercase text_color">{{ translate('Add Your Product') }}</h1>
                        </div>
                    </div>
                </div>
                <form class="" action="{{route('customer_products.store')}}" method="POST" enctype="multipart/form-data"
                    id="choice_form">
                    @csrf
                    <input type="hidden" name="added_by" value="{{ Auth::user()->user_type }}">
                    <input type="hidden" name="status" value="available">
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('General')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Product Name')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name"
                                        placeholder="{{ translate('Product Name')}}" required>
                                    <strong class="text-danger">Warning:<span>
                                            <ul class="fw-500">
                                                <li>
                                                    DO NOT post multiple product for same product/service, it will be
                                                    deleted and account blocked.
                                                </li>
                                                <li>
                                                    Spam product, refferal based ads and illegal ads will be deleted.
                                                </li>
                                                <li>
                                                    Wrongly categorized ad (ads posted in irrelevant category) will be
                                                    deleted. </li>
                                                <li>
                                                    Do not write price of product/service or your phone number in ad
                                                    title. </li>
                                            </ul>
                                            <a href="#" class="fw-600"><u>Click here to see product postings rules in
                                                    detail..</u></a>
                                        </span></strong>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Product Category')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <select class="form-control aiz-selectpicker"
                                        data-placeholder="{{ translate('Select a Category')}}" id="categories"
                                        name="category_id" data-live-search="true" required>
                                        @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->getTranslation('name') }}
                                        </option>
                                        @foreach ($category->childrenCategories as $childCategory)
                                        @include('categories.child_category', ['child_category' => $childCategory])
                                        @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Product Brand')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <select class="form-control aiz-selectpicker"
                                        data-placeholder="{{ translate('Select a brand')}}" data-live-search="true"
                                        id="brands" name="brand_id">
                                        <option value=""></option>
                                        @foreach (\App\Brand::all() as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->getTranslation('name') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Product Unit')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('Product unit')}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Condition')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <select class="form-control selectpicker"
                                        data-placeholder="{{ translate('Select a condition')}}" id="conditon"
                                        name="conditon" required>
                                        <option value="new">{{ translate('New')}}</option>
                                        <option value="used">{{ translate('Used')}}</option>
                                    </select>
                                </div>
                            </div>
                            <!-- used for -->
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Used For(Year or Month)')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 6 months')}}">
                                </div>
                            </div>
                            <!-- end of used for -->
                            <!-- ownership document -->
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Ownership Document')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <select class="form-control selectpicker"
                                        data-placeholder="{{ translate('Select a condition')}}" id="document"
                                        name="document">
                                        <option value="sales-bill">{{ translate('Sales bill of my shop
                                            ')}}</option>
                                        <option value="warranty-card">{{ translate('Stamped warranty card
                                            ')}}</option>
                                        <option value="imei">{{ translate('IMEI matching box
                                            ')}}</option>
                                        <option value="purchase-bill">{{ translate('Original purchase bill (for used
                                            phone)
                                            ')}}</option>
                                        <option value="no-doc">{{ translate('I do not have any document.
                                            ')}}</option>
                                    </select>
                                </div>
                            </div>
                            <!-- end of ownership document -->

                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Location')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="location"
                                        placeholder="{{ translate('Location')}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{ translate('Product Tag')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control aiz-tag-input" name="tags[]"
                                        placeholder="{{ translate('Type & hit enter')}}">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- feature description -->
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Feature | Description')}}</h5>
                        </div>
                        <div class="card-body">
                            @if($categories)
                        @foreach($categories as $key => $category)
                            <div class="p-lg-4 p-3 @if($key == 0)d-block @else d-none @endif question-{{$category->name}}" >
                                <h3 class="mb-4">General Questions</h3>
                                @if($category->questions)
                                @foreach($category->questions as $question)
                                <div class="form-group mb-4 pb-2">
                                    <h6 class="mb-2 pb-1">{{$question->question}}</h6>
                                    @if($question->answers)
                                        @foreach($question->answers as $answer)
                                        <div class="custom-radio-holder">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="general-questions-{{$answer->id}}" value="{{$answer->id}}" name="question[][{{$question->id}}][answer]">
                                                <label class="custom-control-label font-weight-normal small" for="general-questions-{{$answer->id}}">{{$answer->answer}}</label>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                                @endforeach
                                @endif

                                <div class="form-group mb-4 pb-2">
                                    <h6 class="mb-2 pb-1">Additional remarks that we should know of</h6>
                                    <textarea name="qremark" id="" rows="6" class="form-control" placeholder="Does your device have any other features that isn't listed here?"></textarea>
                                </div>
                            </div>
                            @endforeach
                        @endif

                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Processor')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g intel core i9')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('RAM (GB)')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 4 GB')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('HDD(GB/TB)')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 500GB')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Screen Size (inch)')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 14 inch')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Battery')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 2-3 Hours')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Front Camera')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('e.g 13MP-19MP')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Back Camera')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('8 MP')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Other Features
                                    Description')}}</label>
                                <div class="col-md-10">
                                    <textarea name="meta_description" rows="8" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Warranty Type')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <select class="form-control selectpicker"
                                        data-placeholder="{{ translate('Select a condition')}}" id="warranty-type"
                                        name="warranty-type">
                                        <option value="shop">{{ translate('Dealer/Shop
                                            ')}}</option>
                                        <option value="manufacturer">{{ translate('Manufacturer/Importer
                                            ')}}</option>
                                        <option value="no-warranty">{{ translate('No Warranty
                                            ')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Warranty Period')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="unit"
                                        placeholder="{{ translate('1 year')}}">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end of feature description -->
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Images')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Gallery Images')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <div class="input-group" data-toggle="aizuploader" data-type="image"
                                        data-multiple="true">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{
                                                translate('Browse')}}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="photos" class="selected-files">
                                    </div>
                                    <div class="file-preview box sm">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Thumbnail Image')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <div class="input-group" data-toggle="aizuploader" data-type="image">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{
                                                translate('Browse')}}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="thumbnail_img" class="selected-files">
                                    </div>
                                    <div class="file-preview box sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Videos')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Video From')}}</label>
                                <div class="col-md-10">
                                    <select class="form-control aiz-selectpicker"
                                        data-minimum-results-for-search="Infinity" name="video_provider">
                                        <option value="youtube">{{ translate('Youtube')}}</option>
                                        <option value="dailymotion">{{ translate('Dailymotion')}}</option>
                                        <option value="vimeo">{{ translate('Vimeo')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Video URL')}}</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="video_link"
                                        placeholder="{{ translate('Video link')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Meta Tags')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Meta Title')}}</label>
                                <div class="col-md-10">
                                    <input type="text" name="meta_title" class="form-control"
                                        placeholder="{{ translate('Meta Title')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{translate('Description')}}</label>
                                <div class="col-md-10">
                                    <textarea name="meta_description" rows="8" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{ translate('Meta Image')}}</label>
                                <div class="col-md-10">
                                    <div class="input-group" data-toggle="aizuploader" data-type="image">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{
                                                translate('Browse')}}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="meta_img" class="selected-files">
                                    </div>
                                    <div class="file-preview box sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Price')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{ translate('Unit Price')}} <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-10">
                                    <input type="number" lang="en" min="0" step="0.01" class="form-control"
                                        name="unit_price"
                                        placeholder="{{ translate('Unit Price')}} ({{ translate('Base Price')}})"
                                        required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('Description')}} <span class="text-danger">*</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{ translate('Description')}}</label>
                                <div class="col-md-10">
                                    <div class="mb-3">
                                        <textarea class="aiz-text-editor" name="description" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card color-gray-background">
                        <div class="card-header">
                            <h5 class="mb-0 h6 fw-600">{{translate('PDF Specification')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-from-label">{{ translate('PDF')}}</label>
                                <div class="col-md-10">
                                    <div class="input-group" data-toggle="aizuploader" data-type="document">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{
                                                translate('Browse')}}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="pdf" class="selected-files">
                                    </div>
                                    <div class="file-preview box sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- assured section -->

                    <div class="form-check mb-4">
                        <input type="checkbox" class="form-check-input" id="chkAssured" data-toggle="modal"
                            data-target="#exampleModalLong">
                        <label class="form-check-label text_color pl-2 c-pointer" for="chkAssured">Do you want your
                            product to be assured by analogue mall?</label>
                    </div>


                    <div class="card color-gray-background terms-height-limit" id="showTerms">
                        <div class="card-header bg-soft-secondary">
                            <h5 class="mb-0 h6 fw-700 fs-20 terms_conditions fw-600 text-capitalize">{{translate('Terms
                                & Conditions')}}</h5>
                        </div>
                        <div class="card-body">

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <div class="accept_terms text-capitalize">
                                        <h5 class="fw-600">Accepting the terms</h5>

                                        <p class="terms_para">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                            Nisi exercitationem suscipit quia incidunt ad numquam quam nulla odio facere
                                            eligendi consequatur sunt, voluptates, quibusdam ex corrupti possimus ea
                                            eius vel voluptatibus nemo reprehenderit! Doloribus exercitationem at
                                            veritatis? Ea error dicta corporis non fugiat alias commodi consequuntur
                                            minima, vitae blanditiis recusandae similique cum vero exercitationem
                                            dolorem neque hic placeat saepe optio? Facere autem, explicabo quasi
                                            eligendi rerum et animi architecto quo pariatur possimus ad, officiis
                                            aspernatur dolor aperiam impedit voluptatibus saepe repellat veritatis
                                            inventore. In ad fuga, animi quas, quod earum libero cumque mollitia tenetur
                                            soluta est voluptates architecto, vitae laboriosam.</p>
                                    </div>
                                    <hr>
                                    <div class="accept_terms text-capitalize">
                                        <h5 class="fw-600">Accepting the terms</h5>

                                        <p class="terms_para">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                            Nisi exercitationem suscipit quia incidunt ad numquam quam nulla odio facere
                                            eligendi consequatur sunt, voluptates, quibusdam ex corrupti possimus ea
                                            eius vel voluptatibus nemo reprehenderit! Doloribus exercitationem at
                                            veritatis? Ea error dicta corporis non fugiat alias commodi consequuntur
                                            minima, vitae blanditiis recusandae similique cum vero exercitationem
                                            dolorem neque hic placeat saepe optio? Facere autem, explicabo quasi
                                            eligendi rerum et animi architecto quo pariatur possimus ad, officiis
                                            aspernatur dolor aperiam impedit voluptatibus saepe repellat veritatis
                                            inventore. In ad fuga, animi quas, quod earum libero cumque mollitia tenetur
                                            soluta est voluptates architecto, vitae laboriosam.</p>
                                    </div>
                                    <hr>
                                    <div class="accept_terms text-capitalize">
                                        <h5 class="fw-600">Accepting the terms</h5>

                                        <p class="terms_para">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                            Nisi exercitationem suscipit quia incidunt ad numquam quam nulla odio facere
                                            eligendi consequatur sunt, voluptates, quibusdam ex corrupti possimus ea
                                            eius vel voluptatibus nemo reprehenderit! Doloribus exercitationem at
                                            veritatis? Ea error dicta corporis non fugiat alias commodi consequuntur
                                            minima, vitae blanditiis recusandae similique cum vero exercitationem
                                            dolorem neque hic placeat saepe optio? Facere autem, explicabo quasi
                                            eligendi rerum et animi architecto quo pariatur possimus ad, officiis
                                            aspernatur dolor aperiam impedit voluptatibus saepe repellat veritatis
                                            inventore. In ad fuga, animi quas, quod earum libero cumque mollitia tenetur
                                            soluta est voluptates architecto, vitae laboriosam.</p>
                                    </div>
                                    <hr>
                                    <div class="accept_terms text-capitalize">
                                        <h5 class="fw-600">Accepting the terms</h5>

                                        <p class="terms_para">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                            Nisi exercitationem suscipit quia incidunt ad numquam quam nulla odio facere
                                            eligendi consequatur sunt, voluptates, quibusdam ex corrupti possimus ea
                                            eius vel voluptatibus nemo reprehenderit! Doloribus exercitationem at
                                            veritatis? Ea error dicta corporis non fugiat alias commodi consequuntur
                                            minima, vitae blanditiis recusandae similique cum vero exercitationem
                                            dolorem neque hic placeat saepe optio? Facere autem, explicabo quasi
                                            eligendi rerum et animi architecto quo pariatur possimus ad, officiis
                                            aspernatur dolor aperiam impedit voluptatibus saepe repellat veritatis
                                            inventore. In ad fuga, animi quas, quod earum libero cumque mollitia tenetur
                                            soluta est voluptates architecto, vitae laboriosam.</p>
                                    </div>
                                    <hr>
                                    <div class="accept_terms text-capitalize">
                                        <h5 class="fw-600">Accepting the terms</h5>

                                        <p class="terms_para">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                            Nisi exercitationem suscipit quia incidunt ad numquam quam nulla odio facere
                                            eligendi consequatur sunt, voluptates, quibusdam ex corrupti possimus ea
                                            eius vel voluptatibus nemo reprehenderit! Doloribus exercitationem at
                                            veritatis? Ea error dicta corporis non fugiat alias commodi consequuntur
                                            minima, vitae blanditiis recusandae similique cum vero exercitationem
                                            dolorem neque hic placeat saepe optio? Facere autem, explicabo quasi
                                            eligendi rerum et animi architecto quo pariatur possimus ad, officiis
                                            aspernatur dolor aperiam impedit voluptatibus saepe repellat veritatis
                                            inventore. In ad fuga, animi quas, quod earum libero cumque mollitia tenetur
                                            soluta est voluptates architecto, vitae laboriosam.</p>
                                    </div>
                                    <hr>

                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck3"
                                            data-toggle="" data-target="">
                                        <label class="form-check-label text_color pl-3 pb-3 c-pointer fs-18 fw-600"
                                            for="exampleCheck3" id="terms-cond">I agreed to these term and
                                            conditions.</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="mar-all text-right" id="save-product-btn">
                        <button type="submit" name="button" class="btn btn-primary">{{ translate('Save Product')
                            }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script type="text/javascript">


    $(function () {
        $("#showTerms").hide();
        $("#chkAssured").click(function () {
            if ($(this).is(":checked")) {
                $("#showTerms").slideDown();
                $("#save-product-btn").hide();
                $("#exampleCheck3").prop("checked", false);

            } else {
                $("#showTerms").hide();
                $("#save-product-btn").show();

            }
        });
        $("#exampleCheck3").click(function () {
            if ($(this).is(":checked")) {

                $("#save-product-btn").show();

            } else {
                $("#save-product-btn").hide();

            }
        });

        $('#categories').on('change', function() {
            console.log($(this).val());
            console.log($(this).find("option:selected").text());
    });
    });
</script>

@endsection