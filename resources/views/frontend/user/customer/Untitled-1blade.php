Som Bhandari, [15.02.21 11:14]
@extends('backend.layouts.app')
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
   <h5 class="mb-0 h6">{{translate('Add New Purchase Order')}}</h5>
</div>
<div class="col-md-12 mx-auto">
   <form class="form form-horizontal mar-top" action="{{route('purchaseorders.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
      @csrf
      <input type="hidden" name="added_by" value="admin">
      <div class="card">
         <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Purchase Order Information')}}</h5>
          </div>
         <div class="card-body">
            <div class="form-group row">
               <label class="col-md-3 col-from-label">{{translate('PO Number')}} <span class="text-danger">*</span></label>
               <div class="col-md-8">
                  <input type="text" class="form-control" name="po_number" placeholder="{{ translate('Po Number') }}"  required>
               </div>
            </div>
            <div class="form-group row" id="vendorproduct">
               <label class="col-md-3 col-from-label">{{translate('Select Bill Type')}} <span class="text-danger">*</span></label>
               <div class="col-md-8">
                  <select class="form-control aiz-selectpicker" name="bill_type" id="bill_type" data-live-search="true" required>
                     <option value="">Select Bill Type</option>
                     <option value="pan">{{translate('PAN')}}</option>
                     <option value="vat">{{translate('VAT')}}</option>
                  </select>
               </div>
            </div>
            <div class="form-group row" id="vendorproduct">
               <label class="col-md-3 col-from-label">{{translate('Vendors')}} <span class="text-danger">*</span></label>
               <div class="col-md-8">
                  <select class="form-control aiz-selectpicker" name="vendor_id" id="vendor_id" data-live-search="true" required>
                     <option value="">Select Vendor</option>
                     @foreach ($vendors as $vendor)
                     <option value="{{ $vendor->id }}">{{ ucfirst($vendor->name) }}</option>
                     @endforeach 
                  </select>
               </div>
            </div>
            <div class="form-group row">
               <label class="col-md-3 col-from-label">{{translate('PDC Count')}}</label>
               <div class="col-md-8">
                  <input type="number" class="form-control" name="pdc_count" placeholder="{{ translate('Pdc Count (e.g. 1,2,3..)') }}" >
               </div>
            </div>
            <div class="form-group row">
               <label class="col-md-3 col-from-label">{{translate('Date')}} <span class="text-danger">*</span></label>
               <div class="col-md-8">
                  <input type="date" lang="en" class="form-control" name="date" value="{{ date('Y-m-d') }}" placeholder="{{ translate('Select Date') }}"  >
               </div>
            </div>
         </div>
      </div>
      <div class="card">
         <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Add Product')}}</h5>
         </div>
         <div class="card-body" >
            <table class="class="table aiz-table mb-0"" id="tab_logic">
            <thead>
               <tr>
                  <th class="text-center"> # </th>
                  <th class="text-center"> Product </th>
                  <th class="text-center"> Qty </th>
                  <th class="text-center"> Marked Price </th>
                  <th class="text-center"> Rate </th>
                  <th class="text-center"> Total </th>
               </tr>
            </thead>
            <tbody>
               <input type="hidden" name='dis' placeholder='Discount' class="form-control dis" step="0" min="0" required=""/>
               <input type="hidden" name='dis' placeholder='Discount' class="form-control dis_flat" step="0" min="0" required=""/>
               <tr id='addr0'>
                  <td>1</td>
                  <td>

Som Bhandari, [15.02.21 11:14]
<select class="form-control aiz-selectpicker product" name="product[]"  data-live-search="true" required>
                        <option value="">Select Product for Purchase Order</option>
                        @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ ucfirst($product->name) }}</option>
                        @endforeach 
                     </select>
                  </td>
                  <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0" required=""/></td>
                  <td><input type="number" name='price[]' placeholder='Enter Marked Price' class="form-control " id="price1" step="0.00" min="0" readonly="" /></td>
                  <td><input type="number" name='rate[]' placeholder='Rate' class="form-control rate" step="0.00" min="0" readonly/></td>
                  <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
               </tr>
               <tr id='addr1'></tr>
            </tbody>
            </table>
         </div>
         <div class="aiz-titlebar text-left mt-2 mb-3">
            <div class="row">
               <div class="col-md-6 text-md-left">
                  <button type = "button" id="add_row" class="btn btn-circle btn-info">{{translate('Add New Product')}}</button>
               </div>
               <div class="col-md-6 text-md-right">
                  <button type = "button" id='delete_row' class="btn btn-circle btn-info">{{translate('Delete Product')}}</button>
               </div>
            </div>
         </div>
      </div>
      <div class="card">
         <div class="text-right">
            <div class="" style="display: inline-block; width: 30%">
               <table class="table table-bordered table-hover" id="tab_logic_total">
                  <tbody>
                     <tr>
                        <th class="text-center">Sub Total</th>
                        <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                     </tr>
                     <tr id ="tax_type">
                        <th class="text-center">VAT</th>
                        <td class="text-center">
                           <div class="input-group mb-2 mb-sm-0">
                              <input type="number" class="form-control" id="tax" placeholder="0">
                              <div class="input-group-addon">%</div>
                           </div>
                        </td>
                     </tr>
                     <tr id="tax_type_amount">
                        <th class="text-center">VAT Amount</th>
                        <td class="text-center"><input type="number" name='tax_amount' id="tax_amount" placeholder='0.00' class="form-control" readonly/></td>
                     </tr>
                     <tr>
                        <th class="text-center">Grand Total</th>
                        <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="mb-3 text-right">
         <button type="submit" name="button" class="btn btn-primary">{{ translate('Save Purchase Order') }}</button>
      </div>
   </form>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
              var i =2;
                 var products = '{!! $products !!}';
                 $('.product').on('change', function () {
                   var id = $(this).val();
                   alert(id)
                      $.ajax({
                                type: "POST",
                                url: "{{ route('admin.vendor.getProductDetails') }}",
                                data: {
                                    'id': id,

Som Bhandari, [15.02.21 11:14]
'_token':'{{ csrf_token() }}'
                                },
                                dataType: 'json',
                                success: function (response) {
                                  if(response){
                                 console.log(response.unit_price);
                                  $('#price'+1).val(response.unit_price);
                                }
                              }
                      });
                  });
         
                     $('#vendor_id').on('change', function () {
                       var id = $(this).val();
                          $.ajax({
                                    type: "POST",
                                    url: "{{ route('admin.vendor.getVendorDetails') }}",
                                    data: {
                                        'id': id,
                                        '_token':'{{ csrf_token() }}'
                                    },
                                    dataType: 'json',
                                    success: function (response) {
                                      console.log(response);
                                      if(response.seller_commission_percentage != null){
                                        $('.dis').val(response.seller_commission_percentage);
                                      }else{
                                        $('.dis_flat').val(response.seller_commission_flat);
                                      }
                                    }
                                });
                      });

                     $('#bill_type').on('change', function () {
                       var bill_type = $(this).val();
                        if(bill_type == 'pan'){
                          document.getElementById("tax_type").style.display = "none";
                          document.getElementById("tax_type_amount").style.display = "none";
                          }else{
                          document.getElementById("tax_type").style.display = '';
                          document.getElementById("tax_type_amount").style.display = '';
                          }
                });
                   
         
                      $("#add_row").click(function(){
                           var $table = $('#tab_logic');
                           var rowCount = document.getElementById('tab_logic').rows.length;
                            $tab_logic1 = '<tr>'+
                                          '<td>'+(rowCount-1)+'</td>';
                            $tab_logic1 += '<td><select class="form-control aiz-selectpicker product" name="product[]"  data-live-search="true" required>'; 
                            $tab_logic1 += '<option value="">Select Product for Purchase Order</option>';
                            <?php foreach ($products as $key => $product): ?>
                                $tab_logic1 +=  '<option value="'+'{{$product->id}}'+'">'+'{{$product->name}}'+'</option>';
                            <?php endforeach ?>
                            $tab_logic1 +=  '</select></td>';
                       
                            $tab_logic1 += '<td><input type="number" name="qty[]" placeholder="Enter Qty" class="form-control qty" step="0" min="0" required=""/></td>';
                       
                            $tab_logic1 += '<td><input type="number" name="price[]" placeholder="Enter Marked Price" class="form-control" id = "price'+i+'" step="0.00" min="0" required=""/></td>';
                       
                            $tab_logic1 += '<td><input type="number" name="rate[]" placeholder="Rate" class="form-control rate" step="0.00" min="0" readonly/></td>';
                       
                            $tab_logic1 += '<td><input type="number" name="total[]" placeholder="0.00" class="form-control total" readonly/></td></tr>';
                            
                            $table.append($tab_logic1);

Som Bhandari, [15.02.21 11:14]
AIZ.plugins.bootstrapSelect('refresh');
                            i++;
                      });
                      $("#delete_row").click(function(){
                            var rowCount = document.getElementById('tab_logic').rows.length;
                            if(rowCount > 2){
                              $('#tab_logic tr').last().remove();
                            }
                            calc();
                     });
                     
                     $('#tab_logic tbody').on('keyup change',function(){
                       calc();
                     });
                     $('#tax').on('keyup change',function(){
                        calc_total();
                       });   
      });
   
               function calc(){
                 $('#tab_logic tbody tr').each(function(i, element) {
                  var html = $(this).html();
                  if(html!='')
                  {
                    var qty = $(this).find('.qty').val();
                    var price = $(this).find('.price').val();
                    var discount = $('.dis').val();
                    var dis_flat = $('.dis_flat').val();
                    if(discount != null){
                       var rate = price/(1+discount/100);
                     }else{
                       var rate = (price-dis_flat);
                     }
                    var total = qty*rate;
                    $(this).find('.rate').val((rate).toFixed(2));
                    $(this).find('.total').val((total).toFixed(2));
                    
                    calc_total();
                  }
                  });
               }
               
               function calc_total(){
                 total=0;
                 $('.total').each(function() {
                      total += parseFloat($(this).val());
                  });
                 $('#sub_total').val(total.toFixed(2));
                 tax_sum=total/100*$('#tax').val();
                 $('#tax_amount').val(tax_sum.toFixed(2));
                 $('#total_amount').val((tax_sum+total).toFixed(2));
               }
               
               
</script>
@endsection