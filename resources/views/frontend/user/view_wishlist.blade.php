@extends('frontend.layouts.app')

@section('content')
<section class="py-5 bg-white">
    <div class="container">
        <div class="d-flex align-items-start">
            @include('frontend.inc.user_side_nav')
            <div class="aiz-user-panel ">
                <div class="mb-3">
                    <img class="img-fluid" src="./frontend/images/points_ads.png" alt="">
                </div>
                <div class="aiz-titlebar mt-2 mb-3">
                    <div class="row align-items-start">
                        <div class="col-md-6">
                            <b class="h5 text_color fw-600 text-uppercase">{{ translate('Your Wishlist')}}</b>
                        </div>
                    </div>
                </div>

                <div class="row gutters-5 color-gray-background">
                    @foreach ($wishlists as $key => $wishlist)
                    @if ($wishlist->product != null)
                    <div class="col-xxl-3 col-xl-4 col-lg-3 col-md-4 col-sm-6 pt-2" id="wishlist_{{ $wishlist->id }}">
                        <div class="card mb-2">
                            <div class="card-body color-gray-background">
                                <a href="{{ route('product', $wishlist->product->slug) }}" class="d-block mb-3">
                                    <img src="{{ uploaded_asset($wishlist->product->thumbnail_img) }}"
                                        class="img-fit h-140px h-md-200px">
                                </a>

                                <h5 class="fs-14 mb-0 lh-1-5 fw-600 text-truncate-2">
                                    <a href="{{ route('product', $wishlist->product->slug) }}" class="text-reset">{{
                                        $wishlist->product->getTranslation('name') }}</a>
                                </h5>
                                <div class="rating rating-sm mb-1">
                                    {{ renderStarRating($wishlist->product->rating) }}
                                </div>
                                <div class=" fs-14">
                                    @if(home_base_price($wishlist->product->id) !=
                                    home_discounted_base_price($wishlist->product->id))
                                    <del class="opacity-60 mr-1">{{ home_base_price($wishlist->product->id) }}</del>
                                    @endif
                                    <span class="fw-600 text-primary">{{
                                        home_discounted_base_price($wishlist->product->id) }}</span>
                                </div>
                            </div>
                            <div class=" color-gray-background card-footer ">
                                <a href="#" class="link link--style-3" data-toggle="tooltip" data-placement="top"
                                    title="Remove from wishlist" onclick="removeFromWishlist({{ $wishlist->id }})">
                                    <i class="la la-trash la-2x"></i>
                                </a>
                                <button type="button" class="btn btn-sm btn-block btn-primary ml-3"
                                    onclick="showAddToCartModal({{ $wishlist->product->id }})">
                                    <i class="la la-shopping-cart mr-2"></i>{{ translate('Add to cart')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="aiz-pagination">
                    {{ $wishlists->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
@if ($bestSelling->value == 1)
<section class="p-top35 p-bottom35 bg-white">
    <div class="container">
        <div class=" py-4 py-md-3  shadow-sm rounded">
            <div class="d-flex mb-3 align-items-baseline border-bottom">
                <h3 class="h5 fw-700 mb-0">
                    <span class="border-bottom text_color border-primary border-width-2 pb-3 d-inline-block">{{
                        translate('You Might Be Intrested In') }}</span>
                </h3>
                <!-- <a href="javascript:void(0)" class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{ translate('Top 20') }}</a> -->
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="6" data-xl-items="5" data-lg-items="4"
                data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach ($bestProd as $key => $product)
                @include('frontend.partials.single_product')
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
@endsection


@section('modal')

<div class="modal fade" id="addToCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size"
        role="document">
        <div class="modal-content position-relative">
            <div class="c-preloader">
                <i class="fa fa-spin fa-spinner"></i>
            </div>
            <button type="button" class="close absolute-close-btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div id="addToCart-modal-body">

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function removeFromWishlist(id) {
        $.post('{{ route('wishlists.remove') }}', { _token: '{{ csrf_token() }}', id: id }, function (data) {
            $('#wishlist').html(data);
            $('#wishlist_' + id).hide();
            AIZ.plugins.notify('success', '{{ translate('Item has been renoved from wishlist') }}');
        })
    }
</script>
@endsection