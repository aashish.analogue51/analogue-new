@extends('frontend.layouts.app')

@section('content')
<section class="py-2">
    <div class="container text-center">
        <div class="row">

            <div class="col-lg-6">
                <ul class="breadcrumb bg-transparent p-0 justify-content-center justify-content-lg-start">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home')}}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" href="{{ route('categories.all') }}">"{{ translate('All Categories') }}"</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="py-4 bg-white all-category">
    <div class="container">
        <div class="row justify-content-center"> 
            <div class="col-12 mb-3 text-center text-lg-center">
                <h1 class="fw-600 h4" style="text-transform: capitalize; color: #02afeef6;">{{ translate('All Collections') }}</h1>
            </div>
        </div>
        @foreach ($categories as $key => $category)
        <div class="d-block mb-3 align-items-baseline border-bottom">
            <h3 class="h5 fw-600 mb-0">
                <a style="color: black;" href="{{ route('products.category', $category->slug) }}">
                <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block text-capitalize">
                    <strong>
                        {{$category->name}}
                    </strong>
                </span>
                </a>
            </h3>
        </div>
        <div>
            <div class="row">
                @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($category->id) as $key => $first_level_id)
                <div class="col-12 col-sm-4 col-md-4 mb-3">
                    <p><strong><a class="text-capitalize" href="{{ route('products.category', \App\Category::find($first_level_id)->slug) }}">{{ \App\Category::find($first_level_id)->getTranslation('name') }}</a></strong></p>
                    <ul style="list-style: none; padding-left: 10px;">
                        @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id) as $key => $second_level_id)
                        <li>
                            <a class="text-capitalize" href="{{ route('products.category', \App\Category::find($second_level_id)->slug) }}" >{{ \App\Category::find($second_level_id)->getTranslation('name') }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endforeach
            </div>
        </div>
        @endforeach

<!--         <div class="main-box d-none"> 
            @foreach ($categories as $key => $category)
            <div class=" ">
                <div class="border-bottom fs-16 fw-600 shadow-sm rounded border ">
                    <a href="{{ route('products.category', $category->slug) }}" class="text-center all-cat-main-box p-3 d-block" style="">
                        <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                        data-src="{{ uploaded_asset($category->banner) }}"
                        alt="{{ $category->getTranslation('name') }}" class="img-fluid d-inline-block img lazyload"
                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';">
                        <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                        data-src="{{ uploaded_asset($category->banner) }}"
                        alt="{{ $category->getTranslation('name') }}" class="img-fluid d-inline-block img lazyload ablt-img"
                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';">

                        <span>{{  $category->getTranslation('name') }}</span>
                    </a>
                </div>
                {{-- <div class="p-3 p-lg-4">
                    <div class="row">
                        @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($category->id) as $key => $first_level_id)
                        <div class="col-lg-4 col-6 text-left">
                            <h6 class="mb-3"><a class="text-reset fw-600 fs-14" href="{{ route('products.category', \App\Category::find($first_level_id)->slug) }}">{{ \App\Category::find($first_level_id)->getTranslation('name') }}</a></h6>
                            <ul class="mb-3 list-unstyled pl-2">
                                @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id) as $key => $second_level_id)
                                <li class="mb-2">
                                    <a class="text-reset" href="{{ route('products.category', \App\Category::find($second_level_id)->slug) }}" >{{ \App\Category::find($second_level_id)->getTranslation('name') }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
                --}}
            </div>
            @endforeach
        </div> -->
    </div>
</div>
</section>

@endsection
