@extends('frontend.layouts.app') @section('content')
<div class="bg-white">
    <div class="container m-auto pt-5 pb-5 text-center aboutus_container ">
        <div class="aboutUs w-md-75 w-sm-100 m-auto">
            <h1 class="primary-color fw-600 pb-4">About Us</h1>
            <p class="para_one fs-14">
                Analogue Mall is the realization of the idea that there is still
                room for innovation in regards to how people shop for things online.
                The distrust for products and shopping platforms is only heightened
                by our paranoia that we would just be wasting our hard-earned cash.
            </p>
            <p class="para_one fs-14">
                We simply want to make things easier for our customers by providing
                a catalog of genuine and original electronic products and storing
                them conveniently under one roof. Whether you are looking to buy a
                pair of headphones or your next laptop, do it with the confidence
                that what you are getting is what you actually wanted.
            </p>
            <p class="para_one fs-14">For Digitally Smart Nepal .</p>
            <p class="para_one fs-14">
                Analogue Mall is here to redefine your electronic/digital needs. For
                an amazing shopping experience and for digitally smart Nepal,
                Analogue Inc. vows to provide you with genuine, 100% original and
                authentic electronic goods with our trust badge. With Analogue Mall
                stay worry-free and enjoy the digital shopping experience.
            </p>
        </div>
    </div>
</div>

@endsection
