@extends('frontend.layouts.app')
@section('meta')
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $customer_product->meta_title }}">
<meta itemprop="description" content="{{ $customer_product->meta_description }}">
<meta itemprop="image" content="{{ uploaded_asset($customer_product->meta_img) }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:title" content="{{ $customer_product->meta_title }}">
<meta name="twitter:description" content="{{ $customer_product->meta_description }}">
<meta name="twitter:creator" content="@author_handle">
<meta name="twitter:image" content="{{ uploaded_asset($customer_product->meta_img) }}">
<meta name="twitter:data1" content="{{ single_price($customer_product->unit_price) }}">
<meta name="twitter:label1" content="Price">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $customer_product->meta_title }}" />
<meta property="og:type" content="product" />
<meta property="og:url" content="{{ route('product', $customer_product->slug) }}" />
<meta property="og:image" content="{{ uploaded_asset($customer_product->meta_img) }}" />
<meta property="og:description" content="{{ $customer_product->meta_description }}" />
<meta property="og:site_name" content="{{ get_setting('meta_title') }}" />
<meta property="og:price:amount" content="{{ single_price($customer_product->unit_price) }}" />
@endsection

@section('content')

<section class="bg-white pt-3 pb-3">
    <div class="container">
        <div class="bg-white rounded p-3">
            <div class="row ">
                <div class="col-xl-5 col-lg-6 mb-4">
                    <div class="sticky-top z-3 row gutters-10">
                        @if($customer_product->photos != null)
                        @php
                        $photos = explode(',',$customer_product->photos);
                        @endphp
                        <div class="col order-1 order-md-2">
                            <div class="aiz-carousel product-gallery" data-nav-for='.product-gallery-thumb'
                            data-fade='true'>
                            @foreach ($photos as $key => $photo)
                            <div class="carousel-box img-zoom rounded">
                                <img class="img-fluid lazyload"
                                src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                data-src="{{ uploaded_asset($photo) }}"
                                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 col-md-auto w-md-80px order-2 order-md-1 mt-3 mt-md-0">
                        <div class="aiz-carousel product-gallery-thumb" data-items='5'
                        data-nav-for='.product-gallery' data-vertical='true' data-vertical-sm='false'
                        data-focus-select='true'>
                        @foreach ($photos as $key => $photo)
                        <div class="carousel-box c-pointer border p-1 rounded">
                            <img class="lazyload mw-100 size-60px mx-auto"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($photo) }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="col-xl-7 col-lg-6">
            <div class="text-left">
                <h1 class="mb-2 fs-22 fw-700 text_color">
                    {{ $customer_product->getTranslation('name') }}
                </h1>
                <div class="row align-items-center my-3">
                    <div class="col-auto ">
                        <small class="mr-2 fs-16 opacity-80">{{ translate('Sold by')}}: </small>
                        {{ translate($customer_product->user->name) }}
                    </div>
                    @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                    <div class="col-auto">
                        <button class="btn btn-sm btn-soft-primary" onclick="show_chat_modal()">{{
                        translate('Message Seller')}}</button>
                    </div>
                    @endif

                    @if ($customer_product->brand != null)
                    <div class="col-auto">
                        <img src="{{ uploaded_asset($customer_product->brand->logo) }}"
                        alt="{{ $customer_product->brand->getTranslation('name') }}" height="30">
                    </div>
                    @endif
                </div>

                <div class="row no-gutters mt-3">
                    <div class="col-2">
                        <div class="opacity-80 mt-2">{{ translate('Price')}}:</div>
                    </div>
                    <div class="col-10">
                        <div class="">
                            <strong class="h3 fw-600 text-primary">
                                {{ single_price($customer_product->unit_price) }}
                            </strong>
                            @if($customer_product->unit != null || $customer_product->unit != '')
                            <span class="opacity-70">/{{ $customer_product->getTranslation('unit') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <ul class="list-group rounded mt-5">
                    <li class="list-group-item">
                        <div class="d-flex">
                            <span
                            class="d-flex align-items-center justify-content-center rounded-circle size-30px btn_background mr-2">
                            <i class="la la-user fs-18"></i>
                        </span>
                        <div class="flex-grow-1 fs-17 fw-600">
                            {{ $customer_product->user->name }}
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="d-flex">
                        <span
                        class="d-flex align-items-center justify-content-center rounded-circle size-30px btn_background mr-2">
                        <i class="la la-map-marker fs-18"></i>
                    </span>
                    <div class="flex-grow-1 fs-17 fw-600">
                        {{ $customer_product->location }}
                    </div>
                </div>
            </li>
            <li class="list-group-item c-pointer" @if (!Auth::check()) data-toggle="modal"
            data-target="#login_modal" onclick="show_number(this)" @endif>
            <div class="d-flex">
                <span
                class="d-flex align-items-center justify-content-center rounded-circle size-30px bg-primary text-white mr-2">
                <i class="la la-phone fs-18"></i>
            </span>
            @if($customer_product->assured == 3)
            <h3 class="h5 fw-700 mb-0">
                <span class="real fs-17 fw-600">N/A</span>
            </h3>
            @else
            <div class="flex-grow-1">
                @if (Auth::check())
                <h3 class="h5 fw-700 mb-0">
                    <span class="real fs-17 fw-600">{{ $customer_product->user->phone }}</span>
                </h3>
                @else
                <h3 class="h5 fw-700 mb-0">
                    <span class=" fs-17 fw-600">{{
                        str_replace(substr($customer_product->user->phone,3),'XXXXXXXX',
                        $customer_product->user->phone) }}</span>
                    </h3>
                    <p class="mb-0 opacity-80">{{ translate('Click to show phone number') }}</p>
                    @endif
                </div>
                @endif
            </div>
        </li>
    </ul>

    <div class="row no-gutters mt-5">
        <div class="col-2">
            <div class="opacity-80 mt-2">{{ translate('Share')}}:</div>
        </div>
        <div class="col-10">
            <div class="aiz-share"></div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="pb-4 bg-white">
    <div class="container ">
        <div class="bg-white mb-3 rounded">
            <div class="nav border-bottom aiz-nav-tabs">
                <a href="#tab_default_1" data-toggle="tab" class="p-3 fs-16 fw-600 text-reset active show">{{
                translate('Description')}}</a>
                @if($customer_product->video_link != null)
                <a href="#tab_default_2" data-toggle="tab" class="p-3 fs-16 fw-600 text-reset">{{
                translate('Video')}}</a>
                @endif
                @if($customer_product->pdf != null)
                <a href="#tab_default_3" data-toggle="tab" class="p-3 fs-16 fw-600 text-reset">{{
                translate('Downloads')}}</a>
                @endif
            </div>

            <div class="tab-content pt-0">
                <div class="tab-pane active show" id="tab_default_1">
                    <div class="p-4">
                        <div class="mw-100 overflow-hidden text-left">
                            <?php echo $customer_product->getTranslation('description'); ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="tab_default_2">
                    <div class="p-4">
                        <div class="embed-responsive embed-responsive-16by9 mb-5">
                            @if ($customer_product->video_provider == 'youtube' && isset(explode('=',
                            $customer_product->video_link)[1]))
                            <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/{{ explode('=', $customer_product->video_link)[1] }}"></iframe>
                            @elseif ($customer_product->video_provider == 'dailymotion' && isset(explode('video/',
                            $customer_product->video_link)[1]))
                            <iframe class="embed-responsive-item"
                            src="https://www.dailymotion.com/embed/video/{{ explode('video/', $customer_product->video_link)[1] }}"></iframe>
                            @elseif ($customer_product->video_provider == 'vimeo' && isset(explode('vimeo.com/',
                            $customer_product->video_link)[1]))
                            <iframe
                            src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $customer_product->video_link)[1] }}"
                            width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen
                            allowfullscreen></iframe>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_default_3">
                    <div class="p-4 text-center ">
                        <a href="{{ uploaded_asset($customer_product->pdf) }}" class="btn btn-primary">{{
                        translate('Download') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mb-0 color-gray-background">
    <div class="container">
        <div class="color-gray-background shadow-sm rounded">
            <div class="d-flex mb-1 align-items-baseline px-3 pt-4">
                <h5 class="fw-700 mb-0">
                    {{ translate('Analogue Pre-Owned') }}
                </h5>
                <a href="{{ route('customer_products.category', $customer_product->category->slug) }}"
                    class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{ translate('View More') }}</a>
                </div>
                <div class="">
                    <div class="aiz-carousel gutters-5 half-outside-arrow" data-items="6" data-xl-items="5"
                    data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true'
                    data-infinite='true'>
                    @php
                    $products = \App\CustomerProduct::where('category_id', $customer_product->category_id)->where('id',
                    '!=', $customer_product->id)->where('status', '1')->where('published', '1')->limit(10)->get();
                    @endphp
                    @foreach ($products as $key => $product)
                    <div class="carousel-box">
                        <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                            <div class="position-relative bg-white">
                                <a href="{{ route('customer.product', $product->slug) }}" class="d-block">
                                    <img class="img-fit lazyload mx-auto h-140px h-md-210px"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                    alt="{{  $product->getTranslation('name')  }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                                </a>
                                <div class="absolute-top-right pt-2 pr-2">
                                   @if($product->assured == '3')
                                   <span
                                   class="badge badge-inline badge-success bg-primary px-2">{{translate('Assured')}}</span>
                                   @elseif($product->conditon == 'new')
                                   <span class="badge badge-inline badge-success px-2">{{translate('new')}}</span>
                                   @elseif($product->conditon == 'used')
                                   <span class="badge badge-inline badge-danger px-2">{{translate('Used')}}</span>
                                   @endif
                               </div>
                           </div>
                           <div class="p-md-3 p-2 text-left">
                            <div class="fs-15 mb-1">
                                <span class="fw-700 text-primary">{{ single_price($product->unit_price) }}</span>
                            </div>
                            <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
                                <a href="{{ route('customer.product', $product->slug) }}"
                                    class="d-block text-reset">{{ $product->getTranslation('name') }}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<hr>
<section>
    <div class="banner_ads text-center">
        <img class="img-fluid" src="{{ static_asset('assets/img/placeholder-rect.jpg') }}" alt="banner ads">
    </div>

</section>
<section class="bg-white">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 text-center pb-2">
                <img class="img-fluid preowned_img" src="../frontend/images/preowned.png" alt="">
            </div>
            <div class="col-12 text-center pb-2">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum consectetur sed voluptatibus. Ex
                    perspiciatis eligendi odio explicabo, unde, tempore nobis laudantium minus cum blanditiis
                    temporibus, maiores suscipit! Vero optio ratione excepturi veniam dignissimos delectus sed ullam
                    quam? Molestias reiciendis, corrupti voluptate odio asperiores quas, repellendus aut repellat
                    delectus soluta pariatur perferendis distinctio consequatur, doloribus nostrum. Similique
                repellendus eos reiciendis adipisci.</p>
            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-primary text-capitalize">How it works</a>
            </div>
        </div>
    </div>


</section>
@endsection
@section('modal')
<div class="modal fade" id="chat_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
    <div class="modal-content position-relative">
        <div class="modal-header">
            <h5 class="modal-title fw-600 h5 text_color">{{ translate('Any query about this product')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="" action="{{ route('conversations.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="product_id" value="{{ $customer_product->id }}">
            <div class="modal-body gry-bg px-3 pt-3">
                <div class="form-group">
                    <input type="text" class="form-control mb-3" name="title" value="{{ $customer_product->name }}"
                    placeholder="{{ translate('Product Name') }}" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="8" name="message" required
                    placeholder="{{ translate('Your Message') }}"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary fw-600" data-dismiss="modal">{{
                translate('Cancel')}}</button>
                <button type="submit" class="btn btn-primary fw-600">{{ translate('Send')}}</button>
            </div>
        </form>
    </div>
</div>
</div>
<!--login model-->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-dialog-zoom modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title fw-600 text_color text-capitalize">{{ translate('Please Login First')}}</h6>
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="p-3">
                <form class="form-default" role="form" action="{{ route('cart.login.submit') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                        <input type="text"
                        class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="{{ translate('Email Or Phone')}}" name="email"
                        id="email">
                        @else
                        <input type="email"
                        class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                        @endif
                        @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                        <span class="opacity-60">{{ translate('Use country code before number') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control h-auto form-control-lg"
                        placeholder="{{ translate('Password')}}">
                    </div>

                    <div class="row mb-2">
                        <div class="col-6">
                            <label class="aiz-checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class=opacity-60>{{ translate('Remember Me') }}</span>
                                <span class="aiz-square-check"></span>
                            </label>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{ route('password.request') }}" class="text-reset opacity-60 fs-14">{{
                            translate('Forgot password?')}}</a>
                        </div>
                    </div>

                    <div class="mb-5">
                        <button type="submit" class="btn btn-primary btn-block fw-600">{{ translate('Login')
                        }}</button>
                    </div>
                </form>

                <div class="text-center mb-3">
                    <p class="text-muted mb-0">{{ translate('Dont have an account?')}}</p>
                    <a href="{{ route('user.registration') }}">{{ translate('Register Now')}}</a>
                </div>
                @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 ||
                \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 ||
                \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                <div class="separator mb-3">
                    <span class="bg-white px-3 opacity-60">{{ translate('Or Login With')}}</span>
                </div>
                <ul class="list-inline social colored text-center mb-5">
                    @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="facebook">
                            <i class="lab la-facebook-f"></i>
                        </a>
                    </li>
                    @endif
                    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="google">
                            <i class="lab la-google"></i>
                        </a>
                    </li>
                    @endif
                    @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="twitter">
                            <i class="lab la-twitter"></i>
                        </a>
                    </li>
                    @endif
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>
</div>




@endsection


@section('script')
<script type="text/javascript">

    function show_number(el) {
        $(el).find('.dummy').addClass('d-none');
        $(el).find('.real').removeClass('d-none').addClass('d-block');
    }
    function show_chat_modal() {
        @if (Auth:: check())
        $('#chat_modal').modal('show');
        @else
        $('#login_modal').modal('show');
        @endif
    }
</script>
@endsection