@extends('frontend.layouts.app') @section('content')
<div class="container text-center pt-5 pb-5 aboutus_container">
    <h1 class="primary-color pb-4">Contact</h1>
    <p class="para_one fs-18">Want to get in touch with us?</p>
    <div class="contact w-sm-100 w-md-50  m-auto">
        <p>
           <h2><strong>Phone:</strong><a href="tel:+9779802320803">+977-9802320803</a>
           </h2>
           <h2><strong>Email:</strong> <a href="mailto:info@analoguemall.com">info@analoguemall.com</a>

           </h2>
           <h2><strong>Address: </strong>Pinglasthan, Gaushala
            Kathmandu
           </h2>
           
        </p>
    </div>
</div>

@endsection
