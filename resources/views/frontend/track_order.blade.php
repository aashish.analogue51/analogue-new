@extends('frontend.layouts.app')

@section('content')
<section class="py-1 mb-0">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <ul class="breadcrumb bg-transparent p-0 justify-content-start justify-content-lg-start">
                    <li class="breadcrumb-item opacity-50">
                        <a class="text-reset" href="{{ route('home') }}">{{ translate('Home') }}</a>
                    </li>
                    <li class="text-dark fw-600 breadcrumb-item">
                        <a class="text-reset" style="color: #03afec !important;" href="{{ route('orders.track') }}">"{{ translate('Track Order') }}"</a>
                    </li>
                </ul>
                {{--<h1 class="fw-600 text_color h4 mb-0">{{ translate('Track Order') }}</h1>--}}
            </div>
            <div class="col-lg-6">
                
            </div>
        </div>
    </div>
</section>
<section class="mb-5 py-5 bg-white">
    <div class="container text-left">
        <div class="row">
            <div class="col-xxl-5 col-xl-6 col-lg-8 mx-auto">
                <form class="" action="{{ route('orders.track') }}" method="GET" enctype="multipart/form-data">
                    <div class="border border-light rounded shadow-sm hov-shadow-md has-transition py-3 px-3">
                        <div class="fs-15  p-3 border-bottom text-center text_color">
                            <h4 class="fw-700">{{ translate('Check Your Order Status')}}</h4>
                        </div>
                        <div class="form-box-content p-3">
                            <div class="form-group">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Order Code')}}" name="order_code" required>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">{{ translate('Track Order')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div>
                
            </div>
        </div>
    <div class="track-order d-flex justify-content-center pt-4">
        <div class="row">
            <div class="col-12">
                <h2 class="fs-24 text_color fw-600 text-center">Your Previous Order Tracking Code</h2>
                @if(Auth::user())
                <div class="d-flex justify-content-center">
                    <ul class="pl-0 previous_order-tracking" >
                        @foreach(Auth::user()->orders as $ord)
                        <li>
                            <a href="?order_code={{$ord->code}}"> {{$ord->code}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
             
                
                @endif
    
            </div>
        
        </div>
     
    </div>
    

        @isset($order)
            <div class="bg-white rounded shadow-sm mt-5">
                <div class="fs-15 fw-600 p-3 border-bottom">
                    {{ translate('Order Summary')}}
                </div>
                <div class="p-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Order Code')}}:</td>
                                    <td>{{ $order->code }}</td>
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Customer')}}:</td>
                                    <td>{{ json_decode($order->shipping_address)->name }}</td>
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Email')}}:</td>
                                    @if ($order->user_id != null)
                                        <td>{{ $order->user->email }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Shipping address')}}:</td>
                                    <td>{{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->country }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Order date')}}:</td>
                                    <td>{{ date('d-m-Y H:i A', $order->date) }}</td>
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Total order amount')}}:</td>
                                    <td>{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}</td>
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Shipping method')}}:</td>
                                    <td>{{ translate('Flat shipping rate')}}</td>
                                </tr>
                                <tr>
                                    <td class="w-50 fw-600">{{ translate('Payment method')}}:</td>
                                    <td>{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            @foreach ($order->orderDetails as $key => $orderDetail)
                @php
                    $status = $orderDetail->delivery_status;
                @endphp
                <div class="bg-white track-order-summery rounded shadow-sm mt-4">
                    <div class="p-4">
                        <ul class="list-inline text-center aiz-steps">
                            <li class="list-inline-item @if($status == 'pending') active @else done @endif">
                                <div class="icon">
                                    <i class="las la-file-invoice"></i>
                                </div>
                                <div class="title">{{ translate('Order placed')}}</div>
                            </li>
                            <li class="list-inline-item @if($status == 'confirmed') active @elseif($status == 'on_delivery' || $status == 'delivered') done @endif">
                                <div class="icon">
                                    <i class="las la-newspaper"></i>
                                </div>
                                <div class="title">{{ translate('Confirmed')}}</div>
                            </li>
                            <li class="list-inline-item @if($status == 'on_delivery') active @elseif($status == 'delivered') done @endif">
                                <div class="icon">
                                    <i class="las la-truck"></i>
                                </div>
                                <div class="title">{{ translate('On delivery')}}</div>
                            </li>
                            <li class="list-inline-item @if($status == 'delivered') done @endif">
                                <div class="icon">
                                    <i class="las la-clipboard-check"></i>
                                </div>
                                <div class="title">{{ translate('Delivered')}}</div>
                            </li>
                        </ul>
                    </div>
                    @if($orderDetail->product != null)
                    <div class="p-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ translate('Product Name')}}</th>
                                    <th>{{ translate('Quantity')}}</th>
                                    <th>{{ translate('Shipped By')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>{{ $orderDetail->product->getTranslation('name') }} ({{ $orderDetail->variation }})</td>
                                    <td>{{ $orderDetail->quantity }}</td>
                                    <td>{{ $orderDetail->product->user->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            @endforeach

        @endisset
    </div>
</section>

@endsection
