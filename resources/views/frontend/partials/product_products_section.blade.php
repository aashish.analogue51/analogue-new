<!-- Tab panes -->
<div class="tab-content">
@if(isset($topCategories))
    @foreach ($topCategories as $key => $category)
    <div role="tabpanel" class="tab-pane @if($key+1 == 1) active @endif" id="cat{{$category->id}}">
        <section class="topcategories_product_item">
            <div class="container">
                <div class=" py-md-3 bg-white shadow-sm rounded">
                    <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                        @foreach ($category->products as $key => $product)
                        <div class="carousel-box">
                            <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                                <div class="position-relative">
                                    <a href="{{ route('product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-140px h-md-210px"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                    <div class="absolute-top-right aiz-p-hov-icon">
                                        <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                                            <i class="la la-heart-o"></i>
                                        </a>
                                        <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                                            <i class="las la-sync"></i>
                                        </a>
                                        <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="p-md-3 p-2">
                                    <div class="star-rating star-rating-sm mt-1">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0;">{{ renderStarRating($product->rating) }}</div>
                                            <div class="col" align="right"><span class="rating-number">(0 Reviews)</span></div>                                                    
                                        </div>

                                    </div>
                                    <div class="sub-cat">{{ __($product->category->name) }}</div>
                                    <h2 class="product-title p-0">
                                        <a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
                                    </h2>
                                    <div class="price-box">
                                        @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                        <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                                        @endif
                                        <span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
                                    </div>
                                    <div class="Product-cart-2-footer-btn">
                                        <button class="add-to-cart p-all" title="Add to Cart" onclick="showAddToCartModal({{ $product->id }})">
                                            <i class="las la-cart-arrow-down"></i> Add To Cart
                                        </button>
                                        <button class="add-to-compare btn" title="Add to Compare" onclick="addToCompare({{ $product->id }})" tabindex="0">
                                            <i class="la la-refresh opacity-80"></i>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endforeach
    @endif
</div>