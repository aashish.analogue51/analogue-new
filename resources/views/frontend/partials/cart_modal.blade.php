<!--Cart Modal--->
{{-- <div class="checkout-sidebox-analogue">--}}
    <div class="modal right fade cart-modal" id="myModal233" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">{{translate('All Cart Items')}}</h4>
                    <button type="button" class="close" id="close-btn" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                </div>
                @if(Session::has('cart'))
                @if(count($cart = Session::get('cart')) > 0)
                <div class="modal-body">
                    <ul class="overflow-auto c-scrollbar-light list-group list-group-flush">
                        @php
                        $total = 0;
                        @endphp
                        @foreach($cart as $key => $cartItem)
                        @php
                        $product = \App\Product::find($cartItem['id']);
                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                        @endphp
                        @if ($product != null)
                        <li class="list-group-item">
                            <span class="d-flex align-items-center">
                                <a href="{{ route('product', $product->slug) }}"
                                    class="text-reset d-flex align-items-center flex-grow-1">
                                    <img src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        class="img-fit lazyload size-60px rounded"
                                        alt="{{  $product->getTranslation('name')  }}">
                                    <span class="minw-0 pl-2 flex-grow-1">
                                        <span class="fw-600 mb-1 text-truncate-2">
                                            {{ $product->getTranslation('name') }}
                                        </span>
                                        <span class="">{{ $cartItem['quantity'] }}x</span>
                                        <span class="">{{ single_price($cartItem['price']) }}</span>
                                    </span>
                                </a>
                                <span class="">
                                    <button onclick="removeFromCart({{ $key }})"
                                        class="btn btn-sm btn-icon stop-propagation" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="la la-close"></i>
                                    </button>
                                </span>
                            </span>
                        </li>
                        @endif
                        @endforeach
                    </ul>

                </div>
                <div class="modal-footer">
                    <div class="px-3 py-2 fs-15 border-top d-flex justify-content-between">
                        <span class="opacity-60">{{translate('Subtotal')}}</span>
                        <span class="fw-600">{{ single_price($total) }}</span>
                    </div>
                    <div class="px-3 py-2 text-center border-top">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="{{ route('cart') }}" class="btn btn-soft-primary btn-sm">
                                    {{translate('View cart')}}
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary btn-sm">
                                    {{translate('Checkout')}}
                                </a>
                            </li>
                            @if (Auth::check())
                            @endif
                        </ul>
                    </div>
                </div>
                @else
                <div class="text-center p-3">
                    <i class="las la-frown la-3x opacity-60 mb-3"></i>
                    <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>
                </div>
                @endif
                @else
                <div class="text-center p-3">
                    <i class="las la-frown la-3x opacity-60 mb-3"></i>
                    <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>
                </div>
                @endif

            </div>
            <!-- modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
    {{--
</div>--}}