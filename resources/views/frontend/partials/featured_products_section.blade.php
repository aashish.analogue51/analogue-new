@if (isset($last_brand))
@php $products = $helper->getProducts($last_brand); @endphp
<section class="bg-white p-top35 p-bottom35">
    <div class="container">
        <div class="bg-white shadow-sm rounded">
            <div class="d-flex mb-3 align-items-baseline border-bottom">
                <h3 class="h5 fw-600 mb-0">
                    <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">{{ $helper->getBrandName($last_brand) }}</span>
                </h3>
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach ($products->take(10) as $key => $product)
                @include('frontend.partials.single_product')
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif