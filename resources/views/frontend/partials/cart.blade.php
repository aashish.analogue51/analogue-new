<a href="javascript:void(0)" class="d-flex   align-items-center text-reset h-100 cart-click" data-toggle="modal"
   data-target="#myModal233">
    <span class="flex-grow-1 ml-1 pos-rel d-none d-md-inline-block">
        @if(Session::has('cart'))
            <span class="badge badge-primary badge-inline badge-pill">{{ count(Session::get('cart'))}}</span>
        @else
            <span class="badge badge-primary badge-inline badge-pill"></span>
        @endif
        <!-- <i class="las la-cart-arrow-down la-2x opacity-80"></i> -->
        <svg xmlns="http://www.w3.org/2000/svg" style="height: 18px;" width="28.86" height="25.035" viewBox="0 0 28.86 25.035"><g transform="translate(0 0)"><path d="M498.913,166.626l-2.006-13.363a.745.745,0,0,0-.737-.639l-3.594-.022.01-1.659,3.594.022a2.4,2.4,0,0,1,2.367,2.052l.223,1.486,22.667.143-2.327,10.54Zm.107-10.463,1.3,8.7,17.432-1.244,1.618-7.328Z" transform="translate(-492.576 -150.943)" fill="#02afef"/><g transform="translate(5.821 14.669)"><path d="M518.034,166.29l-19.589-.114a1.437,1.437,0,0,1-1.411-1.668l.384-2.377,1.638.265-.344,2.123,19.332.113Z" transform="translate(-497.016 -162.131)" fill="#02afef"/></g><path d="M502.98,168.337a2.236,2.236,0,1,1-2.222-2.25A2.236,2.236,0,0,1,502.98,168.337Z" transform="translate(-490.73 -146.231)" fill="none" stroke="#02afef" stroke-miterlimit="10" stroke-width="1.265"/><path d="M511.93,168.394a2.236,2.236,0,1,1-2.221-2.251A2.236,2.236,0,0,1,511.93,168.394Z" transform="translate(-487.946 -146.214)" fill="none" stroke="#02afef" stroke-miterlimit="10" stroke-width="1.265"/></g></svg>
        <br>
        <span class="nav-box-text d-xl-block">{{translate('Cart')}}</span>
    </span>
</a>

{{--    <!--Cart Modal--->--}}
{{--    <div class="checkout-sidebox-analogue">--}}
{{--        <div class="modal right fade cart-modal" id="myModal233" tabindex="-1" role="dialog"--}}
{{--             aria-labelledby="myModalLabel2">--}}
{{--            <div class="modal-dialog" role="document">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <h4 class="modal-title" id="myModalLabel2">{{translate('All Cart Items')}}</h4>--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
{{--                                aria-hidden="true">&times;</span></button>--}}

{{--                    </div>--}}
{{--                    @if(Session::has('cart'))--}}
{{--                        @if(count($cart = Session::get('cart')) > 0)--}}
{{--                            <div class="modal-body">--}}
{{--                                <ul class="overflow-auto c-scrollbar-light list-group list-group-flush">--}}
{{--                                    @php--}}
{{--                                        $total = 0;--}}
{{--                                    @endphp--}}
{{--                                    @foreach($cart as $key => $cartItem)--}}
{{--                                        @php--}}
{{--                                            $product = \App\Product::find($cartItem['id']);--}}
{{--                                            $total = $total + $cartItem['price']*$cartItem['quantity'];--}}
{{--                                        @endphp--}}
{{--                                        @if ($product != null)--}}
{{--                                            <li class="list-group-item">--}}
{{--                            <span class="d-flex align-items-center">--}}
{{--                                <a href="{{ route('product', $product->slug) }}"--}}
{{--                                   class="text-reset d-flex align-items-center flex-grow-1">--}}
{{--                                    <img src="{{ static_asset('assets/img/placeholder.jpg') }}"--}}
{{--                                         data-src="{{ uploaded_asset($product->thumbnail_img) }}"--}}
{{--                                         class="img-fit lazyload size-60px rounded"--}}
{{--                                         alt="{{  $product->getTranslation('name')  }}">--}}
{{--                                    <span class="minw-0 pl-2 flex-grow-1">--}}
{{--                                        <span class="fw-600 mb-1 text-truncate-2">--}}
{{--                                            {{ $product->getTranslation('name') }}--}}
{{--                                        </span>--}}
{{--                                        <span class="">{{ $cartItem['quantity'] }}x</span>--}}
{{--                                        <span class="">{{ single_price($cartItem['price']) }}</span>--}}
{{--                                    </span>--}}
{{--                                </a>--}}
{{--                                <span class="">--}}
{{--                                    <button onclick="removeFromCart({{ $key }})"--}}
{{--                                            class="btn btn-sm btn-icon stop-propagation">--}}
{{--                                        <i class="la la-close"></i>--}}
{{--                                    </button>--}}
{{--                                </span>--}}
{{--                            </span>--}}
{{--                                            </li>--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
{{--                                </ul>--}}

{{--                            </div>--}}
{{--                            <div class="modal-footer">--}}
{{--                                <div class="px-3 py-2 fs-15 border-top d-flex justify-content-between">--}}
{{--                                    <span class="opacity-60">{{translate('Subtotal')}}</span>--}}
{{--                                    <span class="fw-600">{{ single_price($total) }}</span>--}}
{{--                                </div>--}}
{{--                                <div class="px-3 py-2 text-center border-top">--}}
{{--                                    <ul class="list-inline mb-0">--}}
{{--                                        <li class="list-inline-item">--}}
{{--                                            <a href="{{ route('cart') }}" class="btn btn-soft-primary btn-sm">--}}
{{--                                                {{translate('View cart')}}--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="list-inline-item">--}}
{{--                                            <a href="{{ route('checkout.shipping_info') }}"--}}
{{--                                               class="btn btn-primary btn-sm">--}}
{{--                                                {{translate('Checkout')}}--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        @if (Auth::check())--}}
{{--                                        @endif--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @else--}}
{{--                            <div class="text-center p-3">--}}
{{--                                <i class="las la-frown la-3x opacity-60 mb-3"></i>--}}
{{--                                <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    @else--}}
{{--                        <div class="text-center p-3">--}}
{{--                            <i class="las la-frown la-3x opacity-60 mb-3"></i>--}}
{{--                            <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--                <!-- modal-content -->--}}
{{--            </div>--}}
{{--            <!-- modal-dialog -->--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg p-0 stop-propagation">--}}
{{--    @if(Session::has('cart'))--}}
{{--        @if(count($cart = Session::get('cart')) > 0)--}}
{{--            <div class="p-3 fs-15 fw-600 p-3 border-bottom">--}}
{{--                {{translate('Cart Items')}}--}}
{{--                </div>--}}
{{--                <ul class="overflow-auto c-scrollbar-light list-group list-group-flush">--}}
{{--@php--}}
{{--                $total = 0;--}}
{{--            @endphp--}}
{{--            @foreach($cart as $key => $cartItem)--}}
{{--                @php--}}
{{--                    $product = \App\Product::find($cartItem['id']);--}}
{{--                    $total = $total + $cartItem['price']*$cartItem['quantity'];--}}
{{--                @endphp--}}
{{--                @if ($product != null)--}}
{{--                    <li class="list-group-item">--}}
{{--                        <span class="d-flex align-items-center">--}}
{{--                            <a href="{{ route('product', $product->slug) }}" class="text-reset d-flex align-items-center flex-grow-1">--}}
{{--                                    <img--}}
{{--                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"--}}
{{--                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"--}}
{{--                                        class="img-fit lazyload size-60px rounded"--}}
{{--                                        alt="{{  $product->getTranslation('name')  }}"--}}
{{--                                    >--}}
{{--                                    <span class="minw-0 pl-2 flex-grow-1">--}}
{{--                                        <span class="fw-600 mb-1 text-truncate-2">--}}
{{--                                                {{  $product->getTranslation('name')  }}--}}
{{--                        </span>--}}
{{--                        <span class="">{{ $cartItem['quantity'] }}x</span>--}}
{{--                                        <span class="">{{ single_price($cartItem['price']) }}</span>--}}
{{--                                    </span>--}}
{{--                                </a>--}}
{{--                                <span class="">--}}
{{--                                    <button onclick="removeFromCart({{ $key }})" class="btn btn-sm btn-icon stop-propagation">--}}
{{--                                        <i class="la la-close"></i>--}}
{{--                                    </button>--}}
{{--                                </span>--}}
{{--                            </span>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--            @endforeach--}}
{{--                </ul>--}}
{{--                <div class="px-3 py-2 fs-15 border-top d-flex justify-content-between">--}}
{{--                    <span class="opacity-60">{{translate('Subtotal')}}</span>--}}
{{--                <span class="fw-600">{{ single_price($total) }}</span>--}}
{{--            </div>--}}
{{--            <div class="px-3 py-2 text-center border-top">--}}
{{--                <ul class="list-inline mb-0">--}}
{{--                    <li class="list-inline-item">--}}
{{--                        <a href="{{ route('cart') }}" class="btn btn-soft-primary btn-sm">--}}
{{--                            {{translate('View cart')}}--}}
{{--                </a>--}}
{{--            </li>--}}
{{--@if (Auth::check())--}}
{{--                <li class="list-inline-item">--}}
{{--                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-primary btn-sm">--}}
{{--                            {{translate('Checkout')}}--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--@endif--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        @else--}}
{{--            <div class="text-center p-3">--}}
{{--                <i class="las la-frown la-3x opacity-60 mb-3"></i>--}}
{{--                <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    @else--}}
{{--        <div class="text-center p-3">--}}
{{--            <i class="las la-frown la-3x opacity-60 mb-3"></i>--}}
{{--            <h3 class="h6 fw-700">{{translate('Your Cart is empty')}}</h3>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--        </div> -->--}}
