@if (isset($first_brand))
@php $products = $helper->getProducts($first_brand); @endphp
<section class="p-top35 p-bottom35 bg-white">
    <div class="container">
        <div class=" py-4 py-md-3  shadow-sm rounded">
            <div class="d-flex mb-3 align-items-baseline border-bottom">
                <h3 class="h5 fw-600 mb-0">
                    <span class="border-bottom border-primary border-width-2 pb-3 d-inline-block">{{ $helper->getBrandName($first_brand) }}</span>
                </h3>
                <a href="javascript:void(0)" class="ml-auto mr-0 btn btn-primary btn-sm shadow-md">{{ translate('Top 10') }}</a>
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach ($products->take(10) as $key => $product)
                @include('frontend.partials.single_product')
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif


