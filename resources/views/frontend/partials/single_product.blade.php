<div class="carousel-box">
    <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
        <div class="position-relative">
            <a href="{{ route('product', $product->slug) }}" class="d-block">
                <img
                class="img-fit lazyload mx-auto h-auto"
                src="{{ static_asset('assets/img/placeholder.jpg') }}"
                data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                alt="{{  $product->getTranslation('name')  }}"
                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                >
            </a>
            <div class="absolute-top-right aiz-p-hov-icon">
                <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                    <i class="la la-heart-o"></i>
                </a>
                <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                    <i class="las la-sync"></i>
                </a>
                <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                    <i class="las la-cart-arrow-down"></i>
                </a>
            </div>
        </div>

        <div class="p-md-3 p-2">
            <div class="star-rating star-rating-sm mt-1">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0;">{{ renderStarRating($product->rating) }}</div>
                    <div class="col" align="right"><span class="rating-number">(0 Reviews)</span></div>                                                    
                </div>

            </div>
            <div class="sub-cat">{{ __($product->category ? $product->category->name : '') }}</div>
            <h2 class="product-title p-0">
                <a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
            </h2>
            <div class="price-box">
                @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                @else
               <br>
                @endif
                <span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
            </div>
            <div class="Product-cart-2-footer-btn text-left">
                <div class="d-inline-block">
                     <button class="add-to-cart p-all btn" title="Add to Cart" onclick="showAddToCartModal({{ $product->id }})" tabindex="0">
                        <i class="fa fa-shopping-cart"></i> 
                        <span class="cart-name">Add To Cart</span>
                    </button>
                    <a href="javascript:void(0)" class="product-footer-a cart-compare" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                        <svg xmlns="http://www.w3.org/2000/svg" width="33.147" height="23.736" viewBox="0 0 33.147 23.736">
                          <path id="Path_281" data-name="Path 281" d="M56.035,126.3H44.417v3.391H56.035v5.087L62.648,128l-6.613-6.782Zm9.911-1.7v-5.087H77.564v-3.391H65.946v-5.087l-6.613,6.782Z" transform="translate(-44.417 -111.042)" fill="#fff"/>
                        </svg>

                    </a>
                    
                </div>
                
            </div>
    </div>
</div>
</div>