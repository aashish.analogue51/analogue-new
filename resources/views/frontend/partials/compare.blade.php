<a href="{{ route('compare') }}" class="d-flex pos-rel text-reset">
    <span class="flex-grow-1 ml-1 align-items-center">
        @if(Session::has('compare'))
            <span class="badge badge-primary badge-inline badge-pill">{{ count(Session::get('compare'))}}</span>
        @else
            <span class="badge badge-primary badge-inline badge-pill"></span>
        @endif
    	<!-- <i class="la la-refresh la-2x opacity-80"></i> -->
    	<svg xmlns="http://www.w3.org/2000/svg" style="height: 18px;" width="33.147" height="23.736" viewBox="0 0 33.147 23.736"><path d="M56.035,126.3H44.417v3.391H56.035v5.087L62.648,128l-6.613-6.782Zm9.911-1.7v-5.087H77.564v-3.391H65.946v-5.087l-6.613,6.782Z" transform="translate(-44.417 -111.042)" fill="#02afef"/></svg><br>
        <span class="nav-box-text d-xl-block">{{translate('Compare')}}</span>
    </span>
</a>