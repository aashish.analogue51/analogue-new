<a href="{{ route('wishlists.index') }}" class="d-flex pos-rel text-reset">
    <span class="flex-grow-1 ml-1 align-items-center ">
        @if(Auth::check())
            <span class="badge badge-primary badge-inline badge-pill">{{ count(Auth::user()->wishlists)}}</span>
        @else
            <span class="badge badge-primary badge-inline badge-pill"></span>
        @endif
    	<!-- <i class="la la-heart-o la-2x opacity-80"></i> -->
    	<svg xmlns="http://www.w3.org/2000/svg" style="height: 18px;" width="24.422" height="23.407" viewBox="0 0 24.422 23.407"><path d="M91.236,270.7a6.766,6.766,0,0,0-6.862,2.809,6.772,6.772,0,0,0-6.862-2.809,6.037,6.037,0,0,0-4.6,6.058c-.149,8.574,11.218,15.6,11.333,15.635a.438.438,0,0,0,.126.019.433.433,0,0,0,.127-.019c.116-.035,11.482-7.07,11.333-15.635A6.036,6.036,0,0,0,91.236,270.7Zm3.719,6.073c.13,7.446-9.248,13.813-10.581,14.68-1.333-.866-10.711-7.231-10.581-14.68a5.182,5.182,0,0,1,3.918-5.218,4.964,4.964,0,0,1,1.123-.128A6.283,6.283,0,0,1,84,274.521a.449.449,0,0,0,.744,0,6.009,6.009,0,0,1,6.291-2.963A5.182,5.182,0,0,1,94.955,276.776Z" transform="translate(-72.163 -269.757)" fill="#02afef" stroke="#02afef" stroke-width="1.5"/></svg>
    	<br>
        <span class="nav-box-text d-xl-block">{{translate('Wishlist')}}</span>
    </span>
</a>
