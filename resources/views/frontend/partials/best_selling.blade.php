<div class="container">
    <div class="row gutters-10">
        <div class="col-md-4">
            <div class="align-items-baseline m-bottom15 border-bottom">
                <h3 class="border-bottom border-primary border-width-2  d-inline-block">Best Deal</h3>
            </div>
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="1" data-xl-items="1" data-lg-items="1"  data-md-items="1" data-sm-items="1" data-rows="1" data-xs-items="1"data-arrows='flase' data-infinite='true'>
               @foreach ($flashDeal as $key => $product)
               <div class="carousel-box">
                <div class="aiz-card-box border  border-light rounded hov-shadow-md my-2 has-transition">
                    <div class="position-relative ">
                        <a href="{{ route('product', $product->slug) }}" class="d-block p-all">
                            <img
                            class="img-fit lazyload mx-auto h-300px h-300px h-md-300px h-sm-300px"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                            alt="{{  $product->getTranslation('name')  }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                            >
                        </a>
                        <div class="absolute-top-right aiz-p-hov-icon">
                            <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                                <i class="la la-heart-o"></i>
                            </a>
                            <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                                <i class="las la-sync"></i>
                            </a>
                            <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </div>
                    </div>

                    <div class="p-md-3 p-2 ">

                        <h2 class="product-title p-0">
                            <a href="{{ route('product', $product->slug) }}" class=" text-truncate" >{{ __($product->name) }}</a>
                        </h2>
                        <div class="blue-bg p-all">
                            <div class="row">
                                <div class="col-12" style="text-align: center;">
                                    @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                                    @endif 
                                </div>
                                <div class="col-12" style="text-align: center;">
                                    <span class="product-price strong-800 font-size24" style="font-weight: bold;">{{ home_discounted_base_price($product->id) }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="star-rating  star-rating-sm mt-1 padding-right ">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0; color: #fff;">{{ renderStarRating($product->rating) }}</div>
                                <div class="col" align="right"><span class="rating-number">(0 Reviews)</span></div>                                                    
                            </div>

                        </div>                        
                   
                        <div class="Product-cart-2-footer-btn">
                            <div class="row">
                                <div class="col-12 text-left">
                                   <button class="add-to-cart p-all btn" title="Add to Cart" onclick="showAddToCartModal({{ $product->id }})" tabindex="0">
                                        <i class="las la-cart-arrow-down"></i> Add To Cart
                                    </button>
                                    <button class="add-to-cart p-all btn" title="Add to wishlist" onclick="addToCompare({{ $product->id }})" tabindex="0">
                                       <i class="la la-heart-o"></i>
                                   </button>
                               </div>
                           </div>
                       </div>
               </div>
           </div>
       </div>
       @endforeach
   </div>
</div>
<div class="col-md-8 col-sm-12 best-selling-8">
    <div class="row">
        @foreach(json_decode($topthree_bar->value) as $key=>$topbar)
        <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
            <div class="smal-product-box">
                <div class="align-items-baseline m-bottom15 border-bottom">
                    <h3 class="border-bottom border-primary border-width-2  d-inline-block">
                        {{$helper->getCategory($topbar)? $helper->getCategory($topbar)->name : '' }}
                    </h3>                    
                </div>
                <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="1" data-xl-items="1" data-lg-items="1"  data-md-items="1" data-sm-items="1" data-rows="6" data-xs-items="1"data-arrows='true' data-infinite='true'>

                  @php $products = $helper->getCatProducts($topbar); @endphp
                  @if(count($products))
                  @foreach($products as $key=> $product)
                  <div class="carousel-box">
                    <div class="aiz-card-box border border-light rounded hov-shadow-md has-transition">
                        <div class="p-all small-product-box">
                            <div class="row align-items-center">
                                <div class="col-5">
                                    <a href="{{ route('product', $product->slug) }}" class="d-block">
                                        <img
                                        class="img-fit lazyload mx-auto h-50 h-md-50 h-sm-70"
                                        src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                        data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                                        alt="{{  $product->getTranslation('name')  }}"
                                        onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                        >
                                    </a>
                                </div>
                                <div class="col-7 text-start">                                                
                                    <h2 class="product-title p-0">
                                        <a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
                                    </h2>
                                    <div class="price-box">
                                        @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                        <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                                        @endif
                                        <span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
    @endforeach


</div>
</div>
{{--
    <div class="col-md-8 col-sm-12">
        <div class="align-items-baseline m-bottom15 border-bottom">
            <h3 class="border-bottom border-primary border-width-2  d-inline-block">Best Brand Products</h3>
        </div>
        <div class="">
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="3"  data-md-items="3" data-sm-items="3" data-rows="2" data-xs-items="2"data-arrows='true' data-infinite='true'>
              @foreach ($top10_brands as $key => $value)
              @php $products = $helper->getProducts($value); @endphp
              @if(count($products))
              @foreach($products as $key=> $product)
              <div class="carousel-box">
                <div class="aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
                    <div class="position-relative">
                        <a href="{{ route('product', $product->slug) }}" class="d-block">
                            <img
                            class="img-fit lazyload mx-auto h-200px h-md-140px h-sm-200px"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                            alt="{{  $product->getTranslation('name')  }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                            >
                        </a>
                        <div class="absolute-top-right aiz-p-hov-icon">
                            <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                                <i class="la la-heart-o"></i>
                            </a>
                            <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                                <i class="las la-sync"></i>
                            </a>
                            <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </div>
                    </div>

                    <div class="p-md-3 p-2 p-top0">
                        <div class="star-rating star-rating-sm mt-1">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 0;">{{ renderStarRating($product->rating) }}</div>
                                <!-- <div class="col" align="left"><span class="rating-number">(0 Reviews)</span></div> -->                                                    
                            </div>

                        </div>
                        <div class="sub-cat">{{ __($product->category->name) }}</div>
                        <h2 class="product-title p-0">
                            <a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
                        </h2>
                        <div class="price-box">
                            @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                            <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                            @endif
                            <span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @endforeach
        </div>

    </div>


    
</div>
--}}
</div>
</div>