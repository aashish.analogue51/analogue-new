<div class="container">
  <div class="tab-content product-slider-ana">
    <div role="tabpanel" class="tab-pane tab-pane-first active " id="trending">
        @if ($bestSelling->value == 1)       
               <div class=" py-md-3 bg-white shadow-sm rounded">
                    <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-rows="2" data-xs-items="2"data-arrows='true' data-infinite='true'>
                       @foreach ($bestProd as $key => $product)
                           @include('frontend.partials.single_product')
                       @endforeach
                   </div>
               </div>           
        @endif
    </div>
    <div role="tabpanel" class="tab-pane  active" id="featured">
            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-rows="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach ($featuredProd as $key => $product)
                    @include('frontend.partials.single_product')
                @endforeach
            </div>
    </div>
    <div role="tabpanel" class="tab-pane  active" id="narrival">
        <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="4" data-xl-items="4" data-lg-items="4"  data-md-items="3" data-sm-items="2" data-rows="2" data-xs-items="2" data-arrows='true' data-infinite='true'>
                @foreach ($latestProd as $key => $product)
                    @include('frontend.partials.single_product')
                @endforeach
            </div>
    </div>
    <!-- <div role="tabpanel" class="tab-pane" id="latest">d...</div> -->
</div>
</div>