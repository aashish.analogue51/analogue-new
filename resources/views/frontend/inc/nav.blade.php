<!-- Top Bar -->
<!-- <div class="top-navbar border-bottom border-soft-secondary z-1035">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col">
                <ul class="list-inline d-flex justify-content-between justify-content-lg-start mb-0">
                    @if(get_setting('show_language_switcher') == 'on')
                    <li class="list-inline-item dropdown mr-3" id="lang-change">
                        @php
                            if(Session::has('locale')){
                                $locale = Session::get('locale', Config::get('app.locale'));
                            }
                            else{
                                $locale = 'en';
                            }
                        @endphp
                        <a href="javascript:void(0)" class="dropdown-toggle text-reset py-2" data-toggle="dropdown" data-display="static">
                            <img src="{{ static_asset('assets/img/placeholder.jpg') }}" data-src="{{ static_asset('assets/img/flags/'.$locale.'.png') }}" class="mr-2 lazyload" alt="{{ \App\Language::where('code', $locale)->first()->name }}" height="11">
                            <span class="opacity-60">{{ \App\Language::where('code', $locale)->first()->name }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-left" >
                            @foreach (\App\Language::all() as $key => $language)
                                <li>
                                    <a href="javascript:void(0)" data-flag="{{ $language->code }}" class="dropdown-item @if($locale == $language) active @endif">
                                        <img src="{{ static_asset('assets/img/placeholder.jpg') }}" data-src="{{ static_asset('assets/img/flags/'.$language->code.'.png') }}" class="mr-1 lazyload" alt="{{ $language->name }}" height="11">
                                        <span class="language">{{ $language->name }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @endif
    
                    @if(get_setting('show_currency_switcher') == 'on')
                    <li class="list-inline-item dropdown" id="currency-change">
                        @php
                            if(Session::has('currency_code')){
                                $currency_code = Session::get('currency_code');
                            }
                            else{
                                $currency_code = \App\Currency::findOrFail(\App\BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
                            }
                        @endphp
                        <a href="javascript:void(0)" class="dropdown-toggle text-reset py-2 opacity-60" data-toggle="dropdown" data-display="static">
                            {{ \App\Currency::where('code', $currency_code)->first()->name }} {{ (\App\Currency::where('code', $currency_code)->first()->symbol) }}
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left">
                            @foreach (\App\Currency::where('status', 1)->get() as $key => $currency)
                                <li>
                                    <a class="dropdown-item @if($currency_code == $currency->code) active @endif" href="javascript:void(0)" data-currency="{{ $currency->code }}">{{ $currency->name }} ({{ $currency->symbol }})</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
    
            <div class="col-5 text-right d-none d-lg-block">
                <ul class="list-inline mb-0">
                    @auth
                        @if(isAdmin())
                            <li class="list-inline-item mr-3">
                                <a href="{{ route('admin.dashboard') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('My Panel')}}</a>
                            </li>
                        @else
                            <li class="list-inline-item mr-3">
                                <a href="{{ route('dashboard') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('My Panel')}}</a>
                            </li>
                        @endif
                        <li class="list-inline-item">
                            <a href="{{ route('logout') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('Logout')}}</a>
                        </li>
                    @else
                        <li class="list-inline-item mr-3">
                            <a href="{{ route('user.login') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('Login')}}</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ route('user.registration') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('Registration')}}</a>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </div>
</div> -->
<div class="top-navbar">
    <div class="container">
        <div class="row topbar-rightsection">
            <div class="col-lg-6 col">
                <ul class="inline-links d-lg-inline-block d-flex justify-content-between">
                    <li class="topoffer">
                        <p>Get 5% off on your first purchage. Hurry now!</p>
                    </li>
                </ul>
            </div>
            <div class="col-6  text-right d-lg-block ">
                <ul class="inline-links topbar-right123">

                    <!-- <li class="dropdown list-inline-item m-hide-analogue" id="currency-change">
                        <img src="{{ static_asset('frontend/images/icons/curency.svg') }}">                           
                                
                                @php
                                if(Session::has('currency_code')){
                                $currency_code = Session::get('currency_code');
                            }
                            else{
                            $currency_code = \App\Currency::findOrFail(\App\BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
                        }
                        @endphp
                        <a href="" class="dropdown-toggle top-bar-item" data-toggle="dropdown">
                            {{ \App\Currency::where('code', $currency_code)->first()->name }}
                        </a>
                        <ul class="dropdown-menu">
                            @foreach (\App\Currency::where('status', 1)->get() as $key => $currency)
                            <li class="dropdown-item @if($currency_code == $currency->code) active @endif">
                                <a href="" data-currency="{{ $currency->code }}">{{ $currency->name }} ({{ $currency->symbol }})</a>
                            </li>
                            @endforeach
                        </ul>
                    </li> -->
                    <!-- <li class="dropdown list-inline-item m-hide-analogue" id="lang-change">
                        <img src="{{static_asset('frontend/images/icons/language.svg') }}">                           
                        @php
                        if(Session::has('locale')){
                        $locale = Session::get('locale', Config::get('app.locale'));
                        }
                        else{
                        $locale = 'en';
                        }
                        @endphp
                        <a href="" class="dropdown-toggle top-bar-item" data-toggle="dropdown">
                        <span class="language">{{ \App\Language::where('code', $locale)->first()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                        @foreach (\App\Language::all() as $key => $language)
                        <li class="dropdown-item @if($locale == $language) active @endif">
                        <a href="#" data-flag="{{ $language->code }}"><img src="{{ static_asset('frontend/images/placeholder.jpg') }}" data-src="{{ static_asset('frontend/images/icons/flags/'.$language->code.'.png') }}" class="flag lazyload" alt="{{ $language->name }}" height="11"><span class="language">{{ $language->name }}</span></a>
                        </li>
                        @endforeach
                        </ul>
                    </li> -->
                    @auth
                    @if(isAdmin())
                    <li class="list-inline-item mr-3">
                        <a href="{{ route('orders.track') }}" class="top-bar-item">{{ translate('Track Order')}}</a>
                    </li>
                    <li class="list-inline-item mr-3">
                        <a href="{{ route('admin.dashboard') }}" class="text-reset d-inline-block opacity-60"
                        style="padding: 5px 0">{{ translate('My Panel')}}</a>
                    </li>
                    @else
                    <li class="list-inline-item mr-3">
                        <a href="{{ route('dashboard') }}" style="padding: 5px 0;"
                        class="text-reset d-inline-block opacity-60">{{ translate('My Panel')}}</a>
                    </li>
                    @endif
                    <li class="list-inline-item">
                        <img src="{{static_asset('frontend/images/icons/login.svg')}}" style=" margin-top: -4px;">
                        <a href="{{ route('logout') }}" style="padding: 5px 0;"
                        class="text-reset d-inline-block opacity-60">{{ translate('Logout')}}</a>
                    </li>
                    @else
                    <li class="list-inline-item ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="15.56" height="15" viewBox="0 0 15.56 15">
                          <g id="dollar_1_" data-name="dollar (1)" transform="translate(0 -84.928)">
                            <g id="Group_72" data-name="Group 72" transform="translate(0 84.928)">
                              <g id="Group_71" data-name="Group 71" transform="translate(0 0)">
                                <path id="Path_187" data-name="Path 187" d="M15.439,85.563a.269.269,0,0,0-.276-.11l-.315.081a13.347,13.347,0,0,1-7.006-.08,13.8,13.8,0,0,0-7.248-.083l-.315.081a.485.485,0,0,0-.28.492V98.911a.611.611,0,0,0,.121.381A.269.269,0,0,0,.4,99.4l.315-.081a13.337,13.337,0,0,1,7.006.08,13.8,13.8,0,0,0,7.248.083l.315-.081a.485.485,0,0,0,.28-.492V85.944A.611.611,0,0,0,15.439,85.563Zm-.555,12.929-.035.009a13.347,13.347,0,0,1-7.006-.08,13.8,13.8,0,0,0-7.166-.1V86.363l.035-.008a13.34,13.34,0,0,1,7.007.079,13.8,13.8,0,0,0,7.166.1V98.492Z" transform="translate(0 -84.928)" fill="#02afef"/>
                            </g>
                        </g>
                        <g id="Group_74" data-name="Group 74" transform="translate(8.824 96.36)">
                          <g id="Group_73" data-name="Group 73" transform="translate(0 0)">
                            <path id="Path_188" data-name="Path 188" d="M345.087,328.789a.466.466,0,0,0-.51-.429,27.059,27.059,0,0,1-2.767.1H341.8a.471.471,0,0,0-.008.942c.152,0,.3,0,.454,0,.805,0,1.614-.035,2.409-.1A.47.47,0,0,0,345.087,328.789Z" transform="translate(-341.332 -328.358)" fill="#02afef"/>
                        </g>
                    </g>
                    <g id="Group_76" data-name="Group 76" transform="translate(2.966 87.448)">
                      <g id="Group_75" data-name="Group 75" transform="translate(0 0)">
                        <path id="Path_189" data-name="Path 189" d="M67.286,138.6c-.959-.013-1.926.018-2.875.1a.471.471,0,0,0,.04.94l.04,0a26.508,26.508,0,0,1,2.779-.1h.008a.471.471,0,0,0,.008-.942Z" transform="translate(-63.98 -138.601)" fill="#02afef"/>
                    </g>
                </g>
                <g id="Group_78" data-name="Group 78" transform="translate(5.425 89.135)">
                  <g id="Group_77" data-name="Group 77" transform="translate(0 0)">
                    <path id="Path_190" data-name="Path 190" d="M194.452,173.589l-.1-.02c-.959-.177-1.406-.473-1.406-.933,0-.511.647-.942,1.414-.942.738,0,1.372.4,1.413.891a.471.471,0,0,0,.938-.079,2.434,2.434,0,0,0-4.708.13c0,.967.733,1.593,2.261,1.875l.1.019c.958.177,1.4.473,1.4.933,0,.511-.647.942-1.414.942-.738,0-1.372-.4-1.413-.891a.471.471,0,1,0-.938.079,2.435,2.435,0,0,0,4.709-.129C196.71,174.495,195.978,173.87,194.452,173.589Z" transform="translate(-191.999 -170.752)" fill="#02afef"/>
                </g>
            </g>
            <g id="Group_80" data-name="Group 80" transform="translate(7.309 88.66)">
              <g id="Group_79" data-name="Group 79" transform="translate(0 0)">
                <path id="Path_191" data-name="Path 191" d="M235.137,160a.471.471,0,0,0-.471.471v6.594a.471.471,0,1,0,.942,0v-6.594A.471.471,0,0,0,235.137,160Z" transform="translate(-234.666 -160)" fill="#02afef"/>
            </g>
        </g>
    </g>
</svg>


</svg>

<a href="{{ route('user.login') }}" class="text-reset login-btn-text py-1 d-inline-block ">{{translate('USD')}}</a>
</li>
<li class="list-inline-item ">
    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15.56" viewBox="0 0 15 15.56">
      <g id="translate" transform="translate(0 0)">
        <path id="Path_184" data-name="Path 184" d="M317.159,301.4c-.037-.122-.187-.179-.341-.179a.33.33,0,0,0-.337.179l-.784,2.556a.237.237,0,0,0-.008.041c0,.13.191.219.333.219.089,0,.159-.028.179-.106l.154-.541h.931l.155.541c.02.077.089.106.179.106.142,0,.333-.093.333-.219a.179.179,0,0,0-.008-.041Zm-.7,1.756.354-1.248.354,1.248Zm0,0" transform="translate(-306.172 -291.607)" fill="#02afef"/>
        <path id="Path_185" data-name="Path 185" d="M13.384,6.347h-4.5v-2l1.463-1.084a.466.466,0,0,0,0-.742l-1.481-1.1A1.639,1.639,0,0,0,7.265,0H1.616A1.649,1.649,0,0,0,0,1.676V7.536A1.649,1.649,0,0,0,1.616,9.212h4.5v2L4.656,12.292a.466.466,0,0,0,0,.742l1.481,1.1a1.639,1.639,0,0,0,1.6,1.429h5.648A1.649,1.649,0,0,0,15,13.883V8.024a1.649,1.649,0,0,0-1.616-1.677ZM1.616,8.3a.752.752,0,0,1-.737-.765V1.676A.752.752,0,0,1,1.616.912H7.265A.752.752,0,0,1,8,1.676a.461.461,0,0,0,.184.371l1.147.85-1.147.85A.461.461,0,0,0,8,4.118v2.23H7.735A1.649,1.649,0,0,0,6.119,8.024V8.3Zm12.5,5.583a.752.752,0,0,1-.737.765H7.735A.752.752,0,0,1,7,13.883a.461.461,0,0,0-.184-.371l-1.147-.85,1.147-.85A.461.461,0,0,0,7,11.442V8.024a.752.752,0,0,1,.737-.765h5.648a.752.752,0,0,1,.737.765Zm0,0" fill="#02afef"/>
        <path id="Path_186" data-name="Path 186" d="M99.565,93.517a.214.214,0,0,0,0-.428h-.952v-.52a.214.214,0,1,0-.428,0v.52h-.952a.214.214,0,0,0,0,.428h.295a2.041,2.041,0,0,0,.535,1.177,1.611,1.611,0,0,1-.83.229.214.214,0,0,0,0,.428,2.035,2.035,0,0,0,1.166-.366,2.036,2.036,0,0,0,1.166.366.214.214,0,0,0,0-.428,1.609,1.609,0,0,1-.83-.229,2.041,2.041,0,0,0,.535-1.177Zm-1.166.909a1.616,1.616,0,0,1-.44-.909h.879A1.616,1.616,0,0,1,98.4,94.426Zm0,0" transform="translate(-94.072 -89.407)" fill="#02afef"/>
    </g>
</svg>

</svg>

<a href="{{ route('user.login') }}" class="text-reset login-btn-text py-1 d-inline-block">{{translate('English')}}</a>
</li>

<li class="list-inline-item ">
    <svg xmlns="http://www.w3.org/2000/svg" style="height: 16px;" width="18.56" height="18" viewBox="0 0 18.56 18">
      <g id="enter" transform="translate(0 -0.333)">
        <path id="Path_178" data-name="Path 178" d="M10.057,219.989H.495a.495.495,0,1,1,0-.989h9.562a.495.495,0,1,1,0,.989Zm0,0" transform="translate(0 -210.162)" fill="#02afef"/>
        <path id="Path_179" data-name="Path 179" d="M224.5,139.9a.495.495,0,0,1-.35-.845l2.288-2.288-2.288-2.288a.495.495,0,0,1,.7-.7l2.638,2.638a.5.5,0,0,1,0,.7l-2.638,2.638A.491.491,0,0,1,224.5,139.9Zm0,0" transform="translate(-215.044 -127.43)" fill="#02afef"/>
        <path id="Path_180" data-name="Path 180" d="M44.9,18.333a8.96,8.96,0,0,1-8.383-5.708.614.614,0,0,1,1.143-.448,7.773,7.773,0,1,0,0-5.688.614.614,0,0,1-1.143-.448A9.005,9.005,0,1,1,44.9,18.333Zm0,0" transform="translate(-35.344 0)" fill="#02afef"/>
    </g>
</svg>

<a href="{{ route('user.login') }}" class="text-reset login-btn-text py-1 d-inline-block">{{translate('Login')}}</a>
</li>


                    <!-- <li class="list-inline-item">
                        <a href="{{ route('user.registration') }}" class="text-reset py-2 d-inline-block opacity-60">{{ translate('Registration')}}</a>
                    </li> -->
                    @endauth
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- END Top Bar -->
{{-- <header
    class="@if(get_setting('header_stikcy') == 'on') sticky-top @endif z-1020 bg-white border-bottom shadow-sm">
    <div class="position-relative logo-bar-area">
        <div class="container">
            <div class="d-flex align-items-center">

                <div class="col-auto col-xl-3 pl-0 pr-3 d-flex align-items-center">
                    <a class="d-block py-20px mr-3 ml-0" href="{{ route('home') }}">
                        @php
                        $header_logo = get_setting('header_logo');
                        @endphp
                        @if($header_logo != null)
                        <img src="{{ uploaded_asset($header_logo) }}" alt="{{ env('APP_NAME') }}"
                        class="mw-100 h-30px h-md-40px" height="40">
                        @else
                        <img src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}"
                        class="mw-100 h-30px h-md-40px" height="40">
                        @endif
                    </a>

                    @if(Route::currentRouteName() != 'home')
                    <div class="d-none d-xl-block align-self-stretch category-menu-icon-box ml-auto mr-0">
                        <div class="h-100 d-flex align-items-center" id="category-menu-icon">
                            <div
                            class="dropdown-toggle navbar-light bg-light h-40px w-50px pl-2 rounded border c-pointer">
                            <span class="navbar-toggler-icon"></span>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="d-lg-none ml-auto mr-0">
                <a class="p-2 d-block text-reset" href="javascript:void(0);" data-toggle="class-toggle"
                data-target=".front-header-search">
                <i class="las la-search la-flip-horizontal la-2x"></i>
            </a>
        </div>

        <div class="flex-grow-1 front-header-search d-flex align-items-center bg-white">
            <div class="position-relative flex-grow-1 search-box">
                <form action="{{ route('search') }}" method="GET" class="stop-propagation">
                    <div class="d-flex position-relative align-items-center">
                        <div class="d-lg-none" data-toggle="class-toggle" data-target="front-header-search">
                            <button class="btn px-2" type="button"><i
                                class="la la-2x la-long-arrow-left"></i></button>
                            </div>
                            <div class="input-group">
                                <input type="text" class="border-0 border-lg form-control" id="search" name="q"
                                placeholder="{{translate('I am shopping for...')}}" autocomplete="off">
                                <div class="input-group-append d-none d-lg-block">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="la la-search la-flip-horizontal fs-18"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100"
                    style="min-height: 200px">
                    <div class="search-preloader absolute-top-center">
                        <div class="dot-loader">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="search-nothing d-none p-3 text-center fs-16">

                    </div>
                    <div id="search-content" class="text-left">

                    </div>
                </div>
            </div>
        </div>

        <div class="d-none d-lg-none ml-3 mr-0">
            <div class="nav-search-box">
                <a href="#" class="nav-box-link">
                    <i class="la la-search la-flip-horizontal d-inline-block nav-box-icon"></i>
                </a>
            </div>
        </div>

        <div class="d-none d-lg-block ml-3 mr-0">
            <div class="" id="compare1">
                @include('frontend.partials.compare')
            </div>
        </div>

        <div class="d-none d-lg-block ml-3 mr-0">
            <div class="" id="wishlist1">
                @include('frontend.partials.wishlist')
            </div>
        </div>

        <div class="d-none d-lg-block  align-self-stretch ml-3 mr-0" data-hover="dropdown">
            <div class="nav-cart-box dropdown h-100" id="cart_items1">
                @include('frontend.partials.cart')

            </div>
        </div>

    </div>
</div>
@if(Route::currentRouteName() != 'home')
<div class="hover-category-menu position-absolute w-100 top-100 left-0 right-0 d-none z-3"
id="hover-category-menu">
<div class="container">
    <div class="row gutters-10 position-relative">
        <div class="col-lg-3 position-static">
            @include('frontend.partials.category_menu')
        </div>
    </div>
</div>
</div>
@endif
</div>
</header> --}}
<div class="position-relative logo-bar-area py-2">
    <div class="">
        <div class="container">
            <div class="row no-gutters align-items-center">
                <div class="col-lg-3 col-6">
                    <div class="d-flex">
                        <!-- Brand/Logo -->
                        <a href="#" class="d-block d-md-none" data-toggle="modal" data-target=".bd-example-modal-sm">
                            <div class="burger_icon">
                                <span class="line1"></span>
                                <span class="line2"></span>
                                <span class="line3"></span>
                                
                            </div>
                        </a>

                        <a class="d-block py-10px mr-3 ml-0" href="{{ route('home') }}">
                            @php
                            $header_logo = get_setting('header_logo');
                            @endphp
                            @if($header_logo != null)
                            <img src="{{ uploaded_asset($header_logo) }}" alt="{{ env('APP_NAME') }}"
                            class="h-30px h-md-40px" height="40">
                            @else
                            <img src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}"
                            class="mw-100 h-30px h-md-40px" height="40">
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-6 position-static">
                    <div class="d-flex w-100">
                        <div class="search-box flex-grow-1 px-4 search-desktop">
                            <form action="{{ route('search') }}" method="GET">
                                <div class="d-flex position-relative">
                                    <div class="d-lg-none search-box-back">
                                        <button class="" type="button"><i class="la la-long-arrow-left"></i></button>
                                    </div>
                                    <div class="w-100">
                                        <input type="text" aria-label="Search" id="search" name="q" class="w-100"
                                        placeholder="{{translate('I am shopping for...')}}" autocomplete="off">
                                    </div>
                                    <button class="d-none d-lg-block" type="submit">
                                        <i class="la la-search la-flip-horizontal"></i>
                                    </button>
                                    <div class="typed-search-box d-none">
                                        <div class="search-preloader">
                                            <div class="loader">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div class="search-nothing d-none">
                                        </div>
                                        <div id="search-content">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="logo-bar-icons d-inline-block ml-auto">
                            <div class="d-lg-inline-block">
                                <div class="nav-compare-box" id="compare">
                                    @include('frontend.partials.compare')
                                </div>
                            </div>
                            <div class="d-lg-inline-block">
                                <div class="nav-wishlist-box" id="wishlist">
                                    @include('frontend.partials.wishlist')
                                </div>
                            </div>
                            <div class="d-inline-block" data-hover="dropdown">
                                <div class="nav-cart-box dropdown" id="cart_items">
                                    @include('frontend.partials.cart')


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-12 search-box-mobile">
                    <div class="search-box flex-grow-1 px-4 show">
                        <div class="row">
                            <div class="col-3 logo-fixed-icon">
                                <div class="row">
                                    <div class="col-3">
                                        <a href="#" class="d-block d-md-none" data-toggle="modal" data-target=".bd-example-modal-sm">
                                            <div class="burger_icon">
                                                <span class="line1"></span>
                                                <span class="line2"></span>
                                                <span class="line3"></span>
                                                
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <img src="{{static_asset('frontend/images/fav.png')}}" style=" margin-top: -4px;" class="img-fluid">
                                    </div>                                        
                                </div>
                                
                            </div>
                            <div class="col">
                                <form action="{{ route('search') }}" method="GET">
                                    <div class="d-flex position-relative">
                                        <div class="w-100">
                                            <input type="text" aria-label="Search" id="search" name="q" class="w-100"
                                            placeholder="{{translate('I am shopping for...')}}" autocomplete="off">
                                        </div>
                                        <button class="d-none d-lg-block" type="submit">
                                            <i class="la la-search la-flip-horizontal"></i>
                                        </button>
                                        <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader">
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </div>
                                            </div>
                                            <div class="search-nothing d-none">
                                            </div>
                                            <div id="search-content">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mainmenu d-none d-md-block">
    <div class="container">
        <div class="row ">
            <div class="col-lg-3 col-3 ">
                <a class="d-block menu-btn" data-toggle="modal" data-target=".bd-example-modal-sm">
                    All <span class="cat">Categories</span>
                    <div class="hamburger-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
            </div>
            <nav id="menu">
                <ul>
                    <div class="menu-header">
                        <i class="la la-user"></i> Hello,
                        <span class="mobile-hide-sidebar">
                            @auth
                            <li>
                                <a href="{{ route('dashboard') }}" class="">{{ translate(Auth::user()->name)}}</a>
                            </li>
                            <li class="side-logout">
                                <a href="{{ route('logout') }}" class="">{{ translate('Logout')}}</a>
                            </li>
                            @else
                            <li class="login-btn">
                                <a href="{{ route('user.login') }}" class="">{{ translate('Login')}}</a>
                            </li>
                            @endauth

                        </span>


                    </div>
                    <div class="mobile-compare-wishlist">
                        <span class="d-lg-inline-block">
                            <span class="nav-compare-box" id="compare2">
                                <a href="{{ route('compare') }}" class="nav-box-link">
                                    <img src="{{static_asset('frontend/images/icons/compare.svg')}}" style="">
                                    <span class="nav-box-text  d-xl-inline-block">{{translate('Compare')}}</span>
                                    @if(Session::has('compare'))
                                    <span class="nav-box-number">{{ count(Session::get('compare'))}}</span>
                                    @endif
                                </a>
                            </span>
                        </span>
                        <span class="d-lg-inline-block">
                            <span class="nav-wishlist-box" id="wishlist2">
                                <a href="{{ route('wishlists.index') }}" class="nav-box-link">
                                    <img src="{{static_asset('frontend/images/icons/wishlist.svg')}}" style="">
                                    <span class="nav-box-text d-xl-inline-block">{{translate('Wishlist')}}</span>
                                    @if(Auth::check())
                                    <span class="nav-box-number">{{ count(Auth::user()->wishlists)}}</span>
                                    @endif
                                </a>
                            </span>
                        </sapn>
                    </div>
                    @foreach ($categories as $key => $category)
                    <li>
                        <a href="{{ route('products.category', $category->slug) }}">
                            <span> {{ __($category->name) }}</span>
                        </a>
                        <ul>
                            <!-- <div class="menu-header"><i class="la la-user"></i> Hello, Login</div> -->
                            <div class="mainmemu">
                                <a class="mm-btn mm-btn_prev mm-navbar__btn" href="#mm-1">
                                    <span class="mm-sronly">Close submenu</span>
                                    <span>Main Menu</span>
                                </a>
                            </div>
                            @if(count($subcategories))
                            @foreach($subcategories->where('parent_id',$category->id) as $key => $subcat)
                            <li>
                                <a href="{{ route('products.category', $subcat->slug) }}">
                                    {{$subcat-> name}}
                                </a>
                            </li>


                            @endforeach
                            @else
                            <li><a href="#">No Categories</a></li>
                            @endif
                        </ul>
                    </li>

                    @endforeach

                    @auth
                    <li>
                        <a href="{{ route('dashboard') }}" class="">{{ translate('My Panel')}}</a>
                    </li>
                    <li class="side-logout">
                        <a href="{{ route('logout') }}" class="">{{ translate('Logout')}}</a>
                    </li>
                    @else
                    <li class="login-btn">
                        <!-- <a href="{{ route('user.login') }}" class="">{{ translate('Login')}}</a> -->
                    </li>
                    @endauth
                    <div class="mobile-top-hide">
                        <li class="dropdown" id="currency-change">
                            <img src="{{ static_asset('frontend/images/icons/curency.svg') }}">
                            @php
                            if(Session::has('currency_code')){
                            $currency_code = Session::get('currency_code');
                        }
                        else{
                        $currency_code = $currencyCode;
                    }
                    @endphp
                    <a href="" class="dropdown-toggle top-bar-item" data-toggle="dropdown">
                        {{$currencies ? $currencies->where('code', $currency_code)->first()->name : '' }}
                        <!--  {{ (\App\Currency::where('code', $currency_code)->first()->symbol) }} -->
                    </a>
                    <ul class="dropdown-menu">
                        @foreach ($currencies as $key => $currency)
                        <li class="dropdown-item @if($currency_code == $currency->code) active @endif">
                            <a href="" data-currency="{{ $currency->code }}">{{ $currency->name }} ({{
                                $currency->symbol }})</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="dropdown" id="lang-change">
                        <img src="{{static_asset('frontend/images/icons/language.svg') }}">
                        @php
                        if(Session::has('locale')){
                        $locale = Session::get('locale', Config::get('app.locale'));
                    }
                    else{
                    $locale = 'en';
                }
                @endphp
                <a href="" class="dropdown-toggle top-bar-item" data-toggle="dropdown">
                    <span class="language">{{ $languages ? $languages->where('code', $locale)->first()->name
                    : '' }}</span>
                </a>
                <ul class="dropdown-menu">
                    @foreach ($languages as $key => $language)
                    <li class="dropdown-item @if($locale == $language) active @endif">
                        <a href="#" data-flag="{{ $language->code }}"><img
                            src="{{ static_asset('frontend/images/placeholder.jpg') }}"
                            data-src="{{ static_asset('frontend/images/icons/flags/'.$language->code.'.png') }}"
                            class="flag lazyload" alt="{{ $language->name }}" height="11"><span
                            class="language">{{ $language->name }}</span></a>
                        </li>
                        @endforeach
                    </ul>
                </li>
            </div>

        </ul>
    </nav>

    <div class="col-lg-9 col non-categories text-sm-center text-md-left">
        <ul>
            <li><a href="">Analogue Trade In</a></li>
            <li><a href="{{route('emi')}}">EMI</a></li>
            <li><a href="{{route('returnpolicy')}}">Return Policy</a></li>
            <li><a href="{{route('about')}}">About Us</a></li>
            <li><a href="{{route('contact')}}">Contact</a></li>
            <li class="c-search-btn" data-toggle="modal" data-target="#myModal"><a href="#">Customize Search</a></li>
        </ul>
    </div>
</div>
</div>
</div>

<div class="modal bd-example-modal-sm side-navigation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <i class="fa fa-user"></i>
        <p class="">
            @auth
            Hello,
            <a href="{{ route('dashboard') }}" class="sidebar-a">{{ translate(Auth::user()->name)}}</a>
            @else
            <a href="{{ route('user.login') }}" class="sidebar-a">{{ translate('Login')}}</a>
            @endauth                        
        </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs navigation-tab" role="tablist">
        <li role="presentation" class="navigation-menu">
            <a href="#category" aria-controls="category" role="tab" data-toggle="tab" class="active">Category</a>                
        </li>            
        <li role="presentation" class="navigation-menu"><a href="#menu1" aria-controls="menu1" role="tab" data-toggle="tab">Menu</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane " id="menu1">
            <ul>
                <li><a class="a-link" href="">Analogue Trade In</a></li>
                <li><a class="a-link" href="{{route('emi')}}">EMI</a></li>
                <li><a class="a-link" href="{{route('returnpolicy')}}">Return Policy</a></li>
                <li><a class="a-link" href="{{route('about')}}">About Us</a></li>
                <li><a class="a-link" href="{{route('contact')}}">Contact</a></li>
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane active" id="category">
            <ul>
                <li><a href="{{route('categories.all')}}" class="a-link side-bar-nav" data-foggle="0" ><img class="d-none mr-2" src="{{static_asset('assets/img/categories-all.webp')}}"><strong>All Categories</strong></a></li>
                @foreach ($categories as $key => $category)
                <li>
                    <a @if(!count(\App\Utility\CategoryUtility::get_immediate_children_ids($category->id))) href="{{ route('products.category', $category->slug) }}" @endif class="a-link side-bar-nav" data-foggle="0" >
                        <!-- <i class="las la-laptop"></i> -->
                        <img class="d-none" src="{{ uploaded_asset($category->icon) }}" data-src="{{ uploaded_asset($category->icon) }}">
                        <strong>{{ __($category->name) }}</strong>
                        @if(count(\App\Utility\CategoryUtility::get_immediate_children_ids($category->id)))
                        <span><i class="las la-angle-right"></i></span>
                        @endif
                    </a>
                    @if(count(\App\Utility\CategoryUtility::get_immediate_children_ids($category->id)))
                    <ul class="subcat ml-3">
                        @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($category->id) as $key => $first_level_id)
                        <li>
                            <a  @if(!count(\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id))) href="{{ route('products.category', \App\Category::find($first_level_id)->slug) }}" @endif  class="a-link side-bar-nav1 in"> {{\App\Category::find($first_level_id)->getTranslation('name')}}
                                @if(count(\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id)))
                                <span><i class="las la-angle-right"></i></span>
                                @endif
                            </a>
                            @if(count(\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id)))
                            <ul class="subcat2 pl-2">
                             @foreach (\App\Utility\CategoryUtility::get_immediate_children_ids($first_level_id) as $key => $second_level_id)
                             <li>
                              <a class="text-capitalize a-link in" href="{{ route('products.category', \App\Category::find($second_level_id)->slug) }}" >{{ \App\Category::find($second_level_id)->getTranslation('name') }}</a>
                            </li>
                             @endforeach
                         </ul>
                         @endif
                     </li>
                     @endforeach
                 </ul>
                 @endif
             </li>                    
             @endforeach
         </ul>
     </div>

 </div>

</div>

</div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
    </div> -->
</div>
</div>
</div>

<!-- 
    <script  type="text/javascript">
      var side_menu = document.getElementById('close_btn');
    
      var remove_menu = document.getElementById('menu');
      side_menu.addEventListener('click', removeMenu, false);

      function removeMenu() {
       remove_menu.classList.remove('mm-menu_opened');    
       remove_menu.setAttribute('area-hidden', 'true');    
        document.querySelector('.mm-wrapper').classList.remove( 'mm-wrapper_opened','mm-wrapper_opening','mm-wrapper_blocking','mm-wrapper_background');
      }
    </script> -->