<section class="bg-white border-top mt-auto">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-3 col-md-6">
                <a class="row text-reset border-left p-4 d-block d-flex" href="{{ route('terms') }}">
                    <div class="col-3 my-auto">
                        <i class="la la-file-text la-3x text_color mb-2"></i>
                    </div>
                    <div class="col-9">
                        <h4 class="h6 fw-700 text_color">{{ translate('Terms & conditions') }}</h4>
                        <h5 class="h5 fw-600 fs-15 opacity-90">Regardless any regions.
                            We deliver our products
                            world-Wide.</h5>
                    </div>

                </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a class="row text-reset border-left p-4 d-block d-flex" href="{{ route('returnpolicy') }}">
                    <div class="col-3 my-auto">
                        <i class="la la-mail-reply la-3x text_color mb-2"></i>
                    </div>
                    <div class="col-9">
                        <h4 class="h6 fw-700 text_color">{{ translate('Return Policy') }}</h4>
                        <h5 class="h5 fw-600 fs-15 opacity-90">Regardless any regions.
                            We deliver our products
                            world-Wide.</h5>
                    </div>

                </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a class="row text-reset border-left p-4 d-block d-flex " href="{{ route('supportpolicy') }}">
                    <div class="col-3 my-auto">
                        <i class="la la-support la-3x text_color mb-2"></i>
                    </div>
                    <div class="col-9">

                        <h4 class="h6 fw-700 text_color">{{ translate('Support Policy') }}</h4>
                        <h5 class="h5 fw-600 fs-15 opacity-90">Regardless any regions.
                            We deliver our products
                            world-Wide.</h5>

                    </div>

                </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a class="row text-reset border-left border-right p-4 d-block d-flex"
                    href="{{ route('privacypolicy') }}">
                    <div class="col-3 my-auto">
                        <i class="las la-exclamation-circle la-3x text_color mb-2"></i>
                    </div>
                    <div class="col-9">
                        <h4 class="h6 fw-700 text_color">{{ translate('Privacy Policy') }}</h4>
                        <h5 class="h5 fw-600 fs-15 opacity-90">Regardless any regions.
                            We deliver our products
                            world-Wide.</h5>

                    </div>


                </a>
            </div>
        </div>
    </div>
</section>

<section class="bg-footer-color py-5 text-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-xl-4 text-center text-md-left">
                <div class="mt-4">
                    <a href="{{ route('home') }}" class="d-block">
                        @if(get_setting('footer_logo') != null)
                        <img class="lazyload img-fluid" src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                            data-src="{{ uploaded_asset(get_setting('footer_logo')) }}" alt="{{ env('APP_NAME') }}"
                            height="44">
                        @else
                        <img class="lazyload" src="{{ static_asset('assets/img/placeholder-rect.jpg') }}"
                            data-src="{{ static_asset('assets/img/logo.png') }}" alt="{{ env('APP_NAME') }}"
                            height="44">
                        @endif
                    </a>
                    <div class="my-3 foot-company-description">
                        @php
                        echo get_setting('about_us_description');
                        @endphp
                    </div>
                    <div class="d-inline-block d-md-block">
                        <form class="form-inline" method="POST" action="{{ route('subscribers.store') }}">
                            @csrf
                            <div class="form-group mb-0">
                                <input type="email" class="form-control no-round"
                                    placeholder="{{ translate('Your Email Address') }}" name="email" required>
                            </div>
                            <button style="margin-left: 5px;" type="submit"
                                class="uppercase btn no-round btn_background_footer fw-700">
                                {{ translate('Subscribe') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 ml-xl-auto col-md-4 mr-0">
                <div class="text-center text-md-left mt-4">
                    <h4 class="fs-16 text-uppercase fw-600 border-bottom border-gray-900 pb-2 mb-4">
                        {{ translate('Contact Info') }}
                    </h4>
                    <ul class="list-unstyled">
                        <li class="mb-2">
                            <span class="fs-14 d-block opacity-100">{{ translate('Address') }}:</span>
                            <span class="fs-14 d-block opacity-100 hov-opacity-90 ">{{ get_setting('contact_address')
                                }}</span>
                        </li>
                        <li class="mb-2">
                            <span class="fs-14 d-block opacity-100">{{translate('Phone')}}:</span>
                            <span class="fs-14 d-block opacity-100 hov-opacity-90">{{ get_setting('contact_phone')
                                }}</span>
                        </li>
                        <li class="mb-2">
                            <span class="fs-14 d-block opacity-100">{{translate('Email')}}:</span>
                            <span class="fs-14 d-block opacity-100 hov-opacity-90">
                                <a href="mailto:{{ get_setting('contact_email') }}" class="text-reset">{{
                                    get_setting('contact_email') }}</a>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4">
                <div class="text-center text-md-left mt-4">
                    <h4 class="fs-16 text-uppercase fw-600 border-bottom border-gray-900 pb-2 mb-4">
                        {{ get_setting('widget_one') }}
                    </h4>
                    <ul class="list-unstyled">
                        @if ( get_setting('widget_one_labels') != null )
                        @foreach (json_decode( get_setting('widget_one_labels'), true) as $key => $value)
                        <li class="mb-2">
                            <a href="{{ json_decode( get_setting('widget_one_links'), true)[$key] }}"
                                class="opacity-100 hov-opacity-90 text-reset fs-14">
                                {{ $value }}
                            </a>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-4">
                <div class="text-center text-md-left mt-4">
                    <h4 class="fs-16  text-uppercase fw-600 border-bottom border-gray-900 pb-2 mb-4">
                        Analogue Mall
                    </h4>
                    <ul class="list-unstyled">

                        <li class="mb-2">
                            <a href="#" class="fs-14 opacity-100 hov-opacity-90 text-reset">
                                About Trade-In

                            </a>
                        </li>
                        <li class="mb-2">
                            <a href="#" class="fs-14 opacity-100 hov-opacity-90 text-reset">
                                Trade-In Cash


                            </a>
                        </li>
                        <li class="mb-2">
                            <a href="#" class="fs-14 opacity-100 hov-opacity-90 text-reset">
                                Trade-In Product


                            </a>
                        </li>
                        <li class="mb-2">
                            <a href="#" class="fs-14 opacity-100 hov-opacity-90 text-reset">
                                Analogue Pre-owned
                            </a>
                        </li>


                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-lg-2">
                <div class="text-center text-md-left mt-4">
                    <h4 class="fs-16 text-uppercase fw-600 border-bottom border-gray-900 pb-2 mb-4">
                        {{ translate('My Account') }}
                    </h4>
                    <ul class="list-unstyled">
                        @if (Auth::check())
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-reset" href="{{ route('logout') }}">
                                {{ translate('Logout') }}
                            </a>
                        </li>
                        @else
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-reset" href="{{ route('user.login') }}">
                                {{ translate('Login') }}
                            </a>
                        </li>
                        @endif
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-reset"
                                href="{{ route('purchase_history.index') }}">
                                {{ translate('Order History') }}
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-reset"
                                href="{{ route('wishlists.index') }}">
                                {{ translate('My Wishlist') }}
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-reset" href="{{ route('orders.track') }}">
                                {{ translate('Track Order') }}
                            </a>
                        </li>
                        @if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated)
                        <li class="mb-2">
                            <a class="fs-14 opacity-100 hov-opacity-90 text-light"
                                href="{{ route('affiliate.apply') }}">{{ translate('Be an affiliate partner')}}</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- FOOTER -->
<footer class="pt-3 pb-7 pb-xl-3 bg-footer-color border-top-footer text-light">
    <div class="container">
        <div class="row align-items-center ">

            <div class="col-md-6 col-lg-6 ">
                <ul class="list-inline my-3 my-md-0 social colored text-center text-md-left">
                    @if ( get_setting('facebook_link') != null )
                    <li class="list-inline-item">
                        <a href="{{ get_setting('facebook_link') }}" target="_blank" class="facebook"><i
                                class="lab la-facebook-f"></i></a>
                    </li>
                    @endif
                    @if ( get_setting('twitter_link') != null )
                    <li class="list-inline-item">
                        <a href="{{ get_setting('twitter_link') }}" target="_blank" class="twitter"><i
                                class="lab la-twitter"></i></a>
                    </li>
                    @endif
                    @if ( get_setting('instagram_link') != null )
                    <li class="list-inline-item">
                        <a href="{{ get_setting('instagram_link') }}" target="_blank" class="instagram"><i
                                class="lab la-instagram"></i></a>
                    </li>
                    @endif
                    @if ( get_setting('youtube_link') != null )
                    <li class="list-inline-item">
                        <a href="{{ get_setting('youtube_link') }}" target="_blank" class="youtube"><i
                                class="lab la-youtube"></i></a>
                    </li>
                    @endif
                    @if ( get_setting('linkedin_link') != null )
                    <li class="list-inline-item">
                        <a href="{{ get_setting('linkedin_link') }}" target="_blank" class="linkedin"><i
                                class="lab la-linkedin-in"></i></a>
                    </li>
                    @endif
                </ul>
            </div>
            {{-- <div class="col-md-4 col-lg-4">
                <div class="text-center text-md-right">
                    <ul class="list-inline mb-0">
                        @if ( get_setting('payment_method_images') != null )
                        @foreach (explode(',', get_setting('payment_method_images')) as $key => $value)
                        <li class="list-inline-item">
                            <img src="{{ uploaded_asset($value) }}" height="30">
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            --}}
            <div class="col-md-6 col-lg-6">
                <div class="text-center text-md-right">
                    @php
                    echo get_setting('frontend_copyright_text');
                    @endphp
                </div>
            </div>
        </div>
    </div>
</footer>


<div class="aiz-mobile-bottom-nav d-xl-none d-lg-none d-md-none fixed-bottom bg-white shadow-lg border-top">
    <div class="d-flex justify-content-around align-items-center">
        <a href="{{ route('home') }}"
            class="text-reset flex-grow-1 text-center py-3 border-right {{ areActiveRoutes(['home'],'bg-soft-primary')}}">
            <i class="las la-home la-2x"></i>
        </a>
        <a href="{{ route('customer.products') }}"
            class="text-reset flex-grow-1 text-center py-3 border-right {{ areActiveRoutes(['categories.all'],'bg-soft-primary')}}">
            <span class="d-inline-block position-relative px-2">
                <img src="{{static_asset('frontend/images/4.png')}}" title="Pre-owned" style="width: 20px;" alt="">
            </span>
        </a>
        <a href="{{ route('cart') }}"
            class="text-reset flex-grow-1 text-center py-3 border-right {{ areActiveRoutes(['cart'],'bg-soft-primary')}}">
            <span class="d-inline-block position-relative px-2">
                <i class="las la-shopping-cart la-2x"></i>
                @if(Session::has('cart'))
                <span class="badge badge-circle badge-primary position-absolute absolute-top-right"
                    id="cart_items_sidenav">{{ count(Session::get('cart'))}}</span>
                @else
                <span class="badge badge-circle badge-primary position-absolute absolute-top-right"
                    id="cart_items_sidenav">0</span>
                @endif
            </span>
        </a>
        @if (Auth::check())
        @if(isAdmin())
        <a href="{{ route('admin.dashboard') }}" class="text-reset flex-grow-1 text-center py-2">
            <span class="avatar avatar-sm d-block mx-auto">
                @if(Auth::user()->photo != null)
                <img src="{{ custom_asset(Auth::user()->avatar_original)}}">
                @else
                <img src="{{ static_asset('assets/img/avatar-place.png') }}">
                @endif
            </span>
        </a>
        @else
        <a href="javascript:void(0)" class="text-reset flex-grow-1 text-center py-2 mobile-side-nav-thumb"
            data-toggle="class-toggle" data-target=".aiz-mobile-side-nav">
            <span class="avatar avatar-sm d-block mx-auto">
                @if(Auth::user()->photo != null)
                <img src="{{ custom_asset(Auth::user()->avatar_original)}}">
                @else
                <img src="{{ static_asset('assets/img/avatar-place.png') }}">
                @endif
            </span>
        </a>
        @endif
        @else
        <a href="{{ route('user.login') }}" class="text-reset flex-grow-1 text-center py-2">
            <span class="avatar avatar-sm d-block mx-auto">
                <img src="{{ static_asset('assets/img/avatar-place.png') }}">
            </span>
        </a>
        @endif
    </div>
</div>
@if (Auth::check() && !isAdmin())
<div class="aiz-mobile-side-nav collapse-sidebar-wrap sidebar-xl d-xl-none z-1035">
    <div class="overlay dark c-pointer overlay-fixed" data-toggle="class-toggle" data-target=".aiz-mobile-side-nav"
        data-same=".mobile-side-nav-thumb"></div>
    <div class="collapse-sidebar bg-white">
        @include('frontend.inc.user_side_nav')
    </div>
</div>
@endif