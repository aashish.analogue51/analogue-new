@extends('frontend.layouts.app')

@section('meta_title'){{ $detailedProduct->meta_title }}@stop

@section('meta_description'){{ $detailedProduct->meta_description }}@stop

@section('meta_keywords'){{ $detailedProduct->tags }}@stop

@section('meta')
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $detailedProduct->meta_title }}">
<meta itemprop="description" content="{{ $detailedProduct->meta_description }}">
<meta itemprop="image" content="{{ uploaded_asset($detailedProduct->meta_img) }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:title" content="{{ $detailedProduct->meta_title }}">
<meta name="twitter:description" content="{{ $detailedProduct->meta_description }}">
<meta name="twitter:creator" content="@author_handle">
<meta name="twitter:image" content="{{ uploaded_asset($detailedProduct->meta_img) }}">
<meta name="twitter:data1" content="{{ single_price($detailedProduct->unit_price) }}">
<meta name="twitter:label1" content="Price">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $detailedProduct->meta_title }}" />
<meta property="og:type" content="og:product" />
<meta property="og:url" content="{{ route('product', $detailedProduct->slug) }}" />
<meta property="og:image" content="{{ uploaded_asset($detailedProduct->meta_img) }}" />
<meta property="og:description" content="{{ $detailedProduct->meta_description }}" />
<meta property="og:site_name" content="{{ get_setting('meta_title') }}" />
<meta property="og:price:amount" content="{{ single_price($detailedProduct->unit_price) }}" />
<meta property="product:price:currency"
content="{{ \App\Currency::findOrFail(\App\BusinessSetting::where('type', 'system_default_currency')->first()->value)->code }}" />
<meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
@endsection

@section('content')
<section class="pt-3 bg-white product-detail-page">
    <div class="container">
        <div class="bg-white shadow-sm rounded p-3">
            <div class="row">
                <div class="col-xl-5 col-lg-6 mb-4">
                    <div class="sticky-top z-3 row gutters-10">
                        @php
                        $photos = explode(',',$detailedProduct->photos);
                        @endphp
                        <div class="col order-1 order-md-2">
                            <div class="aiz-carousel product-gallery" data-nav-for='.product-gallery-thumb' data-fade='true' data-auto-height='true'>
                                @foreach ($detailedProduct->stocks as $key => $stock)
                                @if ($stock->image != null)
                                <div class="carousel-box img-zoom rounded">
                                    <img
                                    class="img-fluid lazyload"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($stock->image) }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                    >
                                </div>
                                @endif
                                @endforeach

                                @foreach ($photos as $key => $photo)
                                @if($photo != null)
                                <div class="carousel-box img-zoom rounded">
                                    <img
                                    class="img-fluid lazyload"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($photo) }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                    >
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12 col-md-auto w-md-80px order-2 order-md-1 mt-3 mt-md-0">
                            <div class="aiz-carousel product-gallery-thumb" data-items='5' data-nav-for='.product-gallery' data-vertical='true' data-vertical-sm='false' data-focus-select='true' data-arrows='true'>
                                @foreach ($detailedProduct->stocks as $key => $stock)
                                @if ($stock->image != null)
                                <div class="carousel-box c-pointer border p-1 rounded" data-variation="{{ $stock->variant }}">
                                    <img
                                    class="lazyload mw-100 size-50px mx-auto"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($stock->image) }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                    >
                                </div>
                                @endif
                                @endforeach
                                @foreach ($photos as $key => $photo)
                                @if($photo != null)
                                <div class="carousel-box c-pointer border p-1 rounded">
                                    <img
                                    class="lazyload mw-100 size-50px mx-auto"
                                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                    data-src="{{ uploaded_asset($photo) }}"
                                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                    >
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-7 col-lg-6">
                    <div class="text-left">
                        <h1 class="mb-2 fs-22 fw-700 text_color">
                            {{ $detailedProduct->getTranslation('name') }}
                        </h1>

                        <div class="row pt-2">
                            <div class="col-6">
                                @php
                                $total = 0;
                                $total += $detailedProduct->reviews->count();
                                @endphp
                                <span class="rating">
                                    {{ renderStarRating($detailedProduct->rating) }}
                                </span>
                                <span class="ml-1 opacity-50">({{ $total }} {{ translate('reviews')}})</span>
                            </div>
                            <div class="col-6 text-right">
                                @php
                                $qty = 0;
                                if($detailedProduct->variant_product){
                                foreach ($detailedProduct->stocks as $key => $stock) {
                                $qty += $stock->qty;
                            }
                        }
                        else{
                        $qty = $detailedProduct->current_stock;
                    }
                    @endphp
                    @if ($qty > 0)

                    @else
                    <span class="badge badge-md badge-inline badge-pill badge-danger">{{ translate('Out of
                    stock')}}</span>
                    @endif
                </div>
            </div>


            <div class="row align-items-center my-3">

                @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                <div class="col-auto">
                    <button class="btn btn-sm btn-soft-primary" onclick="show_chat_modal()">{{
                    translate('Message Us')}}</button>
                </div>
                @endif

                @if ($detailedProduct->brand != null)
                <div class="col-auto">
                    <img src="{{ uploaded_asset($detailedProduct->brand->logo) }}"
                    alt="{{ $detailedProduct->brand->getTranslation('name') }}" height="30">
                </div>
                @endif
            </div>

                        <!-- 
                            <div class="row pt-2">
                            <div class="col-auto short-description">
                                    <?php echo $detailedProduct->getTranslation('description'); ?> 
                            </div>
                        </div> -->
                        


                        <div class="d-table width-100">
                            <div class="d-table-cell">
                                <!-- Add to wishlist button -->
                                <div class="display_text">
                                    <div type="button" class="btn pl-0 btn-link fw-600 display_text color_gray"
                                    onclick="addToWishList({{ $detailedProduct->id }})">
                                    <div>
                                        <i class="la la-heart-o la-2x opacity-80 text_color pr-1"></i>
                                    </div>
                                    <div class="fs-16 text_color">
                                        {{ translate('Wishlist')}}
                                    </div>

                                </div>
                                <!-- Add to compare button -->
                                <div type="button" class="btn btn-link btn-icon-left fw-600 display_text color_gray"
                                onclick="addToCompare({{ $detailedProduct->id }})">
                                <div>
                                    <i class="la la-refresh la-2x opacity-80 text_color pr-1"></i>
                                </div>
                                <div class="fs-16 text_color">
                                    {{ translate('Compare')}}
                                </div>

                            </div>
                        </div>
                        @if(Auth::check() && \App\Addon::where('unique_identifier', 'affiliate_system')->first()
                        != null && \App\Addon::where('unique_identifier',
                        'affiliate_system')->first()->activated && (\App\AffiliateOption::where('type',
                        'product_sharing')->first()->status || \App\AffiliateOption::where('type',
                        'category_wise_affiliate')->first()->status) && Auth::user()->affiliate_user != null &&
                        Auth::user()->affiliate_user->status)
                        @php
                        if(Auth::check()){
                        if(Auth::user()->referral_code == null){
                        Auth::user()->referral_code = substr(Auth::user()->id.Str::random(10), 0, 10);
                        Auth::user()->save();
                    }
                    $referral_code = Auth::user()->referral_code;
                    $referral_code_url =
                    URL::to('/product').'/'.$detailedProduct->slug."?product_referral_code=$referral_code";
                }
                @endphp
                <div class="form-group">
                    <textarea id="referral_code_url" class="form-control" readonly type="text"
                    style="display:none">{{$referral_code_url}}</textarea>
                </div>
                <button type=button id="ref-cpurl-btn" class="btn btn-sm btn-secondary"
                data-attrcpy="{{ translate('Copied')}}"
                onclick="CopyToClipboard('referral_code_url')">{{ translate('Copy the Promote
            Link')}}</button>
            @endif
        </div>
    </div>

    <hr>


    @if(home_price($detailedProduct->id) != home_discounted_price($detailedProduct->id))

    <div class="row no-gutters mt-2">

        <div class="col-sm-10">
            <div class="fs-18 opacity-60">
                <del>
                    {{ home_price($detailedProduct->id) }}
                    @if($detailedProduct->unit != null)
                    <span>/{{ $detailedProduct->getTranslation('unit') }}</span>
                    @endif
                </del>
            </div>
        </div>
    </div>

    <div class="row no-gutters my-2">
        <div class="col-sm-2">
            <div class="opacity-80">{{ translate('Price')}}:</div>
        </div>
        <div class="col-sm-10">
            <div class="">
                <strong class="h4 fw-600 text_color">
                    {{ home_discounted_price($detailedProduct->id) }}
                </strong>
                @if($detailedProduct->unit != null)
                <span class="opacity-80">/{{ $detailedProduct->getTranslation('unit') }}</span>
                @endif
            </div>
        </div>
    </div>
    @else
    <div class="row no-gutters mt-2">

        <div class="col-sm-10">
            <div class="">
                <strong class="h4 fw-700 text_color">
                    {{ home_discounted_price($detailedProduct->id) }}
                </strong>
                @if($detailedProduct->unit != null)
                <span class="opacity-80">/{{ $detailedProduct->getTranslation('unit') }}</span>
                @endif
            </div>
        </div>
    </div>
    @endif

    @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null &&
    \App\Addon::where('unique_identifier', 'club_point')->first()->activated &&
    $detailedProduct->earn_point > 0)
    <div class="row no-gutters mt-4">
        <div class="col-sm-2">
            <div class="opacity-50 my-2">{{ translate('Club Point') }}:</div>
        </div>
        <div class="col-sm-10">
            <div class="d-inline-block rounded px-2 bg-soft-primary border-soft-primary border">
                <span class="strong-700">{{ $detailedProduct->earn_point }}</span>
            </div>
        </div>
    </div>
    @endif

    <hr>

    <form id="option-choice-form">
        @csrf
        <input type="hidden" name="id" value="{{ $detailedProduct->id }}">

        @if ($detailedProduct->choice_options != null)
        @foreach (json_decode($detailedProduct->choice_options) as $key => $choice)

        <div class="row no-gutters">
            <div class="col-sm-2">
                <div class="opacity-80 my-2">{{
                    $attributes->find($choice->attribute_id)->getTranslation('name') }}:</div>
                </div>
                <div class="col-sm-10">
                    <div class="aiz-radio-inline">
                        @foreach ($choice->values as $key => $value)
                        <label class="aiz-megabox pl-0 mr-2">
                            <input type="radio" name="attribute_id_{{ $choice->attribute_id }}"
                            value="{{ $value }}" @if($key==0) checked @endif>
                            <span
                            class="aiz-megabox-elem rounded d-flex align-items-center justify-content-center py-2 px-3 mb-2">
                            {{ $value }}
                        </span>
                    </label>
                    @endforeach
                </div>
            </div>
        </div>

        @endforeach
        @endif

        @if (count(json_decode($detailedProduct->colors)) > 0)
        <div class="row no-gutters">
            <div class="col-sm-2">
                <div class="opacity-80 my-2">{{ translate('Color')}}:</div>
            </div>
            <div class="col-sm-10">
                <div class="aiz-radio-inline">
                    @foreach (json_decode($detailedProduct->colors) as $key => $color)
                    <label class="aiz-megabox pl-0 mr-2" data-toggle="tooltip" data-title="{{ \App\Color::where('code', $color)->first()->name }}">
                        <input
                        type="radio"
                        name="color"
                        value="{{ \App\Color::where('code', $color)->first()->name }}"
                        @if($key == 0) checked @endif
                        >
                        <span class="aiz-megabox-elem rounded d-flex align-items-center justify-content-center p-1 mb-2">
                            <span class="size-30px d-inline-block rounded" style="background: {{ $color }};"></span>
                        </span>
                    </label>
                    @endforeach
                </div>
            </div>
        </div>

        <hr>
        @endif

        <!-- Quantity + Add to cart -->
        <div class="row no-gutters">
            <div class="col-sm-2">
                <div class="opacity-80 my-2">{{ translate('Quantity')}}:</div>
            </div>
            <div class="col-sm-4">
                <div class="product-quantity d-flex align-items-center">
                    <div class="row no-gutters align-items-center aiz-plus-minus mr-3"
                    style="width: 130px;">
                    <button class="btn col-auto btn-icon btn-sm btn-circle btn-light"
                    type="button" data-type="minus" data-field="quantity" disabled="">
                    <i class="las la-minus"></i>
                </button>
                <input type="text" name="quantity"
                class="col border-0 text-center flex-grow-1 fs-16 input-number"
                placeholder="1" value="{{ $detailedProduct->min_qty }}"
                min="{{ $detailedProduct->min_qty }}" max="10" readonly>
                <button class="btn  col-auto btn-icon btn-sm btn-circle btn-light"
                type="button" data-type="plus" data-field="quantity">
                <i class="las la-plus"></i>
            </button>
        </div>

    </div>
</div>


</div>

<hr>

<div class="row no-gutters pb-3 d-none" id="chosen_price_div">
    <div class="col-sm-2">
        <div class="opacity-80 my-2">{{ translate('Total Price')}}:</div>
    </div>
    <div class="col-sm-10">
        <div class="product-price">
            <strong id="chosen_price" class="h4 fw-600 text-primary">

            </strong>
        </div>
    </div>
</div>


</form>







@php
$refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
$refund_sticker = \App\BusinessSetting::where('type', 'refund_sticker')->first();
@endphp
@if ($refund_request_addon != null && $refund_request_addon->activated == 1 &&
$detailedProduct->refundable)
<div class="row no-gutters mt-4">
    <div class="col-sm-2">
        <div class="opacity-50 my-2">{{ translate('Refund')}}:</div>
    </div>
    <div class="col-sm-10">
        <a href="{{ route('returnpolicy') }}" target="_blank">
            @if ($refund_sticker != null && $refund_sticker->value != null)
            <img src="{{ uploaded_asset($refund_sticker->value) }}" height="36">
            @else
            <img src="{{ static_asset('assets/img/refund-sticker.jpg') }}" height="36">
            @endif
        </a>
        <a href="{{ route('returnpolicy') }}" class="ml-2" target="_blank">View Policy</a>
    </div>
</div>
@endif
<div class="row no-gutters mt-2">
    <div class="col-sm-2">
        <div class="opacity-80 my-2">{{ translate('Share')}}:</div>
    </div>
    <div class="col-sm-8">
        <div class="aiz-share"></div>
    </div>
    <div class="col-sm-12 trade_in trade_in_main_box pt-4 pt-md-0 mt-4 justify-content-start">
        <div class="">
            <p class="fs-16 fw-600 mb-2 mb-md-0">Do you want to do trade in with this product?
            </p>
            <div class="option">
                <a class="text_color fs-14 d-block  pb-3">See how trade-in works</a>
                <a class="btn border btn_outline px-5">NO</a>
                <a class="btn border px-5 btn_outline btn_yes">YES</a>
            </div>

        </div>
    </div>
    <div class="col-sm-12 trade_in trade-in-popupbox pt-4 pt-md-0 justify-content-start">
        <h3>Which model do you have?</h3>
        <p>On your iPhone, go to Settings > Your Name. Scroll down to see the model. On other
            smartphones, go to Settings > About phone.</p>

            <div class="form-element">
                <label aria-hidden="true" class="form-label"
                id="fs-tradeUpInline.product.moduleData.dimensions.0-model-label">Choose Your
                Model
            </label><br>
            <select id="mobile-modal" name="m">
                <option value="">
                Select</option>
                <option value="model_9004">
                iPhone 11 Pro Max - Up to $500</option>
                <option value="model_9005">
                iPhone 11 Pro - Up to $460</option>
                <option value="model_9006">
                iPhone 11 - Up to $360</option>
                <option value="model_7876">
                iPhone XS Max - Up to $340</option>
                <option value="model_7877">
                iPhone XS - Up to $270</option>
                <option value="model_7874">
                iPhone XR - Up to $220</option>
                <option value="model_6905">
                iPhone X - Up to $220</option>
                <option value="model_5134">
                iPhone 8 Plus - Up to $180</option>
                <option value="model_5133">
                iPhone 8 - Up to $120</option>
                <option value="model_332" selected="selected">
                iPhone 7 Plus - Up to $130</option>
                <option value="model_729">
                iPhone 7 - Up to $90</option>
                <option value="model_268">
                iPhone 6s Plus - Up to $75</option>
                <option value="model_254">
                iPhone 6s - Up to $50</option>
                <option value="model_549">
                iPhone 6 Plus - Up to $50</option>
                <option value="model_2544">
                iPhone 6 - Up to $35</option>
                <option value="model_10139">
                iPhone SE (2nd generation) - Up to $200</option>
                <option value="model_9204">
                iPhone SE (1st generation) - Up to $25</option>
                <option value="model_8678">
                Galaxy Note10 - Up to $270</option>
                <option value="model_8679">
                Galaxy Note9 - Up to $145</option>
                <option value="model_7720">
                Galaxy Note8 - Up to $80</option>
                <option value="model_10053">
                Galaxy S20+ - Up to $315</option>
                <option value="model_10050">
                Galaxy S20 - Up to $240</option>
                <option value="model_7940">
                Galaxy S10+ - Up to $220</option>
                <option value="model_7939">
                Galaxy S10 - Up to $185</option>
                <option value="model_7941">
                Galaxy S10e - Up to $145</option>
                <option value="model_7068">
                Galaxy S9+ - Up to $110</option>
                <option value="model_7075">
                Galaxy S9 - Up to $90</option>
                <option value="model_7079">
                Galaxy S8+ - Up to $70</option>
                <option value="model_7077">
                Galaxy S8 - Up to $55</option>
                <option value="model_10131">
                Google Pixel 4 XL - Up to $220</option>
                <option value="model_10130">
                Google Pixel 4 - Up to $185</option>
                <option value="model_7895">
                Google Pixel 3 XL - Up to $90</option>
                <option value="model_7893">
                Google Pixel 3 - Up to $65</option>
                <option value="model_7897">
                Google Pixel 3a - Up to $50</option>
                <option value="model_4180">
                Other Models - Recycling</option>
            </select>
            <div class="condition_box">
                <h3>Is the iPhone in good condition?</h3>
                <p>Answer yes if all of the following apply:</p>
                <ul>
                    <li>It turns on and functions normally</li>
                    <li>All the buttons work</li>
                    <li>The cameras work</li>
                    <li>The body is free of dents and scratches</li>
                    <li>The touchscreen and back glass are undamaged</li>
                </ul>
                <div class="row">
                    <div class="col-6">
                        <a href="#" class="yes_no_btn"><span>Yes</span></a>
                    </div>
                    <div class="col-6">
                        <a class="yes_no_btn no-btn-condition"><span>No</span></a>
                    </div>
                </div>
                <div class="sub-condition">
                    <h3>Is the body of the iPhone in good shape?</h3>
                    <p>Answer yes if all of the following apply:</p>
                    <ul>
                        <li>It’s free of major cracks, chips, and scratches</li>
                        <li>If there’s glass on the back, it’s not shattered</li>
                    </ul>
                    <div class="row">
                        <div class="col-6">
                            <a href="#" class="yes_no_btn"><span>Yes</span></a>
                        </div>
                        <div class="col-6">
                            <a href="#" class="yes_no_btn"><span>No</span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-12 trade-in-emi p-top10">
        <div class="">
            <p class="fs-16 fw-600 m-bottom10 mb-md-0">Choose A Payment Option.</p>
            <div class="option">
                <a class="btn border btn_outline px-5">Pay in Full</a>
                <a class="btn border px-5 btn_outline pay-monthly">Pay Monthly</a>
            </div>

        </div>
        <div class="emi_innerbox  m-top10">
            <label class="check_innerbox">
                <input type="radio" checked="checked" name="emi">
                <div class="inner_box">
                    <h3>Apple Card Monthly Installments</h3>
                    <p><strong>$20.79/mo.for 24 mo.*</strong></p>
                    <p><small><i>Includes $200.00 instant trade‑in credit*</i></small></p>
                    <hr>
                    <ul>
                        <li>Pay for your iPhone with low monthly payments</li>
                        <li>Financed at 0% APR</li>
                        <li>Get 3% cash back on the full cost of the iPhone, right away</li>
                    </ul>
                </div>
            </label>
            <a href="" style="text-transform: initial;">Learn how to pay monthly at 0% APR when
            you choose Apple Card Monthly Installments</a>
            <label class="check_innerbox">
                <input type="radio" checked="" name="emi">
                <div class="inner_box ">
                    <h3>Apple Card Monthly Installments</h3>
                    <p><strong>$20.79/mo.for 24 mo.*</strong></p>
                    <p><small><i>Includes $200.00 instant trade‑in credit*</i></small></p>
                    <hr>
                    <ul>
                        <li>Pay for your iPhone with low monthly payments</li>
                        <li>Financed at 0% APR</li>
                        <li>Get 3% cash back on the full cost of the iPhone, right away</li>
                    </ul>
                </div>
            </label>
        </div>

    </div>
</div>
<hr>
<div class="row">
    <div class="col-12 col-md-12 d-flex pt-4 p-all pt-md-0">
        @if ($qty > 0)
        <button type="button" class="btn btn-sec  mr-2 add-to-cart fw-600"
        onclick="addToCart()">
        <i class="las la-shopping-bag"></i>
        {{ translate('Add to cart')}}
    </button>
    <button type="button" class="btn btn_background btn-primary buy-now fw-600"
    onclick="buyNow()">
    <i class="la la-shopping-cart"></i> {{ translate('Buy Now')}}
</button>
@else
<button type="button" class="btn btn-secondary fw-600" disabled>
    <i class="la la-cart-arrow-down"></i> {{ translate('Out of Stock')}}
</button>
@endif
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="bg-white product-detail-page-description-section">
    <div class="container">
        <div class="row gutters-10">
            <div class="col-xl-3 order-1 order-xl-0">


                {{-- Flash Deal --}}

                @if($flash_deal != null && strtotime(date('Y-m-d H:i:s')) >= $flash_deal->start_date &&
                strtotime(date('Y-m-d H:i:s')) <= $flash_deal->end_date)
                <section class="mb-4 white-background">
                    <div class="container">
                        <div class="py-4  py-md-3 bg-white rounded">

                            <div class="d-block mb-3 align-items-baseline">
                                <h3 class="h6 fw-700 mb-2">
                                    <span class="border-width-2 pb-3 ">{{ translate('Flash Deals')
                                    }}</span>
                                </h3>
                                <div><div class="aiz-count-down ml-auto align-items-center" data-date="{{ date('Y/m/d H:i:s', $flash_deal->end_date) }}"></div></div>
                            </div>

                            <div class="aiz-carousel gutters-10 half-outside-arrow" data-items="1" data-xl-items="1"
                            data-lg-items="" data-md-items="3" data-sm-items="2" data-xs-items="2"
                            data-arrows='true' data-infinite='true'>
                            @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
                            @if ($flash_deal_product->product != null && $flash_deal_product->product->published
                            != 0)
                            <div class="carousel-box">
                                <div class="aiz-card-box rounded my-2 has-transition">
                                    <div class="position-relative">
                                        <a href="{{ route('product', $flash_deal_product->product->slug) }}"
                                            class="d-block">
                                            <img class="lazyload img-fluid"
                                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                            data-src="{{ uploaded_asset($flash_deal_product->product->thumbnail_img) }}"
                                            alt="{{  $flash_deal_product->product->getTranslation('name')  }}"
                                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                                        </a>
                                        <div class="absolute-top-right aiz-p-hov-icon hover_icon">
                                            <a href="javascript:void(0)"
                                            onclick="addToWishList({{ $flash_deal_product->product->id }})"
                                            data-toggle="tooltip"
                                            data-title="{{ translate('Add to wishlist') }}"
                                            data-placement="left">
                                            <i class="la la-heart-o"></i>
                                        </a>
                                        <a href="javascript:void(0)"
                                        onclick="addToCompare({{ $flash_deal_product->product->id }})"
                                        data-toggle="tooltip"
                                        data-title="{{ translate('Add to compare') }}"
                                        data-placement="left">
                                        <i class="las la-sync"></i>
                                    </a>
                                    <a href="javascript:void(0)"
                                    onclick="showAddToCartModal({{ $flash_deal_product->product->id }})"
                                    data-toggle="tooltip"
                                    data-title="{{ translate('Add to cart') }}"
                                    data-placement="left">
                                    <i class="las la-shopping-cart"></i>
                                </a>
                            </div>
                        </div>

                        <div class="p-md-3 p-2">
                            <div class="star-rating star-rating-sm mt-1">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"
                                    style="padding-right: 0;">{{
                                    renderStarRating($flash_deal_product->product->rating) }}
                                </div>
                                <div class="col" align="right"><span class="rating-number">(0
                                Reviews)</span></div>
                            </div>

                        </div>
                        <div class="sub-cat">{{ __($flash_deal_product->product->category->name)
                        }}</div>
                        <h2 class="product-title p-0">
                            <a href="{{ route('product', $flash_deal_product->product->slug) }}"
                                class=" text-truncate">{{ __($flash_deal_product->product->name)
                            }}</a>
                        </h2>
                        <div class="price-box">
                            @if(home_base_price($flash_deal_product->product->id) !=
                            home_discounted_base_price($flash_deal_product->product->id))
                            <del class="old-product-price strong-400">{{
                                home_base_price($flash_deal_product->product->id) }}</del>
                                @endif
                                <span class="product-price strong-600">{{
                                    home_discounted_base_price($flash_deal_product->product->id)
                                }}</span>
                            </div>
                            <div class="Product-cart-2-footer-btn d-flex justify-content-start justify-content-md-center">

                                <div class="row">
                                    <div class="col-12 text-center">
                                     <button class="add-to-cart p-all" title="Add to Cart" onclick="showAddToCartModal({{ $flash_deal_product->product->id }})">
                                        <i class="las la-cart-arrow-down"></i> Add To Cart
                                    </button>
                                    <button class="add-to-cart p-all btn" style="position: relative; top:-2px;" title="Add to wishlist" onclick="addToCompare({{ $flash_deal_product->product->id }})">
                                     <i class="la la-heart-o"></i>
                                 </button>
                             </div>
                         </div>
                     </div>


                 </div>
             </div>
         </div>
         @endif
         @endforeach
     </div>
 </div>
</div>
</section>
@endif
</div>
<div class="@if($flash_deal != null) col-xl-9 @else col-xl-12 @endif order-0 order-xl-1">
    <div class="bg-white mb-3 rounded">
        <div class="nav border-bottom aiz-nav-tabs">
            <a href="#tab_default_1" data-toggle="tab" class="p-3 fs-16 fw-700 text-reset active show">{{
            translate('Description')}}</a>
            @if($detailedProduct->video_link != null)
            <a href="#tab_default_2" data-toggle="tab" class="p-3 fs-16 fw-700 text-reset">{{
            translate('Video')}}</a>
            @endif
            @if($detailedProduct->pdf != null)
            <a href="#tab_default_3" data-toggle="tab" class="p-3 fs-16 fw-700 text-reset">{{
            translate('Downloads')}}</a>
            @endif
            <a href="#tab_default_4" data-toggle="tab" class="p-3 fs-16 fw-700 text-reset">{{
            translate('Reviews')}}</a>
        </div>

        <div class="tab-content pt-0">
            <div class="tab-pane tab-pane-first  fade active show" id="tab_default_1">
                <div class="p-4">
                    <div class="mw-100 overflow-hidden text-left">
                        <?php echo $detailedProduct->getTranslation('description'); ?>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab_default_2">
                <div class="p-4">
                    <div class="embed-responsive embed-responsive-16by9">
                        @if ($detailedProduct->video_provider == 'youtube' && isset(explode('=',
                        $detailedProduct->video_link)[1]))
                        <iframe class="embed-responsive-item"
                        src="https://www.youtube.com/embed/{{ explode('=', $detailedProduct->video_link)[1] }}"></iframe>
                        @elseif ($detailedProduct->video_provider == 'dailymotion' &&
                        isset(explode('video/', $detailedProduct->video_link)[1]))
                        <iframe class="embed-responsive-item"
                        src="https://www.dailymotion.com/embed/video/{{ explode('video/', $detailedProduct->video_link)[1] }}"></iframe>
                        @elseif ($detailedProduct->video_provider == 'vimeo' && isset(explode('vimeo.com/',
                        $detailedProduct->video_link)[1]))
                        <iframe
                        src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $detailedProduct->video_link)[1] }}"
                        width="500" height="281" frameborder="0" webkitallowfullscreen
                        mozallowfullscreen allowfullscreen></iframe>
                        @endif
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab_default_3">
                <div class="p-4 text-center ">
                    <a href="{{ uploaded_asset($detailedProduct->pdf) }}" class="btn btn-primary">{{
                    translate('Download') }}</a>
                </div>
            </div>
            <div class="tab-pane fade" id="tab_default_4">
                <div class="p-4">
                    <ul class="list-group list-group-flush">
                        @foreach ($detailedProduct->reviews as $key => $review)
                        @if($review->user != null)
                        <li class="media list-group-item d-flex">
                            <span class="avatar avatar-md mr-3">
                                <img class="lazyload" src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                                @if($review->user->avatar_original !=null)
                                data-src="{{ uploaded_asset($review->user->avatar_original) }}"
                                @else
                                data-src="{{ static_asset('assets/img/placeholder.jpg') }}"
                                @endif
                                >
                            </span>
                            <div class="media-body text-left">
                                <div class="d-flex justify-content-between">
                                    <h3 class="fs-15 fw-700 mb-0">{{ $review->user->name }}</h3>
                                    <span class="rating rating-sm">
                                        @for ($i=0; $i < $review->rating; $i++)
                                        <i class="las la-star active"></i>
                                        @endfor
                                        @for ($i=0; $i < 5-$review->rating; $i++)
                                        <i class="las la-star"></i>
                                        @endfor
                                    </span>
                                </div>
                                <div class="opacity-80 mb-2">{{ date('d-m-Y',
                                    strtotime($review->created_at)) }}</div>
                                    <p class="comment-text">
                                        {{ $review->comment }}
                                    </p>
                                </div>
                            </li>
                            @endif
                            @endforeach
                        </ul>

                        @if(count($detailedProduct->reviews) <= 0) <div class="text-center fs-18 opacity-80">
                            {{ translate('There have been no reviews for this product yet.') }}
                        </div>
                        @endif

                        @if(Auth::check())
                        @php
                        $commentable = false;
                        @endphp
                        @foreach ($detailedProduct->orderDetails as $key => $orderDetail)
                        @if($orderDetail->order != null && $orderDetail->order->user_id == Auth::user()->id &&
                        $orderDetail->delivery_status == 'delivered' && $reviews->where('user_id',
                        Auth::user()->id)->where('product_id', $detailedProduct->id)->first() == null)
                        @php
                        $commentable = true;
                        @endphp
                        @endif
                        @endforeach
                        @if ($commentable)
                        <div class="pt-4">
                            <div class="border-bottom mb-4">
                                <h3 class="fs-17 fw-600">
                                    {{ translate('Write a review')}}
                                </h3>
                            </div>
                            <form class="form-default" role="form" action="{{ route('reviews.store') }}"
                            method="POST">
                            @csrf
                            <input type="hidden" name="product_id" value="{{ $detailedProduct->id }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="text-uppercase c-gray-light">{{ translate('Your
                                        name')}}</label>
                                        <input type="text" name="name" value="{{ Auth::user()->name }}"
                                        class="form-control" disabled required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="text-uppercase c-gray-light">{{
                                        translate('Email')}}</label>
                                        <input type="text" name="email" value="{{ Auth::user()->email }}"
                                        class="form-control" required disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="opacity-60">{{ translate('Rating')}}</label>
                                <div class="rating rating-input">
                                    <label>
                                        <input type="radio" name="rating" value="1">
                                        <i class="las la-star"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="rating" value="2">
                                        <i class="las la-star"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="rating" value="3">
                                        <i class="las la-star"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="rating" value="4">
                                        <i class="las la-star"></i>
                                    </label>
                                    <label>
                                        <input type="radio" name="rating" value="5">
                                        <i class="las la-star"></i>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="opacity-60">{{ translate('Comment')}}</label>
                                <textarea class="form-control" rows="4" name="comment"
                                placeholder="{{ translate('Your review')}}" required></textarea>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary mt-3">
                                    {{ translate('Submit review')}}
                                </button>
                            </div>
                        </form>
                    </div>
                    @endif
                    @endif
                </div>
            </div>

        </div>
    </div>

</div>
</div>
</div>
<div class="container">
    <div class="bg-white rounded shadow-sm">
        <div class="border-bottom bg-related p-3">
            <h3 class="fs-16 fw-700 mb-0">
                <span class="mr-4">{{ translate('Related products')}}</span>
            </h3>
        </div>
        <div class="p-3">
            <div class="aiz-carousel gutters-5 half-outside-arrow" data-items="5" data-xl-items="4"
            data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="1" data-arrows='true'
            data-infinite='true'>
            @foreach ($relatedProducts->where('category_id', $detailedProduct->category_id)->where('id', '!=',
            $detailedProduct->id) as $key => $related_product)
            @if($key <= 9) <div class="carousel-box">
                <div class="aiz-card-box border rounded my-2 has-transition">
                    <div class="">
                        <a href="{{ route('product', $related_product->slug) }}" class="d-block">
                            <img class="img-fit lazyload mx-auto h-140px h-md-210px"
                            src="{{ static_asset('assets/img/placeholder.jpg') }}"
                            data-src="{{ uploaded_asset($related_product->thumbnail_img) }}"
                            alt="{{ $related_product->getTranslation('name') }}"
                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';">
                        </a>
                    </div>
                    <div class="p-md-3 p-2 text-left">
                        <div class="fs-15">
                            @if(home_base_price($related_product->id) !=
                            home_discounted_base_price($related_product->id))
                            <del class="fw-600 opacity-50 mr-1">{{ home_base_price($related_product->id)
                            }}</del>
                            @endif
                            <span class="fw-700 text-primary">{{
                                home_discounted_base_price($related_product->id) }}</span>
                            </div>
                            <div class="rating rating-sm mt-1">
                                {{ renderStarRating($related_product->rating) }}
                            </div>
                            <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
                                <a href="{{ route('product', $related_product->slug) }}"
                                    class="d-block text-reset">{{ $related_product->getTranslation('name') }}</a>
                                </h3>
                                @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null &&
                                \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
                                <div class="rounded px-2 mt-2 bg-soft-primary border-soft-primary border">
                                    {{ translate('Club Point') }}:
                                    <span class="fw-700 float-right">{{ $related_product->earn_point }}</span>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>

    </div>

</section>

@endsection

@section('modal')
<div class="modal fade" id="chat_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
    <div class="modal-content position-relative">
        <div class="modal-header">
            <h5 class="modal-title fw-600 h5">{{ translate('Any query about this product')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="" action="{{ route('conversations.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="product_id" value="{{ $detailedProduct->id }}">
            <div class="modal-body gry-bg px-3 pt-3">
                <div class="form-group">
                    <input type="text" class="form-control mb-3" name="title" value="{{ $detailedProduct->name }}"
                    placeholder="{{ translate('Product Name') }}" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="8" name="message" required
                    placeholder="{{ translate('Your Message') }}">{{ route('product', $detailedProduct->slug) }}</textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary fw-600" data-dismiss="modal">{{
                translate('Cancel')}}</button>
                <button type="submit" class="btn btn-primary fw-600">{{ translate('Send')}}</button>
            </div>
        </form>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-dialog-zoom  modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title fw-600 text_color">{{ translate('Please Login First')}}</h6>
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="p-3">
                <form class="form-default" role="form" action="{{ route('cart.login.submit') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                        <input type="text"
                        class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="{{ translate('Email Or Phone')}}" name="email"
                        id="email">
                        @else
                        <input type="email"
                        class="form-control h-auto form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                        @endif
                        @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null &&
                        \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                        <span class="opacity-60">{{ translate('Use country code before number') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control h-auto form-control-lg"
                        placeholder="{{ translate('Password')}}">
                    </div>

                    <div class="row mb-2">
                        <div class="col-6">
                            <label class="aiz-checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class=opacity-60>{{ translate('Remember Me') }}</span>
                                <span class="aiz-square-check"></span>
                            </label>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{ route('password.request') }}" class="text-reset opacity-60 fs-14">{{
                            translate('Forgot password?')}}</a>
                        </div>
                    </div>

                    <div class="mb-5">
                        <button type="submit" class="btn btn-primary btn-block fw-600">{{ translate('Login')
                        }}</button>
                    </div>
                </form>

                <div class="text-center mb-3">
                    <p class="text-muted mb-0">{{ translate('Dont have an account?')}}</p>
                    <a href="{{ route('user.registration') }}">{{ translate('Register Now')}}</a>
                </div>
                @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 ||
                \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 ||
                \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                <div class="separator mb-3">
                    <span class="bg-white px-3 opacity-60">{{ translate('Or Login With')}}</span>
                </div>
                <ul class="list-inline social colored text-center mb-5">
                    @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="facebook">
                            <i class="lab la-facebook-f"></i>
                        </a>
                    </li>
                    @endif
                    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="google">
                            <i class="lab la-google"></i>
                        </a>
                    </li>
                    @endif
                    @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                    <li class="list-inline-item">
                        <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="twitter">
                            <i class="lab la-twitter"></i>
                        </a>
                    </li>
                    @endif
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        getVariantPrice();
    });

    function CopyToClipboard(containerid) {
        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("Copy");

        } else if (window.getSelection) {
            var range = document.createRange();
            document.getElementById(containerid).style.display = "block";
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
            document.execCommand("Copy");
            document.getElementById(containerid).style.display = "none";

        }
        AIZ.plugins.notify('success', 'Copied');
    }

    function show_chat_modal() {
        @if (Auth:: check())
        $('#chat_modal').modal('show');
        @else
        $('#login_modal').modal('show');
        @endif
    }

</script>
@endsection