@php $cImg = ''; @endphp
@foreach($colors as $key => $col)
@if($product->attribute_image)
@foreach(json_decode($product->attribute_image) as $key => $photo)

@if($photo->color_name == $col)

@php 
$cImg = $photo->color_image;
@endphp

@endif
@endforeach
@endif
<div class="row attribute-image-single">
	<label class="col-form-label col-sm-12">
		<span class="required-star">Image for color *</span>
	</label>
	<div class="col-md-3" >
		<div style="background: {{$col}} ;height: 40px;" class="col-12 col-md-12 order-1 order-md-0"></div>
	</div>
	<div class="col-md-8">
		<input type="hidden" name="attribute_image_color[]" value="{{$col}}">
		<div class="input-group" data-toggle="aizuploader" data-type="image" data-multiple="false">
			<div class="input-group-prepend">
				<div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse')}}</div>
			</div>
			<div class="form-control file-amount">{{ translate('Choose File') }}</div>
			<input type="hidden" name="attribute_image[]" id="attphotos-{{$key+1}}" value="{{ $cImg }}" class="selected-files">
		</div>
		<div class="file-preview box sm">
			@if($cImg)
			<div class="d-flex justify-content-between align-items-center mt-2 file-preview-item" data-id="{{ $cImg }}" title="image.jpg">
				<div class="align-items-center align-self-stretch d-flex justify-content-center thumb"><img src="{{ uploaded_asset($cImg) }}" class="img-fit">
				</div>
				<div class="remove">
					<button class="btn btn-sm btn-link remove-attachment" type="button"><i class="la la-close"></i></button>
				</div>
			</div>
			@endif
		</div>
		<small class="text-muted">{{translate('These images are visible in product details page gallery. Use 600x600 sizes images.')}}</small>
	</div>
</div>
@php $cImg = ''; @endphp
@endforeach