@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
			<h1 class="h3">{{translate('Modify Questionarie')}}</h1>
	</div>
</div>


<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Edit Questionarie')}}</h5>
        <div class="pull-right clearfix">
            <a href="{{route('preownQuestion.index')}}" class="btn btn-secondary text-center">Back</a>
        </div>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('preownQuestion.update', $preownquestions->id) }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Title</label>
                                <input type="text" name="question" value="{{$preownquestions->question}}" class="form-control" required autofocus>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Category</label>
                                <select class="form-control" name="category" required>
                                    <option value="" disabled>Please Select</option>
                                    @foreach($categories as $category)
                                <option value="{{$category->id}}" @if($preownquestions->category_id == $category->id) selected @endif>{{ucfirst($category->name)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Question Type</label>
                                <select class="form-control" name="type" required>
                                    <option value="" disabled>Please Select</option>
                                    <option value="single" @if($preownquestions->type == 'single') selected @endif>Single Answered</option>
                                    <option value="multiple" @if($preownquestions->type == 'multiple') selected @endif>Multiple Answered</option>
                                </select>
                            </div>
        
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Description</label>
                                <textarea name="description" id="description" class="form-control aiz-text-editor">{{$preownquestions->description}}</textarea>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="form-group col-md-8" id="addAns">
                                <label>Answer</label>
                                @foreach($preownquestions->answers as $answer)
                                <div class="entry input-group mb-3" id="cloneAns">
                                    <input type="text" class="form-control" value="{{$answer->answer}}" name="answer[]" placeholder="Your Answer" required>
                                    <div class="input-group-append">
                                      <button class="btn btn-outline-danger remove-ans" type="button">Remove</button>
                                    </div>
                                 </div>  
                                 @endforeach 
                                                         
                                </div>
                        </div>
                        <p> 
                            <a href="#" id="newAns" class="btn btn-outline-primary">Add New Answer</a>
                        </p>
                </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
    </div>
</div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            
            $(document).on("click",'#newAns',function(e){
                e.preventDefault();
            $("#cloneAns").clone().appendTo("#addAns");
            });
            $(document).on("click",".remove-ans",function(e){
    
                e.preventDefault();
                $(this).closest('.entry').remove();
            });
        });

    </script>
@endsection