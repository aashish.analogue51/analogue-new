@extends('backend.layouts.app')
<style type="text/css">
    .badge{
        padding: 1.5em 4em !important;
    }
</style>
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
        <div class="col-md-6">
           <h1 class="h3 text-capitalize">{{translate('All Questionarie')}}</h1>
       </div>
       <div class="col-md-6 text-md-right">
        <a href="{{route('preownQuestion.create')}}" class="btn btn-soft-primary text-center btn-circle float-right">Create</a>
    </div>
</div>
</div>


<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6  text-capitalize">{{translate('Questionarie')}}</h5>
        <div class="pull-right clearfix ">
            <form class="" id="sort_questionarie" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type Question title & Enter') }}">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{translate('Question')}}</th>
                    <th>{{translate('Category')}}</th>
                    <th>{{translate('Type')}}</th>
                    <th>{{translate('Status')}}</th>
                    <th>{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($preownquestions as $key => $questionarie)
                @if ($questionarie)
                <tr>
                    <td>{{ ($key+1)  }}</td>
                    <td>{{ $questionarie->question }}</td>
                    <td>{{ ucfirst($questionarie->category->name) }}</td>
                    <td>{{ ucfirst($questionarie->type)}}</td>
                    <td>
                        <label class="aiz-switch aiz-switch-success mb-0">
                          <input onchange="update_published(this)" value="{{ $questionarie->id }}" type="checkbox" <?php if($questionarie->is_active == 1) echo "checked";?> >
                          <span class="slider round"></span>
                      </label>
                  </td>
                  <td>
                    <a href="{{ route('preownQuestion.edit', $questionarie->id) }}" class="btn btn-sm btn-soft-primary btn-icon btn-circle"><i class="las la-edit" title="edit"></i></a>                             
                    <a href="" data-href="{{ route('preownQuestion.delete', $questionarie->id) }}" class="btn btn-sm btn-soft-danger btn-icon btn-circle confirm-delete" title="delete"><i class="las la-trash"></i></a>
                </td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    <div class="aiz-pagination">
        {{ $preownquestions->appends(request()->input())->links() }}
    </div>
</div>
</div>


<div class="modal fade" id="confirm-ban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to ban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmation" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
@include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript">
    function sort_questionarie(el){
        $('#sort_questionarie').submit();
    }
    function confirm_ban(url)
    {
        $('#confirm-ban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmation').setAttribute('href' , url);
    }

    function confirm_unban(url)
    {
        $('#confirm-unban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmationunban').setAttribute('href' , url);
    }

    function update_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('preownQuestion.status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                AIZ.plugins.notify('success', '{{ translate('Question status updated successfully') }}');
            }
            else{
                AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
            }
        });
    }

</script>
@endsection
