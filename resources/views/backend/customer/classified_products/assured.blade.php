@extends('backend.layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('PreOwn Assured Products')}}</h5>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{translate('Name')}}</th>
                    <th>{{translate('Image')}}</th>
                    <th>{{translate('Uploaded By')}}</th>
                    <th>{{translate('Customer Status')}}</th>
                    <th>{{translate('Published')}}</th>
                    <th>{{translate('Assured')}}</th>
                    <th width="10%">{{translate('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $key => $product)
                <tr>
                    <td>{{ ($key+1) + ($products->currentPage() - 1)*$products->perPage() }}</td>
                    <td><a href="{{ route('customer.product', $product->slug) }}" target="_blank">{{$product->getTranslation('name')}}</a></td>
                    <td><img src="{{ uploaded_asset($product->thumbnail_img) }}" alt="{{translate('Product Image')}}" class="h-50px"></td>
                    <td>{{$product->added_by}}</td>
                    <td>
                        @if ($product->status == 1)
                        <span class="badge badge-inline badge-success">{{ translate('PUBLISHED') }}</span>
                        @else
                        <span class="badge badge-inline badge-danger">{{ translate('UNPUBLISHED') }}</span>
                        @endif
                    </td>
                    <td>
                        <label class="aiz-switch aiz-switch-success mb-0">
                            <input onchange="update_published(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->published == 1) echo "checked";?> >
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                        <div class="form-group">
                            <select onchange="sendStatusToServer({{$product->id}})" id="statusValue{{$product->id}}" name="assured_status" class="form-control">
                                <option @if($product->assured == 1) selected @endif disabled value="1">Requested</option>
                                <option @if($product->assured == 2) selected @endif value="2">Pending</option>
                                <option @if($product->assured == 3) selected @endif value="3">Approved</option>
                                <option @if($product->assured == 0) selected @endif value="0">Rejected</option>
                            </select>
                        </div>
                    </td>
                    <td class="text-right">
                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('customer.product', $product->slug)}}" title="{{ translate('Show') }}">
                            <i class="las la-eye"></i>
                        </a>
                        <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('customer_products.destroy', $product->id)}}}}" title="{{ translate('Delete') }}">
                            <i class="las la-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection

@section('modal')
@include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript">

function update_published(el){
    if(el.checked){
        var status = 1;
    }
    else{
        var status = 0;
    }
    $.post('{{ route('preown_products.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
        if(data == 1){
            AIZ.plugins.notify('success', '{{ translate('Published products updated successfully') }}');
        }
        else{
            AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
        }
    });
}

function sendStatusToServer(id) {
    var p_id = id;
    var assured = $('#statusValue'+p_id).val();
    $.ajax({
      type: "post", 
      dataType: "json", 
      url: "{{ route('preown_products.assured') }}",
      data: {
        id:p_id,assured_id:assured,
        _token: '{{csrf_token()}}'
    },
    success: function(response) {
        console.log(response);
        if(response == 1){
            AIZ.plugins.notify('success', '{{ translate('Assured products updated successfully') }}');
        }
        else{
            AIZ.plugins.notify('danger', '{{ translate('Something went wrong') }}');
        }
    }
});

}
</script>
@endsection
