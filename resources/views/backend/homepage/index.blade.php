@extends('backend.layouts.app')

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class=" align-items-center">
   <h1 class="h3">{{translate('Homepage Drag And Drop')}}</h1>
 </div>
</div>

<div class="col-md-8 mx-auto">
  <div class="card">
    <div class="card-body">

  <table class="table datatable1" id="datatable1">
    <thead>
      <tr>
        <th scope="col">Position</th>
        <th scope="col">Homepage</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody id="sortable">
      @foreach ($frontend as $key => $front)
      <tr class="row1" data-id="{{ $front->id }}">
        <td scope="row">{{ $front->position }}</td>
        <td>{{ $front->type }}</td>
        <td>
         <a onclick="return confirm('Are you sure you change status?')" href="{{route('sort.status',['id'=>$front->id])}}" class="btn @if($front->status == 1) btn-primary @else btn-danger @endif btn-sm" title="Click to change the front Active status.">
          @if($front->status == 1)
          Active 
          @else
          De-active
          @endif
        </a>
      </td>
      <td>
        <a href="{{route('sort.duplicate',['id'=>$front->id])}}" onclick="return confirm('Are you sure to duplicate?')" class="btn btn-primary btn-sm" @if($front->status == 0) disabled title="Activate to Duplicate" @else title="Click to duplicate the content" @endif>Duplicate</a>
        @if($front->id >=6)
        |
        <form action="{{ route('sort.destroy',$front)}}" method="POST">
          @method('DELETE')
          @csrf
          <button onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger btn-sm mt-2">  <i class="las la-trash"></i></button>
        </form>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

</div>
</div>
</div>

@endsection
@section('script')
<!-- Mainly scripts -->
<script src="{{static_asset('assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{static_asset('assets/js/jquery-ui.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{static_asset('datatable/datatables.min.css')}}"/>
<script type="text/javascript" src="{{static_asset('datatable/datatables.min.js')}}"></script>
<script src="{{static_asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript">

  $(function () {
    $( "#sortable" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
        sendOrderToServer();
      }
    });

    function sendOrderToServer() {
      // alert('im updating');
      var front = [];
      $('tr.row1').each(function(index,element) {
        front.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{route('sort.change')}}",
        data: {
          front:front,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
            console.log(response);
          if (response.status == "success") {
            swal({
              text: response.value,
              icon: "success",
            });
          } else {
            swal({
              text: response.value,
              icon: "error",
            });
            // console.log(response);
          }
        }
      });

    }
  });

</script>
@endsection