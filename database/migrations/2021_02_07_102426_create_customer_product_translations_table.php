<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerProductTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_product_translations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('customer_product_id');
			$table->string('name', 200)->nullable();
			$table->string('unit', 20)->nullable();
			$table->text('description')->nullable();
			$table->string('lang', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_product_translations');
	}

}
