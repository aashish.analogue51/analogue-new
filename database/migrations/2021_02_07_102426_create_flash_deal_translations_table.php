<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlashDealTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flash_deal_translations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('flash_deal_id');
			$table->string('title', 50);
			$table->string('lang', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flash_deal_translations');
	}

}
