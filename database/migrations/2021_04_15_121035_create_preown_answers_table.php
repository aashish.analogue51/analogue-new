<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreownAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preown_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longtext('answer');
            $table->unsignedBigInteger('question_id');
            $table->foreign('question_id')->references('id')->on('preown_questions')->onDelete('cascade');
            $table->string('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preown_answers');
    }
}
