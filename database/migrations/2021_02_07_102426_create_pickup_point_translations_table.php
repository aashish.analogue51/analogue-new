<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePickupPointTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pickup_point_translations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('pickup_point_id');
			$table->string('name', 50);
			$table->text('address', 65535);
			$table->string('lang', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pickup_point_translations');
	}

}
