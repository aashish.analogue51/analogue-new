<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontendDragAndDrop extends Model
{
    protected $fillable = [
    	'type','position','status'
    ];
}
