<?php 

namespace App\Helpers;
use App\Category;
use App\Product;
use App\Brand;

class Helper
{
    public  function getCategory($value)
    {
        return Category::find($value);
    }
    public  function getProducts($value)
    {
        return Product::where('brand_id',$value)->get();
    }
    public  function getCatProducts($value)
    {
        return Product::where('category_id',$value)->get();
    }

     public  function getBrand($value)
    {
        return Brand::find($value);
    }
    public  function getBrandName($value)
    {
        return Brand::find($value)->name;
    }
}
