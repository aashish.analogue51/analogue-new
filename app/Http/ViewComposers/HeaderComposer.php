<?php

namespace App\Http\ViewComposers;

use App\Category;
use App\Language;
use App\Currency;
use App\Product;
use App\BusinessSetting;
use Illuminate\View\View;

class HeaderComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = Category::where('level', 0)->get();
        $subcategories = Category::where('level', 1)->get();
        $subsubcategories = Category::where('level', 2)->get();
        $allCategories = Category::latest()->get();
        $languages = Language::all();
        $businessSettings = BusinessSetting::all();
        $currencies = Currency::where('status', 1)->get();
        $a = $businessSettings->where('type', 'system_default_currency')->first()->value;
        $currencyCode = $currencies->where('id',$a)->first()->code;
        $maxPrice = Product::max('unit_price');
        $view->with([
            'categories' => $categories ,
            'subcategories'=> $subcategories,
            'languages' => $languages,
            'currencies' => $currencies,
            'businessSettings' => $businessSettings,
            'currencyCode' => $currencyCode,
            'maxPrice' => $maxPrice,
            'allCategories' => $allCategories,
            'subsubcategories' => $subsubcategories
         ]);
    }
}