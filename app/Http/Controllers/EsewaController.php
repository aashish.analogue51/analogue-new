<?php

namespace App\Http\Controllers;

use Session;
use App\PaymentDetail;
use Illuminate\Http\Request;
use Input;
use Response;
use App\Gateway;
use App\Classes\Esewa;
use App\Order;



/**
 * Class EsewaController
 * @package App\Http\Controllers
 */
class EsewaController extends Controller
{
    
    public function esewa()
    {
        $order = Order::findOrFail(Session::get('order_id'));
       if ($order) {
            return view('frontend.esewa')
                ->withData($order);
        }
        return view('cancel');
          return view('frontend.esewa');
    }

    public function esewaVerify()
    {

        $payment = $this->esewa->esewaVerify();
        $payment = $payment->content();

        $payment = json_decode($payment);


        if ($payment->transaction_id) {
            $transaction_id = $payment->transaction_id;
            $sidhhiPost = $this->sidhhi->siddhi($transaction_id);

            $sidhhiPost = $sidhhiPost->content();

            $sidhhiPost = json_decode($sidhhiPost);

            if ($sidhhiPost == true) {

                return redirect()->route('esewa.sucess', $transaction_id)->with('sucess', sprintf("Your payment is sucessfull"));
            } else {
                Session::flash('success', 'Some error, Payment is recived but recipt cannot be generated. Please contact your nearest branch  to collect recipt.');

                return view('siddhipost_failed')->with('transaction_id', $transaction_id)->with('success', 'Some error, Payment is recived but recipt cannot be generated. Please contact your nearest branch  to collect recipt.');

            }

        } else {
            return redirect()->route('cancel')->with('sucess', sprintf("Your payment is failed"));
        }
    }


    public function esewaSucess()
    {
        return redirect()->route('order_confirmed');
        // $data = PaymentDetail::where('transaction_id', $transaction_id)->first();
        return view('esewasucess')->with('data', $data);
    }

    public function cancel()
    {
        return redirect()->route('checkout.payment_info');
        // return view('cancel');

    }


}
