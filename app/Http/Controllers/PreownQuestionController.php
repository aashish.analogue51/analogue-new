<?php

namespace App\Http\Controllers;

use App\Category;
use App\PreownQuestion;
use App\PreownAnswers;
use Illuminate\Http\Request;

class PreownQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['preownquestions'] = PreownQuestion::paginate(10);
        if ($request->search != null){
            $data['preownquestions'] = PreownQuestion::where('question', 'like', '%'.$request->search.'%')->paginate(10);
            $data['sort_search'] = $request->search;
        }
        return view('backend.customer.preown_question.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::all();
        return view('backend.customer.preown_question.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new PreownQuestion();
        $product->question = $request->question;
        $product->category_id = $request->category;
        $product->type = $request->type;
        $product->description = $request->description;

        if($product->save()){

            foreach($request->answer as $ans)
            {
                $answer=new PreownAnswers();
                $answer->question_id=$product->id;
                $answer->answer=$ans;
                $answer->save();
            }
            flash("Question Successfully Added")->success();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreownQuestion  $preownQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(PreownQuestion $preownQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreownQuestion  $preownQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['preownquestions'] = PreownQuestion::find($id);
        $data['categories'] = Category::all();
        return view('backend.customer.preown_question.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreownQuestion  $preownQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = PreownQuestion::find($id);
        $product->question = $request->question;
        $product->category_id = $request->category;
        $product->type = $request->type;
        $product->description = $request->description;
        if($product->update()){
            $answ=$product->answers()->get();

            foreach($answ as $an)
            {
                $an->delete();
            }
            
            foreach($request->answer as $ans)
                {   $answer=new PreownAnswers();
                    $answer->question_id=$id;
                    $answer->answer=$ans;
                    $answer->save();
                }
                flash('Question Updated successfully')->success();
                return redirect()->back();
            }
            else{
                flash('Something went wrong in Question Update')->error();
                return redirect()->back();
            }
        }

    public function preownQuestionUpdate(Request $request){
        $product = PreownQuestion::find($request->id);
            if($product->is_active == 1)
            {
                $product->is_active = 0;
            }
            else{
                $product->is_active = 1;
            }
            $product->update();
            if ($product->update()) {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreownQuestion  $preownQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $product = PreownQuestion::findOrFail($id);
        if(PreownQuestion::destroy($id)){
            flash('Question deleted')->error();
            return redirect()->back();
        }
         else{
            flash('Something went wrong')->error();
            return back();
        }
    }
}
