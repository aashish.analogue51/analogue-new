<?php

namespace App\Http\Controllers;

use App\FrontendDragAndDrop;
use Illuminate\Http\Request;

class FrontendDragAndDropController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data['frontend'] = FrontendDragAndDrop::orderBy('position','ASC')->get();
       return view('backend.homepage.index', $data);
    }

    public function statusDuplicate($id){
        $fdd = FrontendDragAndDrop::findOrFail($id);
        $fdd->position = (FrontendDragAndDrop::max('position'))+1;
        FrontendDragAndDrop::create([
            'type' => $fdd->type,
            'position' => $fdd->position,
        ]);
        flash(translate('Duplicated successfully.'))->success();
        return back();
    }

    public function sort(Request $request){
     foreach($request->front as $front)
     {
        $fdd=FrontendDragAndDrop::find($front['id']);
        $fdd->position=$front['position'];
        $fdd->save();
   }
        return json_encode(['status' => 'success', 'value' => 'Successfully reordered.'], 200);
    }

    public function statusChange($id){
        $fdd = FrontendDragAndDrop::findOrFail($id);
        if ($fdd->status == 1) {
            $fdd->status = 0;
        } else {
            $fdd->status = 1;
        }
        $fdd->update();
        flash(translate('Active status changed successfully.'))->success();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FrontendDragAndDrop  $frontendDragAndDrop
     * @return \Illuminate\Http\Response
     */
    public function show(FrontendDragAndDrop $frontendDragAndDrop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FrontendDragAndDrop  $frontendDragAndDrop
     * @return \Illuminate\Http\Response
     */
    public function edit(FrontendDragAndDrop $frontendDragAndDrop)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FrontendDragAndDrop  $frontendDragAndDrop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FrontendDragAndDrop $frontendDragAndDrop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FrontendDragAndDrop  $frontendDragAndDrop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FrontendDragAndDrop::destroy($id);
        flash(translate('Section Deleted'))->error();
        return back();
    }
}
