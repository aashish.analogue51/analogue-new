<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KhaltiController extends Controller
{
	public function verification(Request $request)
	{
		return $request;
	//hit the khalit server
			$args = http_build_query(array(
				'token' => $request->input('trans_token'),
				'amount' => $request->input('amount')
			));
	$url = "https://khalti.com/api/v2/payment/verify/";
	# Make the call using API.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$headers = ['Authorization: Key test_secret_key_fec380f707564b08baa441a88a8cd211'];
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	// Response
	$response = curl_exec($ch);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	//process your code below,
	}
}
