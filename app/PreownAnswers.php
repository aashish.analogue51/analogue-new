<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreownAnswers extends Model
{
	 public function question()
    {
        return $this->belongsTo(PreownQuestion::class, 'question_id', 'id');
    }
}
