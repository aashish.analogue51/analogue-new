<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreownQuestion extends Model
{
	public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    
    public function answers()
    {
        return $this->hasMany(PreownAnswers::class, 'question_id', 'id');
    }
}
